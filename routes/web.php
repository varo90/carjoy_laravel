<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'HomeController@index');

Route::resource('products', 'ProductsController');
Route::post('/products/{selected_list}', 'ProductsController@get_product_list');
Route::get('/search', 'SearchController@filter');
// Route::get('/search/category/{name}', 'SearchController@filter_category');
Route::post('/search', 'SearchController@filter');
Route::get('/quickview/{id}', 'ProductsController@quickview');
Route::resource('shoppingcart', 'ShoppingCartsController');
Route::resource('orders', 'OrdersController');
Route::get('/cart', 'ShoppingCartsController@index');
Route::get('/checkout', 'CheckoutController@index');
Route::resource('wishlist', 'WishlistsController');
Route::get('/wishlist_header', 'WishlistsController@show_wishlist_header');
Route::resource('messages', 'MessagesController');
Route::get('/contact', 'MessagesController@contact');
Route::get('/messagesent', 'MessagesController@messagesent');
Route::get('/user/verify/{token}', 'Auth\RegisterController@verifyUser');
Route::get('/tracker', "TrackerController@show");
Route::resource('users', 'UsersController');
Route::get('/account', "UsersController@user_account");
Route::get('/account/change/{attr}', "UsersController@change_user_attr");
Route::post('/account/change', "UsersController@update_user_attr");
Route::resource('addresses', 'AddressesController');
Route::post('/addresses/{id}/mainaddress', "AddressesController@setmainaddress");
Route::get('/pdf_bill/{id}','OrdersController@pdf_bill');
Route::get('/privacy_policy',function(){ return view('privacy_policy'); });
Route::get('/sales_terms',function(){ return view('sales_terms'); });
Route::get('/checkout/shipper','CheckoutController@get_order_details');
Route::get('/quickbuy/shipper','CheckoutController@get_order_details');
Route::get('/quickbuy','CheckoutController@index');
// Route::get('/payment/{order_id}','CheckoutController@payment');
Route::get('/quickbuy/payment_options','CheckoutController@payment_options');
Route::get('/checkout/payment_options','CheckoutController@payment_options');
Route::get('/order_complete/{id}','OrdersController@order_complete');
Route::post('/set_usr','UsersController@set_password');
Route::get('/sendverifyemail','UsersController@send_verification_email');
Route::get('/demo/{order_id}', function ($order_id) {
    return new App\Mail\NewOrderMail($order_id);
});


Route::group(['prefix' => 'admin3012', 'middleware' => ['admin']], function()
{
	Route::get('/', 'Admin\HomeController@index');
	// Route::get('/products', 'Admin\ProductsController@index');
    Route::resource('products', 'Admin\ProductsController');
    Route::post('products/images', 'Admin\ProductsController@upload_image');
    Route::post('products/images/{id}', 'Admin\ProductsController@delete_image');
    Route::post('products/categories', 'Admin\ProductsController@update_categories');
    Route::post('products/brands', 'Admin\ProductsController@update_brands');
    Route::resource('categories', 'Admin\CategoriesController');
    Route::resource('brands', 'Admin\BrandsController');
    Route::resource('users', 'Admin\UsersController');
	Route::get('/sales', 'Admin\OrdersController@index');
});
// Route::group(['prefix' => 'admin3012'], function () {
    
// });
