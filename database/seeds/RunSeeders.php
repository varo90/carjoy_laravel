<?php

use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RunSeeders extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Artisan::call('db:seed', ['--class' => 'VoyagerDatabaseSeeder']);
        Artisan::call('db:seed', ['--class' => 'OrderStatusesTableSeeder']);
        Artisan::call('db:seed', ['--class' => 'TopicsTableSeeder']);
        Artisan::call('db:seed', ['--class' => 'CountriesTableSeeder']);
    }
}
