<?php

use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TopicsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('topics')->insert([
            ['name' => 'Tengo una duda sobre un producto'],
            ['name' => 'Mi pedido aún no ha llegado'],
            ['name' => 'Mi pedido ha llegado en malas condiciones'],
            ['name' => 'Quiero devolver un pedido'],
            ['name' => 'Dudas sobre las formas de pago'],
            ['name' => 'Tengo una sugerencia para mejorar vuestro e-commerce'],
            ['name' => 'Otros'],
        ]);
    }
}
