<?php

use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DummyProductsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
            ['name' => 'Dummy product 1', 'slug' => 'dummy-product-1', 'short_description' => 'This is a dummy 1 short description', 'description' => 'This is a real shitty description for a dummy-crappy product named dummy product 1.'],
            ['name' => 'Dummy product 2', 'slug' => 'dummy-product-2', 'short_description' => 'This is a dummy 2 short descriptions', 'description' => 'This is a real shitty description for a dummy-crappy product named dummy product 2.'],
            ['name' => 'Dummy product 3', 'slug' => 'dummy-product-3', 'short_description' => 'This is a dummy 3 short descriptions', 'description' => 'This is a real shitty description for a dummy-crappy product named dummy product 3.'],
            ['name' => 'Dummy product 4', 'slug' => 'dummy-product-4', 'short_description' => 'This is a dummy 4 short descriptions', 'description' => 'This is a real shitty description for a dummy-crappy product named dummy product 4.'],
            ['name' => 'Dummy product 5', 'slug' => 'dummy-product-5', 'short_description' => 'This is a dummy 5 short descriptions', 'description' => 'This is a real shitty description for a dummy-crappy product named dummy product 5.'],
        ]);

        DB::table('product_sell_prices')->insert([
            ['product_id' => 1, 'sell_price' => 100],
            ['product_id' => 2, 'sell_price' => 150],
            ['product_id' => 3, 'sell_price' => 200],
            ['product_id' => 4, 'sell_price' => 120],
            ['product_id' => 5, 'sell_price' => 180],
        ]);

        DB::table('product_images')->insert([
            ['product_id' => 1, 'path' => 'images/products/test1.jpg', 'is_main_image' => 1],
            ['product_id' => 1, 'path' => 'images/products/test2.jpg', 'is_main_image' => 0],
            ['product_id' => 2, 'path' => 'images/products/test3.jpg', 'is_main_image' => 0],
        ]);

        DB::table('providers')->insert([
            ['name' => 'Dummy provider 1'],
            ['name' => 'Dummy provider 2'],
        ]);

        DB::table('consignments')->insert([
            ['provider_id' => 1, 'cost_price' => 60],
            ['provider_id' => 2, 'cost_price' => 70],
        ]);

        DB::table('consignment_products')->insert([
            ['consignment_id' => 1, 'product_id' => 1, 'cost_price' => 30, 'quantity' => 2],
            ['consignment_id' => 1, 'product_id' => 2, 'cost_price' => 20, 'quantity' => 3],
            ['consignment_id' => 1, 'product_id' => 2, 'cost_price' => 10, 'quantity' => 2],
            ['consignment_id' => 2, 'product_id' => 3, 'cost_price' => 70, 'quantity' => 4],
        ]);

        DB::table('brands')->insert([
            ['name' => 'Citröen', 'logo' => 'citroen-logo.jpg'],
            ['name' => 'Seat', 'logo' => 'seat-logo.jpg'],
            ['name' => 'Mercedes', 'logo' => 'mercedes-logo.png'],
        ]);
    }
}
