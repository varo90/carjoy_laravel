<?php

use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class OrderStatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('order_statuses')->insert([
            ['name' => 'Nueva compra', 'is_default' => 1],
            ['name' => 'Pago pendiente', 'is_default' => 0],
            ['name' => 'Pagado, en preparación', 'is_default' => 0],
            ['name' => 'Enviado', 'is_default' => 0],
            ['name' => 'Entregado', 'is_default' => 0],
            ['name' => 'Cancelado', 'is_default' => 0],
            ['name' => 'En devolución', 'is_default' => 0],
            ['name' => 'Devuelto', 'is_default' => 0],
        ]);
    }
}
