<?php

use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MeasuresTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('measures')->insert([
            ['name_esp' => 'Unidades', 'abbreviation_esp' => 'Uds.', 'name_eng' => 'Units', 'abbreviation_eng' => 'U.'],
            ['name_esp' => 'Kilos', 'abbreviation_esp' => 'Kg', 'name_eng' => 'Kilos', 'abbreviation_eng' => 'Kg'],
            ['name_esp' => 'Litros', 'abbreviation_esp' => 'L.', 'name_eng' => 'Litres', 'abbreviation_eng' => 'L.'],
        ]);
    }
}
