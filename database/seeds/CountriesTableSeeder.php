<?php

use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CountriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $path = 'countries.json';
        $json = json_decode(file_get_contents($path), true);
        foreach ($json as $country) {
            $name_esp = array_get($country, 'translations.spa.common');
            $name_esp = is_array($name_esp) ? reset($name_esp) : $name_esp;
            $name_eng = array_get($country, 'name.common');
            $name_eng = is_array($name_eng) ? reset($name_eng) : $name_eng;
            $phone_code = array_get($country, 'callingCode');
            $phone_code = is_array($phone_code) ? reset($phone_code) : $phone_code;
            $currency_code = array_get($country, 'currency');
            $currency_code = is_array($currency_code) ? reset($currency_code) : $currency_code;
            $currency_symbol = $this->getCurrencySymbol($currency_code,'en_US');
            $currency_symbol = is_array($currency_symbol) ? reset($currency_symbol) : $currency_symbol;
            $lang_code = array_get($country, 'languages');
            $lang_code = is_array($lang_code) ? reset($lang_code) : $lang_code;

            DB::table('countries')->insert([
                'cca2' => array_get($country, 'cca2'),
                'cca3' => array_get($country, 'cca3'),
                'name_spa' => $name_esp,
                'name_eng' => $name_eng,
                'phone_code' => $phone_code,
                'currency_code' => $currency_code,
                'currency_symbol' => $currency_symbol,
                'lang_code' => $lang_code,
            ]);
        }
    }

    public function getCurrencySymbol($currencyCode, $locale = 'en_US')
    {
        $formatter = new \NumberFormatter($locale . '@currency=' . $currencyCode, \NumberFormatter::CURRENCY);
        return $formatter->getSymbol(\NumberFormatter::CURRENCY_SYMBOL);
    }
}
