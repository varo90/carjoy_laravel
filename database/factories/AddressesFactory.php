<?php

use Faker\Generator as Faker;
use Faker\Provider\Address;

$factory->define(App\Addresses::class, function (Faker $faker) {
    $options = ['SHIPPING', 'BILLING'];
    $k = array_rand($options);
    $v = $options[$k];
    return [
        'user_id' => factory(User::class)->create()->id,
        'type' => $v,
        'first_name' => $faker->name,
        'last_name' => $faker->name,
        'address1' => $faker->address,
        'postcode' => $faker->postcode,
        'city' => $faker->name,
        'country_id' => 69,
        'phone' => $faker->phoneNumber
    ];
});
