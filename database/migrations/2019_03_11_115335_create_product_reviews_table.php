<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('product_reviews')) {
            Schema::create('product_reviews', function(Blueprint $table) {
                $table->increments('id');
                $table->integer('product_id')->unsigned();
                $table->bigInteger('user_id')->unsigned();
                $table->tinyInteger('vote');
                $table->string('review',150);

                $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
                $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));

                $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
                $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            });
            
            DB::unprepared('
                CREATE TRIGGER tr_calculate_avg_for_product_on_review AFTER INSERT ON `product_reviews` FOR EACH ROW
                UPDATE products pr SET pr.reviews_avg = (SELECT avg(prew.vote) FROM product_reviews prew WHERE prew.product_id = pr.id GROUP BY prew.product_id)
                WHERE pr.id = NEW.product_id
            ');
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_reviews');
        DB::unprepared('DROP TRIGGER `tr_calculate_avg_for_product_on_review`');
    }
}
