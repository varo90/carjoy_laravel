<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('addresses')) {
            Schema::create('addresses', function(Blueprint $table) {
                $table->increments('id');
                $table->bigInteger('user_id')->unsigned();
                $table->enum('type', ['SHIPPING', 'BILLING']);
                $table->string('company')->nullable()->default(null);
                $table->string('user_name')->nullable()->default(null);
                $table->string('user_surname')->nullable()->default(null);
                $table->string('nif')->nullable()->default(null);
                $table->string('address1')->nullable()->default(null);
                $table->string('postcode')->nullable()->default(null);
                $table->string('city')->nullable()->default(null);
                $table->integer('country_id')->unsigned()->nullable()->default(null);
                $table->string('phone')->nullable()->default(null);
                $table->string('comments')->nullable()->default(null);
                $table->string('is_main_address')->default(0);
                $table->integer('active')->default(1);

                $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
                $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));

                $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
                $table->foreign('country_id')->references('id')->on('countries')->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('addresses');
    }
}
