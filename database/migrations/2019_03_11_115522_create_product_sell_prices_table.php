<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductSellPricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('product_sell_prices')) {
            Schema::create('product_sell_prices', function(Blueprint $table) {
                $table->increments('id');
                $table->integer('product_id')->unsigned();
                $table->decimal('sell_price', 10, 6)->nullable()->default(null);
                $table->decimal('undiscounted_sell_price', 10, 6)->nullable()->default(null);
                $table->float('percentage_discount')->nullable()->default(null);

                $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
                $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));

                $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_sell_prices');
    }
}
