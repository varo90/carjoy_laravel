<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTaxRatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('tax_rates')) {
            Schema::create('tax_rates', function(Blueprint $table) {
                $table->increments('id');
                $table->string('name')->nullable()->default(null);
                $table->string('description')->nullable()->default(null);
                $table->float('rate', 10, 6);
                $table->integer('country_id')->unsigned();
                $table->integer('state_id')->unsigned()->nullable()->default(null);
                $table->integer('postcode')->nullable()->default(null);
                $table->enum('rate_type', ['PERCENTAGE', 'FIXED'])->default('PERCENTAGE');
                $table->enum('applied_with', ['SHIPPING', 'BILLING', 'STORE'])->default('SHIPPING');

                $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
                $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tax_rates');
    }
}
