<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConsignmentProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('consignment_product')) {
            Schema::create('consignment_product', function(Blueprint $table) {
                $table->increments('id');
                $table->integer('consignment_id')->unsigned();
                $table->integer('product_id')->unsigned();
                $table->decimal('cost_price', 10, 6)->nullable()->default(null);
                $table->float('quantity')->nullable()->default(null);

                $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
                $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));

                $table->foreign('consignment_id')->references('id')->on('consignments')->onDelete('cascade');
                $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
            });

            // DB::unprepared('
            //     CREATE TRIGGER tr_Sum_stock_in_product_on_consignment AFTER INSERT ON `consignment_products` FOR EACH ROW
            //     UPDATE products pr SET pr.quantity = pr.quantity + (SELECT cp.quantity FROM consignment_products cp WHERE cp.product_id = pr.id ORDER BY cp.created_at DESC LIMIT 1)
            //     WHERE pr.id = NEW.product_id
            // ');
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('consignment_product');
        // DB::unprepared('DROP TRIGGER `tr_Sum_stock_in_product_on_consignment`');
    }
}
