<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('orders')) {
            Schema::create('orders', function(Blueprint $table) {
                $table->increments('id');

                $table->integer('shipping_method_id')->unsigned();
                $table->integer('payment_method_id')->unsigned()->nullable()->default(null);
                $table->bigInteger('user_id')->unsigned();
                $table->integer('shipment_address_id')->unsigned();
                $table->integer('billing_address_id')->unsigned();
                $table->integer('coupon_id')->unsigned()->nullable()->default(null);
                $table->float('total_price')->unsigned()->default(0);
                $table->float('tax')->unsigned()->default(Config::get('constants.options.IVA'));
                $table->float('shipping_price')->unsigned()->default(0);
                $table->string('comment_from_client',150)->nullable()->default(null);

                $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
                $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));

                $table->foreign('shipping_method_id')->references('id')->on('shipping_methods')->onDelete('cascade');
                $table->foreign('payment_method_id')->references('id')->on('payment_methods')->onDelete('cascade');
                $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
                $table->foreign('shipment_address_id')->references('id')->on('order_addresses')->onDelete('cascade');
                $table->foreign('billing_address_id')->references('id')->on('order_addresses')->onDelete('cascade');
                $table->foreign('coupon_id')->references('id')->on('coupons')->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
