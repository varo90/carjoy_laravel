<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProvidersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('providers')) {
            Schema::create('providers', function(Blueprint $table) {
                $table->increments('id');
                $table->string('name');
                $table->string('website')->nullable()->default(null);
                $table->string('phone_contact_1')->nullable()->default(null);
                $table->string('phone_contact_2')->nullable()->default(null);
                $table->string('email')->nullable()->default(null);

                $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
                $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('providers');
    }
}
