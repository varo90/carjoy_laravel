<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('products')) {
            Schema::create('products', function(Blueprint $table) {
                $table->increments('id');
                $table->string('name');
                $table->string('slug')->unique();
                $table->string('sku')->nullable()->default(null);
                $table->text('short_description')->nullable()->default(null);
                $table->text('description')->nullable()->default(null);
                $table->decimal('quantity', 10, 6)->default(0);
                $table->tinyInteger('status')->default(1);
                $table->tinyInteger('featured')->default(0);
                $table->integer('recommended_quantity')->default(1);
                $table->integer('times_sold')->default(0);
                $table->integer('units_sold')->default(0);
                $table->float('reviews_avg')->default(0);
                $table->tinyInteger('is_taxable')->default(1);

                $table->integer('measure_id')->unsigned()->default(1);
                $table->float('weight')->nullable()->default(null);
                $table->float('width')->nullable()->default(null);
                $table->float('height')->nullable()->default(null);
                $table->float('length')->nullable()->default(null);

                $table->string('meta_title')->nullable()->default(null);
                $table->string('meta_description')->nullable()->default(null);

                $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
                $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));

                $table->foreign('measure_id')->references('id')->on('measures')->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
