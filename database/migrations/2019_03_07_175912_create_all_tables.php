<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\User;
use App\Country;

class CreateAllTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('roles')) {
            Schema::create('roles', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('name');
                $table->string('display_name');

                $table->timestamps();
            });
        }

        if (!Schema::hasTable('users')) {
            Schema::create('users', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->bigInteger('role_id');
                $table->string('name');
                $table->string('user_name');
                $table->string('user_surname');
                $table->string('nif',30)->nullable();
                $table->string('email',191)->unique();
                $table->string('avatar')->nullable();
                $table->timestamp('email_verified_at')->nullable();
                $table->string('password');
                $table->rememberToken();
                $table->string('settings')->nullable();
                $table->Integer('num_logins')->default(0);
                $table->string('stripe_card_token')->nullable();

                $table->timestamps();

                // $table->foreign('role_id')->references('id')->on('roles')->onDelete('cascade');
            });
        }

        if (!Schema::hasTable('categories')) {
            Schema::create('categories', function(Blueprint $table) {
                $table->increments('id');
                $table->integer('parent_id')->nullable()->default(null)->unsigned();
                $table->string('name');
                $table->string('image')->nullable()->default(null);
                $table->string('meta_title')->nullable()->default(null);
                $table->string('meta_description')->nullable()->default(null);

                $table->timestamps();

                $table->foreign('parent_id')->references('id')->on('categories')->onDelete('cascade');
            });
        }

        if (!Schema::hasTable('measures')) {
            if (!Schema::hasTable('measures')) {
                Schema::create('measures', function(Blueprint $table) {
                    $table->increments('id');
                    $table->string('name_esp');
                    $table->string('abbreviation_esp');
                    $table->string('name_eng');
                    $table->string('abbreviation_eng');
    
                    $table->timestamps();
                });
            }
        }

        if (!Schema::hasTable('products')) {
            Schema::create('products', function(Blueprint $table) {
                $table->increments('id');
                $table->string('name');
                $table->string('slug',191)->unique();
                $table->string('sku')->nullable()->default(null);
                $table->text('short_description')->nullable()->default(null);
                $table->text('description')->nullable()->default(null);
                $table->decimal('quantity', 10, 6)->default(0);
                $table->tinyInteger('status')->default(1);
                $table->tinyInteger('featured')->default(0);
                $table->integer('recommended_quantity')->default(1);
                $table->integer('times_sold')->default(0);
                $table->integer('units_sold')->default(0);
                $table->float('reviews_avg')->default(0);
                $table->tinyInteger('is_taxable')->default(1);

                $table->integer('measure_id')->unsigned()->default(1);
                $table->float('weight')->nullable()->default(null);
                $table->float('width')->nullable()->default(null);
                $table->float('height')->nullable()->default(null);
                $table->float('length')->nullable()->default(null);

                $table->string('meta_title')->nullable()->default(null);
                $table->string('meta_description')->nullable()->default(null);

                $table->timestamps();

                $table->foreign('measure_id')->references('id')->on('measures')->onDelete('cascade');
            });
        }

        if (!Schema::hasTable('category_product')) {
            Schema::create('category_product', function(Blueprint $table) {
                $table->increments('id');
                $table->integer('category_id')->unsigned();
                $table->integer('product_id')->unsigned();

                $table->timestamps();

                $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
                $table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');
            });
        }

        if (!Schema::hasTable('product_images')) {
            Schema::create('product_images', function(Blueprint $table) {
                $table->increments('id');
                $table->integer('product_id')->unsigned();
                $table->text('path');
                $table->boolean('is_main_image')->default(0);

                $table->timestamps();

                $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
            });
        }

        if (!Schema::hasTable('product_reviews')) {
            Schema::create('product_reviews', function(Blueprint $table) {
                $table->increments('id');
                $table->integer('product_id')->unsigned();
                $table->bigInteger('user_id')->unsigned();
                $table->tinyInteger('vote');
                $table->string('review',150);

                $table->timestamps();
                
                $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
                $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            });
            DB::unprepared('
                CREATE TRIGGER tr_calculate_avg_for_product_on_review AFTER INSERT ON `product_reviews` FOR EACH ROW
                UPDATE products pr SET pr.reviews_avg = (SELECT avg(prew.vote) FROM product_reviews prew WHERE prew.product_id = pr.id GROUP BY prew.product_id)
                WHERE pr.id = NEW.product_id
            ');
        }

        if (!Schema::hasTable('product_sell_prices')) {
            Schema::create('product_sell_prices', function(Blueprint $table) {
                $table->increments('id');
                $table->integer('product_id')->unsigned();
                $table->decimal('sell_price', 10, 6)->nullable()->default(null);
                $table->decimal('undiscounted_sell_price', 10, 6)->nullable()->default(null);
                $table->float('percentage_discount')->nullable()->default(null);

                $table->timestamps();
                
                $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
            });
        }

        if (!Schema::hasTable('product_relations')) {
            Schema::create('product_relations', function(Blueprint $table) {
                $table->increments('id');
                $table->integer('product_a_id')->unsigned();
                $table->integer('product_b_id')->unsigned();

                $table->timestamps();
                
                $table->foreign('product_a_id')->references('id')->on('products')->onDelete('cascade');
                $table->foreign('product_b_id')->references('id')->on('products')->onDelete('cascade');
            });
        }

        if (!Schema::hasTable('brands')) {
            Schema::create('brands', function (Blueprint $table) {
                $table->increments('id');
                $table->string('name');
                $table->string('logo')->nullable()->default(null);

                $table->timestamps();
            });
        }
        
        if (!Schema::hasTable('brand_product')) {
            Schema::create('brand_product', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('brand_id')->unsigned();
                $table->integer('product_id')->unsigned();

                $table->timestamps();
            });
        }

        if (!Schema::hasTable('providers')) {
            Schema::create('providers', function(Blueprint $table) {
                $table->increments('id');
                $table->string('name');
                $table->string('website')->nullable()->default(null);
                $table->string('phone_contact_1')->nullable()->default(null);
                $table->string('phone_contact_2')->nullable()->default(null);
                $table->string('email')->nullable()->default(null);

                $table->timestamps();
            });
        }

        if (!Schema::hasTable('consignments')) {
            Schema::create('consignments', function(Blueprint $table) {
                $table->increments('id');
                $table->integer('provider_id')->unsigned();
                $table->decimal('cost_price', 10, 6)->nullable()->default(null);

                $table->timestamps();

                $table->foreign('provider_id')->references('id')->on('providers')->onDelete('cascade');
            });
        }

        if (!Schema::hasTable('consignment_product')) {
            Schema::create('consignment_product', function(Blueprint $table) {
                $table->increments('id');
                $table->integer('consignment_id')->unsigned();
                $table->integer('product_id')->unsigned();
                $table->decimal('cost_price', 10, 6)->nullable()->default(null);
                $table->decimal('quantity')->nullable();

                $table->timestamps();

                $table->foreign('consignment_id')->references('id')->on('consignments')->onDelete('cascade');
                $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
            });
            DB::unprepared('
                CREATE TRIGGER `tr_Sum_stock_in_product_on_consignment` AFTER INSERT ON `consignment_product`
                FOR EACH ROW UPDATE products pr SET pr.quantity = pr.quantity + (SELECT cp.quantity FROM consignment_product cp WHERE cp.id = NEW.id ORDER BY cp.created_at DESC LIMIT 1)
                WHERE pr.id = NEW.product_id
            ');
        }

        if (!Schema::hasTable('coupons')) {
            Schema::create('coupons', function(Blueprint $table) {
                $table->increments('id');
                $table->string('name');
                $table->integer('category_id')->unsigned()->nullable()->default(null);
                $table->float('percentage_discount')->nullable()->default(null);
                $table->float('euros_discount')->nullable()->default(null);

                $table->timestamps();
                
                $table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');
            });
        }

        if (!Schema::hasTable('shipping_methods')) {
            Schema::create('shipping_methods', function(Blueprint $table) {
                $table->increments('id');
                $table->string('name');
                $table->string('image');
                $table->float('price');
                $table->integer('min_transit_days');
                $table->integer('max_transit_days');

                $table->timestamps();
            });
        }

        if (!Schema::hasTable('payment_methods')) {
            Schema::create('payment_methods', function(Blueprint $table) {
                $table->increments('id');
                $table->string('name');
                $table->string('image');

                $table->timestamps();
            });
        }

        if (!Schema::hasTable('topics')) {
            Schema::create('topics', function(Blueprint $table) {
                $table->increments('id');
                $table->string('name');

                $table->timestamps();
            });
        }

        if (!Schema::hasTable('messages')) {
            Schema::create('messages', function(Blueprint $table) {
                $table->increments('id');
                $table->string('name');
                $table->string('email');
                $table->integer('topic_id')->unsigned();
                $table->string('subject',50)->nullable();
                $table->string('messages',1200);

                $table->timestamps();

                $table->foreign('topic_id')->references('id')->on('topics')->onDelete('cascade');
            });
        }

        if (!Schema::hasTable('countries')) {
            Schema::create('countries', function(Blueprint $table) {
                $table->increments('id');
                $table->string('cca2');
                $table->string('cca3');
                $table->string('name_spa');
                $table->string('name_eng');
                $table->string('phone_code')->nullable()->default(null);
                $table->string('currency_code')->nullable()->default(null);
                $table->string('currency_symbol')->nullable()->default(null);
                $table->string('lang_code')->nullable()->default(null);

                $table->timestamps();
            });
        }

        if (!Schema::hasTable('order_statuses')) {
            Schema::create('order_statuses', function(Blueprint $table) {
                $table->increments('id');
                $table->string('name');
                $table->tinyInteger('is_default')->default(0);

                $table->timestamps();
            });
        }

        if (!Schema::hasTable('order_addresses')) {
            Schema::create('order_addresses', function(Blueprint $table) {
                $table->increments('id');
                $table->bigInteger('user_id')->unsigned();
                $table->enum('type', ['SHIPPING', 'BILLING']);
                $table->string('company')->nullable()->default(null);
                $table->string('user_name')->nullable()->default(null);
                $table->string('user_surname')->nullable()->default(null);
                $table->string('nif')->nullable()->default(null);
                $table->string('address1')->nullable()->default(null);
                $table->string('postcode')->nullable()->default(null);
                $table->string('city')->nullable()->default(null);
                $table->integer('country_id')->unsigned()->nullable()->default(null);
                $table->string('phone')->nullable()->default(null);
                $table->string('comments')->nullable()->default(null);
                $table->string('is_main_address')->default(0);
                $table->integer('active')->default(1);

                $table->timestamps();

                $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
                $table->foreign('country_id')->references('id')->on('countries')->onDelete('cascade');
            });
        }

        if (!Schema::hasTable('orders')) {
            Schema::create('orders', function(Blueprint $table) {
                $table->increments('id');

                $table->integer('shipping_method_id')->unsigned();
                $table->integer('payment_method_id')->unsigned()->nullable()->default(null);
                $table->bigInteger('user_id')->unsigned();
                $table->integer('shipment_address_id')->unsigned();
                $table->integer('billing_address_id')->unsigned();
                $table->integer('coupon_id')->unsigned()->nullable()->default(null);
                $table->float('total_price')->unsigned()->default(0);
                $table->float('tax')->unsigned()->default(Config::get('constants.options.IVA'));
                $table->float('shipping_price')->unsigned()->default(0);
                $table->string('comment_from_client',150)->nullable()->default(null);

                $table->timestamps();

                $table->foreign('shipping_method_id')->references('id')->on('shipping_methods')->onDelete('cascade');
                $table->foreign('payment_method_id')->references('id')->on('payment_methods')->onDelete('cascade');
                $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
                $table->foreign('shipment_address_id')->references('id')->on('order_addresses')->onDelete('cascade');
                $table->foreign('billing_address_id')->references('id')->on('order_addresses')->onDelete('cascade');
                $table->foreign('coupon_id')->references('id')->on('coupons')->onDelete('cascade');
            });
        }

        if (!Schema::hasTable('order_histories')) {
            Schema::create('order_histories', function(Blueprint $table) {
                $table->increments('id');

                $table->integer('order_id')->unsigned()->nullable()->default(null);
                $table->integer('order_status_id')->unsigned()->nullable()->default(null);

                $table->timestamps();

                $table->foreign('order_status_id')->references('id')->on('order_statuses')->onDelete('cascade');
                $table->foreign('order_id')->references('id')->on('orders')->onDelete('cascade');
            });
        }

        if (!Schema::hasTable('order_products')) {
            Schema::create('order_products', function(Blueprint $table) {
                $table->increments('id');
                $table->integer('product_id')->unsigned();
                $table->integer('order_id')->unsigned();
                $table->integer('quantity');
                $table->decimal('unit_price', 11, 6);
                $table->decimal('tax_amount', 11, 6);

                $table->timestamps();

                $table->foreign('order_id')->references('id')->on('orders')->onDelete('cascade');
                $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
            });
            DB::unprepared('
                CREATE TRIGGER tr_sum_units_sold_in_product_on_new_order AFTER INSERT ON `order_products` FOR EACH ROW
                UPDATE products pr SET pr.units_sold = pr.units_sold + (SELECT op.quantity FROM order_products op WHERE op.id = NEW.id)
                WHERE pr.id = NEW.product_id
            ');
            DB::unprepared('
                CREATE TRIGGER tr_sum_sold_times_in_product_on_new_order AFTER INSERT ON `order_products` FOR EACH ROW
                UPDATE products pr SET pr.times_sold = pr.times_sold + 1
                WHERE pr.id = NEW.product_id
            ');
        }

        if (!Schema::hasTable('order_trackings')) {
            Schema::create('order_trackings', function(Blueprint $table) {
                $table->increments('id');
                $table->integer('order_id')->unsigned()->nullable()->default(null);
                $table->string('tracking_number');

                $table->timestamps();
                
                $table->foreign('order_id')->references('id')->on('orders')->onDelete('cascade');
            });
        }

        if (!Schema::hasTable('addresses')) {
            Schema::create('addresses', function(Blueprint $table) {
                $table->increments('id');
                $table->bigInteger('user_id')->unsigned();
                $table->enum('type', ['SHIPPING', 'BILLING']);
                $table->string('company')->nullable()->default(null);
                $table->string('user_name')->nullable()->default(null);
                $table->string('user_surname')->nullable()->default(null);
                $table->string('nif')->nullable()->default(null);
                $table->string('address1')->nullable()->default(null);
                $table->string('postcode')->nullable()->default(null);
                $table->string('city')->nullable()->default(null);
                $table->integer('country_id')->unsigned()->nullable()->default(null);
                $table->string('phone')->nullable()->default(null);
                $table->string('comments')->nullable()->default(null);
                $table->string('is_main_address')->default(0);
                $table->integer('active')->default(1);

                $table->timestamps();

                $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
                $table->foreign('country_id')->references('id')->on('countries')->onDelete('cascade');
            });
        }

        if (!Schema::hasTable('shopping_carts')) {
            Schema::create('shopping_carts', function(Blueprint $table) {
                $table->increments('id');
                $table->bigInteger('user_id')->unsigned();
                $table->integer('product_id')->unsigned();
                $table->decimal('quantity')->nullable();
                $table->boolean('active')->default(1);

                $table->timestamps();

                $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
                $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
            });
        }

        if (!Schema::hasTable('wishlists')) {
            Schema::create('wishlists', function(Blueprint $table) {
                $table->increments('id');
                $table->bigInteger('user_id')->unsigned();
                $table->integer('product_id')->unsigned();
                $table->boolean('active')->default(1);

                $table->timestamps();

                $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
                $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
            });
        }

        if (!Schema::hasTable('user_logins')) {
            Schema::create('user_logins', function (Blueprint $table) {
                $table->increments('id');
                $table->bigInteger('user_id')->unsigned();
                $table->string('ip_address')->nullable()->default(null);
                $table->string('browser',500)->nullable()->default(null);
                $table->tinyInteger('is_mobile')->nullable()->default(null);
                $table->string('device',500)->nullable()->default(null);
                $table->string('city',200)->nullable()->default(null);

                $table->timestamps();

                $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            });
        }

        if (!Schema::hasTable('verify_users')) {
            Schema::create('verify_users', function (Blueprint $table) {
                $table->bigInteger('user_id')->unsigned();
                $table->string('token');
                $table->timestamps();

                $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            });
        }


        // Schema::create('permissions', function(Blueprint $table) {
        //     $table->increments('id');
        //     $table->string('name')->unique();

                // $table->timestamp('created_at')->default(0);
                // $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
        // });

        // Schema::create('permission_role', function(Blueprint $table) {
        //     $table->increments('id');
        //     $table->integer('permission_id')->unsigned();
        //     $table->integer('role_id')->unsigned();

                // $table->timestamp('created_at')->default(0);
                // $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));

        //     $table->foreign('permission_id')->references('id')->on('permissions')->onDelete('cascade');
        //     $table->foreign('role_id')->references('id')->on('roles')->onDelete('cascade');
        // });

        // Schema::create('warehouses', function(Blueprint $table) {
        //     $table->increments('id');
        //     $table->string('name');

                // $table->timestamp('created_at')->default(0);
                // $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
        // });

        // Schema::create('stock_warehouses', function(Blueprint $table) {
        //     $table->increments('id');
        //     $table->integer('product_id')->unsigned();
        //     $table->decimal('qty', 10, 6)->nullable();

                // $table->timestamp('created_at')->default(0);
                // $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));

        //     $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
        // });

        // if (!Schema::hasTable('site_currency')) {
        //     Schema::create('site_currency', function(Blueprint $table) {
        //         $table->increments('id');
        //         $table->string('code');
        //         $table->string('symbol');
        //         $table->string('name');
        //         $table->float('conversion_rate');
        //         $table->enum('status', ['ENABLED', 'DISABLED'])->nullable()->default(null);

        //         $table->timestamp('created_at')->default(0);
        //         $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
        //     });
        // }

        if (!Schema::hasTable('tax_groups')) {
            Schema::create('tax_groups', function(Blueprint $table) {
                $table->increments('id');
                $table->string('name')->nullable()->default(null);
                $table->string('description')->nullable()->default(null);

                $table->timestamps();
            });
        }

        if (!Schema::hasTable('tax_rates')) {
            Schema::create('tax_rates', function(Blueprint $table) {
                $table->increments('id');
                $table->string('name')->nullable()->default(null);
                $table->string('description')->nullable()->default(null);
                $table->float('rate', 10, 6);
                $table->integer('country_id')->unsigned();
                $table->integer('state_id')->unsigned()->nullable()->default(null);
                $table->integer('postcode')->nullable()->default(null);
                $table->enum('rate_type', ['PERCENTAGE', 'FIXED'])->default('PERCENTAGE');
                $table->enum('applied_with', ['SHIPPING', 'BILLING', 'STORE'])->default('SHIPPING');

                $table->timestamps();
            });
        }
        
        if (!Schema::hasTable('sessions')) {
            Schema::create('sessions', function (Blueprint $table) {
                $table->string('id',191)->unique();
                $table->unsignedInteger('user_id')->nullable();
                $table->string('ip_address', 45)->nullable();
                $table->text('user_agent')->nullable();
                $table->text('payload');
                $table->integer('last_activity');
            });
        }
        
        Artisan::call('db:seed', ['--class' => 'OrderStatusesTableSeeder']);
        Artisan::call('db:seed', ['--class' => 'TopicsTableSeeder']);
        Artisan::call('db:seed', ['--class' => 'CountriesTableSeeder']);
        Artisan::call('db:seed', ['--class' => 'MeasuresTableSeeder']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP TRIGGER `tr_sum_units_sold_in_product_on_new_order`');
        DB::unprepared('DROP TRIGGER `tr_sum_sold_times_in_product_on_new_order`');

        Schema::dropIfExists('category_product');
        Schema::dropIfExists('product_images');
        Schema::dropIfExists('product_reviews');
        Schema::dropIfExists('product_sell_prices');
        Schema::dropIfExists('product_relations');
        Schema::dropIfExists('brand_product');
        Schema::dropIfExists('order_products');
        Schema::dropIfExists('consignment_product');
        Schema::dropIfExists('consignments');
        
        
        Schema::dropIfExists('order_histories');
        Schema::dropIfExists('orders');
        Schema::dropIfExists('order_statuses');

        Schema::dropIfExists('shipping_methods');
        Schema::dropIfExists('payment_methods');
        Schema::dropIfExists('messages');
        Schema::dropIfExists('topics');
        
        Schema::dropIfExists('order_addresses');
        Schema::dropIfExists('addresses');
        Schema::dropIfExists('user_logins');

        Schema::dropIfExists('shopping_carts');
        Schema::dropIfExists('wishlists');

        Schema::dropIfExists('providers');
        Schema::dropIfExists('measures');
        Schema::dropIfExists('products');
        Schema::dropIfExists('coupons');
        Schema::dropIfExists('categories');
        Schema::dropIfExists('brands');

        // Schema::dropIfExists('permission_role');
        // Schema::dropIfExists('permissions');

        // Schema::dropIfExists('warehouses');
        // Schema::dropIfExists('stock_warehouses');
        // Schema::dropIfExists('site_currency');

        Schema::dropIfExists('countries');

        Schema::dropIfExists('tax_groups');
        Schema::dropIfExists('tax_rates');
    }
}
