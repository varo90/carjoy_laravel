<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('order_products')) {
            Schema::create('order_products', function(Blueprint $table) {
                $table->increments('id');
                $table->integer('product_id')->unsigned();
                $table->integer('order_id')->unsigned();
                $table->integer('quantity');
                $table->decimal('unit_price', 11, 6);
                $table->decimal('tax_amount', 11, 6);

                $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
                $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));

                $table->foreign('order_id')->references('id')->on('orders')->onDelete('cascade');
                $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
            });
            
            DB::unprepared('
                CREATE TRIGGER tr_sum_units_sold_in_product_on_new_order AFTER INSERT ON `order_products` FOR EACH ROW
                UPDATE products pr SET pr.units_sold = pr.units_sold + (SELECT op.quantity FROM order_products op WHERE op.id = NEW.id)
                WHERE pr.id = NEW.product_id
            ');
            DB::unprepared('
                CREATE TRIGGER tr_sum_sold_times_in_product_on_new_order AFTER INSERT ON `order_products` FOR EACH ROW
                UPDATE products pr SET pr.times_sold = pr.times_sold + 1
                WHERE pr.id = NEW.product_id
            ');
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP TRIGGER `tr_sum_units_sold_in_product_on_new_order`');
        DB::unprepared('DROP TRIGGER `tr_sum_sold_times_in_product_on_new_order`');
        Schema::dropIfExists('order_products');
    }
}
