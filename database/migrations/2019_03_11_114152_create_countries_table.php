<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCountriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('countries')) {
            Schema::create('countries', function(Blueprint $table) {
                $table->increments('id');
                $table->string('cca2');
                $table->string('cca3');
                $table->string('name_spa');
                $table->string('name_eng');
                $table->string('phone_code')->nullable()->default(null);
                $table->string('currency_code')->nullable()->default(null);
                $table->string('currency_symbol')->nullable()->default(null);
                $table->string('lang_code')->nullable()->default(null);

                $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
                $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
            });
        
            Artisan::call('db:seed', ['--class' => 'CountriesTableSeeder']);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('countries');
    }
}
