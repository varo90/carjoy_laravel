$(document).ready(function(){
    loadshoppingcarttop('hide');

    foldUnfoldOnClick();
    foldOnMouseOut();

    if(product_tab_section == 1) {
        load_product_list('','best-seller',8);
    }
    if(grid_view == 1) {
        load_product_list('','search',0);
    }
    if(open_bootbox_address != 0) {
        if(open_bootbox_address == 'add') {
            open_address("/addresses/create",{errors:address_form_errors,old_fields:old_address_fields},'Crear nueva dirección');
        } else if(open_bootbox_address == 'edit') {
            open_address("/addresses/"+id_addr+"/edit",{errors:address_form_errors,old_fields:old_address_fields},'Editar dirección');
        }
    }
    if(setCookieID!=0) {
        setCookie('sss',setCookieID,180);
    }

    $('.search_brand').on('click',function() {
        var id = $(this).data('id');
        var token = $('#token').val();
        post('/search', {brands:id,'_token':token}, 'POST');
    });

    $('.tab-list').on('click',function() {
        load_product_list($(this),'',8);
    });

    $('#ordersearch').on('change',function(){
        var params = search('');
        post('/search', params, 'post');
    });

    $( "#slider-range" ).slider({
        stop: function() {
            var params = search('');
            post('/search', params, 'post');
        }
    });

    $( ".submit" ).on('click',function(){
        var params = search($(this).attr('id'));
        post('/search', params, 'post');
    });

    $( ".chck-brands" ).on('click',function(){
        var params = search('');
        post('/search', params, 'post');
    });


    $('.lessquantity_c').on('click',function() {
        changeuds($(this),'r','cart');
    });
    $('.sumquantity_c').on('click',function() {
        changeuds($(this),'s','cart');
    });
    $('.delcart_c').on('click',function() {
        deletefromcart($(this),'cart');
    });
    $('.delcart_w').on('click',function() {
        deletefromwishlist($(this),'wishlist');
    });

    $( "#topic" ).on('change',function(){
        if($(this).val() == 7) {
            $('#hand_topic').removeClass('hidden');
            $("#hand_topic input").prop('required',true);
        } else {
            $('#hand_topic').addClass('hidden');
            $("#hand_topic input").prop('required',false);
        }
    });
    

    $('.personal-info a').on('click',function() {
        change_user_info($(this));
    });

    $('.add_address').on('click',function() {
        add_user_address($(this));
    });
    $('.clicable_addr').on('click',function() {
        edit_user_address($(this));
    });
    $('.delete_addr').on('click',function() {
        delete_user_address($(this));
    });
    $('.make_main_address').on('click',function() {
        make_main_address($(this));
    });

    $('#checkout_form input[name=shipping_address]').on('change', function() {
        fill_checkout_address_form($(this),'SHIPPING'); 
        // alert($('input[name=shipping_address]:checked', '#checkout_form').val());
    });

    $('#checkout_form input[name=billing_address]').on('change', function() {
        $('#checkout_form input[name=same_billing_address]').prop('checked', false);
        $('.billing_address').removeClass('hidden');
        set_attribute_property('input_bll','required',true);
        fill_checkout_address_form($(this),'BILLING'); 
    });
    
    $('#checkout_form input[name=same_billing_address]').on('change', function() {
        if($(this).is(":checked")) {    //NOT VISIBLE
            $('.billing_address').addClass('hidden');
            if ($('input[name=billing_address]', '#checkout_form').length) {
                $('#checkout_form input[name=billing_address]').prop('checked', false);
            }
            set_attribute_property('input_bll','required',false);
        } else {    //VISIBLE
            $('.billing_address').removeClass('hidden');
            if ($('input[name=billing_address]', '#checkout_form').length) {
                $('#checkout_form input[name=billing_address]').prop('checked', true);
            }
            set_attribute_property('input_bll','required',true);
        }
    });

    $('#table_shipping_methods tr td').on('click',function() {
        var thisrow = $(this);
        var shipper_id = thisrow.siblings(":first").text();
        if(shipper_id == 1) {
            bootbox.confirm({
                title: '<center><font color="red"><i class="zmdi zmdi-alert-triangle"></i> <b>¡¡ATENCIÓN!!</b></font></center>',
                message: 'Los envíos ordinarios no disponen de número de seguimiento ni de ninguna otra forma de rastreo, por lo que es una forma de envío propensa a <b>retrasos</b>, <b>extravíos</b> o a que el producto pueda llegar en <b>mal estado</b>.<br><font color="red">Por tanto, LA <b>RESPONSABILIDAD</b> EN CASO DE ESCOGER ESTA MODALIDAD <b>ES DEL CLIENTE</b></font>, y nosotros NO nos haremos cargo de ninguna de las situaciones citadas previamente.<br><b>¿Está seguro/a de que desea elegir ésta modalidad?</b>',
                buttons: {
                    cancel: {
                        label: '<i class="fa fa-times"></i> No, utilizaré otra modalidad para prevenir riesgos'
                    },
                    confirm: {
                        label: '<i class="fa fa-check"></i> Sí, entiendo los riesgos y asumo la responsabilidad'
                    }
                },
                callback: function (result) {
                    if(result) {
                        checkshipper(shipper_id,thisrow)
                    }
                }
            });
        } else {
            checkshipper(shipper_id,thisrow)
        }
    });

    if($('input[name="email"]').length){
        $('input[name="email"]').on('blur', function() {
            refresh_payment_methods();
        });
    }



    $("div." + "cookiewarning").on("click", function() {
        setCookie("cookiewarning", 1, 365)
        setCookieWarning(false);
    });

    // cookie warning
    if (getCookie("cookiewarning") != 1) {
        setCookieWarning(true);
    }

    $(".removecookie").on("click", function() {
        eraseCookie("cookiewarning");
        setCookieWarning(false);
    })
    // $('#transferpay').on('click',function() {
    //     bootbox.alert('asdf');
    // });

});

$(document).on('click','.quickview_btn',function() {
    open_quickview($(this));
});

$(document).on('click','.single_add_to_cart_button',function() {
    add(getparams_quick(),'/shoppingcart');
});

$(document).on('click','.single_add_to_wishlist_button',function() {
    add(getparams_quick(),'/wishlist');
});

$(document).on('click','.cart_btn',function() {
    add(getparams_main($(this)),'/shoppingcart');
});

$(document).on('click','.wishlist_btn',function() {
    add(getparams_main($(this)),'/wishlist');
});

$(document).on('click','.delcart',function() {
    deletefromcart($(this),'carttop');
});

/*******************/

function checkshipper(shipper_id,thisrow) {
    $('input[name="selected_shipping"]').val(shipper_id);
    $('#table_shipping_methods tr').each(function() {
        $(this).removeClass('active');
    });
    thisrow.parent().addClass('active');

    params = {p:findGetParameter('p'),quantity:findGetParameter('quantity'),shipper:shipper_id}
    // post(window.location.href, params, 'get');

    var url = window.location.href.split('?')[0];

    $.ajax(
    {
        url: url+"/shipper",
        type: 'GET',
        data: params,
        // beforeSend: function() {
        //     $('.payment-details').html("<img src='/images/icons/car-loading.gif' />");
        // },
        success: function (response){
            $('.payment-details').html(response);
            refresh_payment_methods();
        }
    });

}

function refresh_payment_methods() {
    var email = '';
    if($('input[name="email"]').length) {
        email = $('input[name="email"]').val();
    }
    var shipper = $('input[name="selected_shipping"]').val();

    var current_url = window.location.href.split('?')[0];
    $.ajax(
    {
        url: current_url+'/payment_options',
        type: 'GET',
        data: {email:email,shipper:shipper},
        // beforeSend: function() {
        //     $('.payment-details').html("<img src='/images/icons/car-loading.gif' />");
        // },
        success: function (response){
            $('.payment-method').html(response);
        }
    });
}

function fill_checkout_address_form(selector,type) {
    var id = selector.val();
    if(type == 'BILLING') {
        t = '_bll';
    } else {
        t = '';
    }

    $.ajax(
    {
        url: "/addresses/"+id,
        type: 'GET',
        dataType: 'JSON',
        data: {
            "id": id,
            // "_token": token,
        },
        success: function (response){
            if(response != false) {
                $('#user_name'+t).val(response.user_name);
                $('#user_surname'+t).val(response.user_surname);
                $('#nif'+t).val(response.nif);
                $('#address1'+t).val(response.address1);
                $('#postcode'+t).val(response.postcode);
                $('#city'+t).val(response.city);
                $('#phone'+t).val(response.phone);
                $('#company'+t).val(response.company);
                $('#comments'+t).val(response.comments);
            }
        }
    });
}

function add_user_address(selector) {
    var type = selector.data("id");
    open_address("/addresses/create",{type:type},'Crear nueva dirección');
}
function edit_user_address(selector) {
    var id = selector.attr("id");
    open_address("/addresses/"+id+"/edit",{},'Editar dirección');
}
function delete_user_address(selector) {
    var id = selector.data("id");
    var token = $("meta[name='csrf-token-"+id+"']").attr("content");
    bootbox.confirm({
        title: "Eliminar dirección",
        message: "¿De verdad quieres eliminar esta dirección?",
        buttons: {
            cancel: {
                label: '<i class="fa fa-times"></i> No'
            },
            confirm: {
                label: '<i class="fa fa-check"></i> Sí'
            }
        },
        callback: function (result) {
            if(result) {
                $.ajax(
                {
                    url: "/addresses/"+id,
                    type: 'DELETE',
                    dataType: 'JSON',
                    data: {
                        "id": id,
                        "_token": token,
                    },
                    success: function (response){
                        if(response.deleted == 1) {
                            location.reload();
                        }
                    }
                });
            }
        }
    });
}
function make_main_address(selector) {
    var id = selector.data("id");
    var type = $("meta[name='type-"+id+"']").attr("content");
    var token = $("meta[name='csrf-token-"+id+"']").attr("content");
    var params = {'_token':token,type:type};
    post('/addresses/'+id+'/mainaddress', params, 'post');
    
}

function open_address(route,params,title) {
    $.get(route, params,function(data,status){
        bootbox.dialog({
            message: data,
            title: title,
            buttons: {},
            backdrop: true
        });
    });
}

function change_user_info(selector) {
    var attr = selector.attr('id');
    $.get("/account/change/"+attr, {},function(data,status){
        bootbox.dialog({
            message: data,
            title: 'Cambiar información personal',
            buttons: {},
            backdrop: true
        });
    });
}


function search(view) {
    if(view == '') {
        view = $('#view').val();
    }
    var search = $('#search').val();
    var orderby = $('#ordersearch').val();
    var min_price = $("#slider-range").slider("values")[0];
    var max_price = $("#slider-range").slider("values")[1];
    var brands = get_checked_brands();
    var token = $('#token').val();
    return {view:view,search:search,orderby:orderby,min_price:min_price,max_price:max_price,brands:brands,"_token":token};
}

function get_checked_brands() {
    if(nselectbrands == 0) { return []; }
    var selectedBrands = new Array();
    $('input[name="chck-brand"]:checked').each(function() {
        selectedBrands.push(this.value);
    });
    return selectedBrands;
}

function open_quickview(selector) {
    prdd = selector.attr('id');
    $.get("/quickview/"+prdd, {prd:prdd},function(data,status){
        bootbox.dialog({
            message: data,
            title: ' ',
            buttons: {},
            backdrop: true
        });
    });
}

function load_product_list(selector,selected_list,numitems) {
    var token = $("meta[name='csrf-token']").attr("content");
    if(selected_list == '') {
        selected_list = selector.data("id");
    }
    $.ajax(
    {
        url: "/products/"+selected_list,
        type: 'POST',
        data: {
            "nitems": numitems,
            "_token": token,
        },
        beforeSend: function() {
            $('#list_of_products').html("<img src='/images/icons/car-loading.gif' />");
        },
        success: function (response){
            $('#list_of_products').html(response);
        }
    });
}

function loadshoppingcarttop(action) {
    $.ajax({
        data:  {},
        url:   '/shoppingcart',
        type:  'GET',
        success:  function (response) {
            var htmlcart;
            htmlcart = $('#topcart').html(response);
            if(action == 'show') {
                htmlcart.find('ul').addClass('unfold_cart_top');
                foldOnMouseOut();
                foldIfEmpty(htmlcart.find('#numitemsincart').html());
                foldUnfoldOnClick();
            }
            $('.lessquantity').on('click',function() {
                changeuds($(this),'r','carttop');
            });
            $('.sumquantity').on('click',function() {
                changeuds($(this),'s','carttop');
            });
        }
    });
}

function changeuds(inf,sr,partpage) {
    var id = inf.data("id");
    var token = $("meta[name='csrf-token-"+id+"']").attr("content");

    $.ajax(
    {
        url: "/shoppingcart/"+id,
        type: 'PATCH',
        // dataType: 'JSON',
        data: {
            "id": id,
            "_token": token,
            "sr": sr
        },
        success: function (response){
            if(response.success == 1) {
                if(partpage == 'carttop') {
                    loadshoppingcarttop('show');
                    if(window.location.href.includes("/cart") ) {
                        location.reload();
                    }
                } else {
                    location.reload();
                }
            }
        }
    });
}

function deletefromcart(inf,partpage) {
    var id = inf.data("id");
    var token = $("meta[name='csrf-token-"+id+"']").attr("content");

    $.ajax(
    {
        url: "/shoppingcart/"+id,
        type: 'DELETE',
        dataType: 'JSON',
        data: {
            "id": id,
            "_token": token,
        },
        success: function (response){
            if(response.success == 1) {
                if(partpage == 'carttop') {
                    loadshoppingcarttop('show');
                    if(window.location.href.includes("/cart") ) {
                        location.reload();
                    }
                } else {
                    location.reload();
                }
            }
        }
    });
}

function deletefromwishlist(inf,partpage) {
    var id = inf.data("id");
    var token = $("meta[name='csrf-token-"+id+"']").attr("content");

    $.ajax(
    {
        url: "/wishlist/"+id,
        type: 'DELETE',
        dataType: 'JSON',
        data: {
            "id": id,
            "_token": token,
        },
        success: function (response){
            if(response.success == 1) {
                if(partpage == 'wishlist') {
                    location.reload();
                }
            }
        }
    });
}

function getparams_main(sel) {
    var prnam = sel.attr('id');
    var d = prnam.split("__");
    var prid = d[d.length-1];
    params = {
        "product_id": prid,
        "quantity": $('#qty__'+prid).val(),
        "_token": $('#token__'+prid).val()
    };
    return params;
}

function getparams_quick() {
    params = {
        "product_id": $('#prdd').val(),
        "quantity": $('#qty').val(),
        "_token": $('#token').val()
    };
    return params;
}

function add(params,wh) {
    $.ajax({
        data:  params,
        url:   wh,
        type:  'POST',
        dataType: 'JSON',
        success:  function (response) {
            if(wh == '/shoppingcart') {
                display_product_added_cart(response);
                loadshoppingcarttop('hide');
            } else {
                if(response.inserted == 1) {
                    $('#added_to_wishlist').html('<p><i class="zmdi zmdi-favorite"></i> En tu lista de deseos</p>');
                    $.ajax({
                        data:  {},
                        url:   '/wishlist_header',
                        type:  'get',
                        success:  function (response) {
                            $('.wishlist_header').html(response);
                        }
                    });
                }
            }
        }
    });
}

function display_product_added_cart(resp) {
    var message;
    if(resp.inserted == 1) {
        message = '<p>Producto añadido al carrito</p>';
    } else {
        message = '<p><font color="red">Actualmente disponemos de ' + resp.udsinstock + ' unidades en stock</font></p>';
    }
    $('#added_to_cart').html(message);
}

function foldOnMouseOut() {
    $(".total-cart-in > ul.unfold_cart_top").mouseout(function(){
        $(".total-cart-in ul").removeClass('unfold_cart_top');
    });
}

function foldIfEmpty(numOfItems) {
    if(numOfItems == 0) {
        setTimeout(function(){
            $(".total-cart-in ul").removeClass('unfold_cart_top');
        }, 1500);
    }
}

function foldUnfoldOnClick() {
    $('.total-cart-in').on('click',function() {
        $(".total-cart-in ul").toggleClass('unfold_cart_top');
    });
}

function setCookie(name,value,days) {
    var expires = "";
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days*24*60*60*1000));
        expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + (value || "")  + expires + "; path=/";
}
function getCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}
function eraseCookie(name) {   
    createCookie(name, "", -1);
    // document.cookie = name+'=; Max-Age=-99999999;';  
}

function post(path, params, method) {
    method = method || "post"; // Set method to post by default if not specified.

    // The rest of this code assumes you are not using a library.
    // It can be made less wordy if you use one.
    var form = document.createElement("form");
    form.setAttribute("method", method);
    form.setAttribute("action", path);

    for(var key in params) {
        if(params.hasOwnProperty(key)) {
            var hiddenField = document.createElement("input");
            hiddenField.setAttribute("type", "hidden");
            hiddenField.setAttribute("name", key);
            hiddenField.setAttribute("value", params[key]);

            form.appendChild(hiddenField);
        }
    }

    document.body.appendChild(form);
    form.submit();
}

function findGetParameter(parameterName) {
    var result = null,
        tmp = [];
    var items = location.search.substr(1).split("&");
    for (var index = 0; index < items.length; index++) {
        tmp = items[index].split("=");
        if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);
    }
    return result;
}

function set_attribute_property(classname,attribute,value) {
    $('.'+classname).each(function() {
        $( this ).prop(attribute,value);
    });
}

function setCookieWarning(active) {
    (!!active) ? $("body").addClass("cookiewarning"): $("body").removeClass("cookiewarning")
}
