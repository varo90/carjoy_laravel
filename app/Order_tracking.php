<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Order_tracking extends Model
{
    use Notifiable;

    public function order()
    {
        return $this->belongsTo('App\Order');
    }
}
