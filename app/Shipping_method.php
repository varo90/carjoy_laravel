<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use App\Order;

class Shipping_method extends Model
{
    use Notifiable;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
            'id','created_at','updated_at',
    ];

    public function order()
    {
        return $this->HasOne(Order::class);
    }
}
