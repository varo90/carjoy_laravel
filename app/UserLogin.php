<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Auth;

class UserLogin extends Model
{
    protected $fillable = [
        'user_id', 'ip_address', 'browser', 'device',
    ];

    public static function save_tracking_data($user) {
        $visitor = \Tracker::currentSession();

        $browser = isset($visitor->agent->browser) ? $visitor->agent->browser : null;
        $browser_version = isset($visitor->agent->browser_version) ? $visitor->agent->browser_version : null;
        $is_mobile = isset($visitor->device->is_mobile) ? $visitor->device->is_mobile : null;
        $device_kind = isset($visitor->device->kind) ? $visitor->device->kind : null;
        $device_platform = isset($visitor->device->platform) ? $visitor->device->platform : null;
        $city = isset($visitor->geoIp) ? $visitor->geoIp->city : null;

        $userLogin = UserLogin::create([
            'user_id' => $user->id,
            'ip_address' => $visitor->client_ip,
            'browser' => $browser . ' - ' . $browser_version,
            'is_mobile' => $is_mobile,
            'device' => $device_kind . ' - ' . $device_platform,
            'city' => $city,
        ]);
    }

    public static function update_cart_and_wishlist($new_user) {
        // if($new_user->role_id != 3) {
        if($new_user->email != Auth::User()->email) {
            $is_guest = strpos(Auth::User()->email, '@') === false && strpos(Auth::User()->email, 'guest') !== false;
            if( $is_guest ) {
                $user = User::get_user_by_field('name',$_COOKIE['sss']);
            } else {
                $user = Auth::User();
            }
            Shopping_cart::where('user_id',$user->id)->update(['user_id'=>$new_user->id]);
            Wishlist::where('user_id',$user->id)->update(['user_id'=>$new_user->id]);

            $sc = Shopping_cart::select(DB::raw('count(*) as num_repeated,product_id'))->where('user_id',$new_user->id)->where('active',1)->groupBy('product_id')->having('num_repeated','>',1)->get();
            foreach($sc as $product) {
                $product_id = Shopping_cart::select(DB::raw('max(id) AS product_id'))->from('shopping_carts')->where('product_id',$product->product_id)->first()->product_id;
                Shopping_cart::where('product_id',$product->product_id)->where('id','<>', $product_id)->delete();
            }
            $wl = Wishlist::select(DB::raw('count(*) as num_repeated,product_id'))->where('user_id',$new_user->id)->where('active',1)->groupBy('product_id')->having('num_repeated','>',1)->get();
            foreach($wl as $product) {
                $product_id = Wishlist::select(DB::raw('max(id) AS product_id'))->from('wishlists')->where('product_id',$product->product_id)->first()->product_id;
                Wishlist::where('product_id',$product->product_id)->where('id','<>', $product_id)->delete();
            }
            if( $is_guest ) {
                User::where('id',$user->id)->delete();
            }
        }
        // }
    }
}
