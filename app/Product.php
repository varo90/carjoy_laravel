<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Product extends Model
{
    use Notifiable;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
            'id','created_at','updated_at',
    ];

    public function category()
    {
        return $this->belongsToMany('App\Category')->using('App\CategoryProduct');
    }
    
    public function brand()
    {
        return $this->belongsToMany('App\Brand')->using('App\BrandProduct');
    }

    public function product_relation()
    {
        return $this->belongsTo('App\Product')->using('App\ProductRelation');
    }

    public function shopping_cart()
    {
        return $this->belongsTo('App\Shopping_cart');
    }

    public function product_sell_price()
    {
        return $this->hasMany('App\Product_sell_price');
    }

    public function product_image()
    {
        return $this->hasMany('App\Product_image');
    }

    public function product_review()
    {
        return $this->hasMany('App\Product_review');
    }

    public function measure()
    {
        return $this->hasOne('App\Measure');
    }

    public static function get_all_products() {
        return Product::with('Product_review')->select('products.*','product_sell_prices.sell_price','product_sell_prices.undiscounted_sell_price','product_sell_prices.percentage_discount','product_images.path')
        ->leftJoin('product_sell_prices', function ($q) {
            $q->where('product_sell_prices.id', \DB::raw("(select psp.id FROM product_sell_prices psp WHERE psp.product_id = products.id ORDER BY psp.created_at DESC LIMIT 1)"));
        })->leftJoin('product_images', function ($q) {
            $q->where('product_images.id',\DB::raw("(select pi.id FROM product_images pi WHERE pi.product_id = products.id AND pi.is_main_image = 1 ORDER BY pi.created_at DESC LIMIT 1)"));
        });
    }
}
