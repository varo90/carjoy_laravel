<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use App\User;
use App\order_statuses;
use App\Shipping_methods;
use App\Payment_methods;
use App\Coupons;
use App\Products;
use App\OrderProduct;
use DB;

class Order extends Model
{
    use Notifiable;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
            'id','created_at','updated_at',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function order_statuses()
    {
        return $this->hasOne(Order_status::class);
    }

    public function shipping_method()
    {
        return $this->belongsTo(Shipping_method::class);
    }

    public function payment_method()
    {
        return $this->hasOne(Payment_method::class);
    }

    public function coupons()
    {
        return $this->hasOne(Coupon::class);
    }

    // public function product()
    // {
    //     return $this->hasMany(Product::class)->using('App\OrderProduct');
    // }

    public function order_histories()
    {
        return $this->hasMany(Order_history::class);
    }

    public function order_tracking()
    {
        return $this->hasMany('App\Order_tracking');
    }

    public function OrderProduct()
    {
        return $this->hasMany(OrderProduct::class);
    }

    public function shipment_order_address()
    {
        return $this->hasOne(Order_address::class,'shipment_order_address_id');
    }

    public function billing_order_address()
    {
        return $this->hasOne(Order_address::class,'billing_order_address_id');
    }

    public static function get_order_details($id) {
        return Order::select('orders.id AS order_id','products.id AS product_id','order_statuses.name AS status_name','order_products.unit_price','order_products.quantity AS order_quantity','order_statuses.*','orders.*','products.*')
                ->leftJoin('order_products','orders.id','order_products.order_id')
                ->leftJoin('products','order_products.product_id','products.id')
                ->leftJoin('order_histories', function ($q) {
                    $q->where('order_histories.id', DB::raw("(select oh.id from order_histories oh WHERE oh.order_id = orders.id ORDER BY oh.created_at DESC LIMIT 1)"));
                })
                ->leftJoin('order_statuses','order_histories.order_status_id','order_statuses.id')
                ->where('orders.id',$id)
                ->get();
    }
}
