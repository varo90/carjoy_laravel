<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class PaymentSubmited implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        // dd(request());
        return request()->has('stripeToken') && request()->has('stripeTokenType') && request()->has('stripeEmail');
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Debes seleccionar un método de pago para la compra.';
    }
}
