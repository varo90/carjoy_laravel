<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;
use Auth;

class Wishlist extends Model
{
    use Notifiable;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
            'id','created_at','updated_at',
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function products()
    {
        return $this->hasMany('App\Product');
    }

    public function get_user_wishlist() {
        return Wishlist::select('products.*','product_images.*','product_sell_prices.*','wishlists.id AS id_wsh')
                            ->distinct()
                            ->leftJoin('products', 'products.id', 'wishlists.product_id')
                            ->leftJoin('product_images', 'products.id', 'product_images.product_id')
                            ->leftJoin('product_sell_prices', function ($q) {
                                $q->where('product_sell_prices.id', DB::raw("(select psp.id from product_sell_prices psp WHERE psp.product_id = products.id ORDER BY psp.created_at DESC LIMIT 1)"));
                              })
                            ->where('user_id',Auth::user()->id)
                            ->where('active',1)
                            ->where('is_main_image',1)
                            ->get();
    }
}
