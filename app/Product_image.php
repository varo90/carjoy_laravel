<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Product_image extends Model
{
    use Notifiable;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
            'id','created_at','updated_at',
    ];

    public function product()
    {
        return $this->belongsTo('App\Product');
    }
}
