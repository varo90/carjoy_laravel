<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Order;
use App\Shipping_method;
use App\Order_address;

class NewOrderMail extends Mailable
{
    use Queueable, SerializesModels;

    // var $order_id,$order,$shipping_method,$shipping_address,$billing_address;
    var $order_id;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($order_id)
    {
        $this->order_id = $order_id;
        // $this->order = Order::get_order_details($order_id);
        // $this->shipping_method = Shipping_method::find($this->order->first()->shipping_method_id);
        // $this->shipping_address = Order_address::find($this->order->first()->shipment_address_id);
        // $this->billing_address = $this->order->first()->shipment_address_id == $this->order->first()->billing_address_id ? $this->shipping_address : Order_address::find($this->order->first()->billing_address_id);
        
        // $this->order=$order;$this->shipping_method=$shipping_method;$this->shipping_address=$shipping_address;$this->billing_address=$billing_address;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Tu pedido en emblemasparacoche.com')->markdown('emails/new_order_mail');
    }
}
