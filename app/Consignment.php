<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Consignment extends Model
{
    use Notifiable;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'id','created_at','updated_at',
    ];

    public function products()
    {
        return $this->hasMany('App\Products')->using('App\ConsignmentProduct');
    }

    public function providers()
    {
        return $this->hasOne('App\Provider');
    }
}