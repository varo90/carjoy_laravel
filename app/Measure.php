<?php

namespace App;
use Illuminate\Notifications\Notifiable;

use Illuminate\Database\Eloquent\Model;

class Measure extends Model
{
    use Notifiable;
    
    public function product()
    {
        return $this->belongsTo('App\Product');
    }
}
