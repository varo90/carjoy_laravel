<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Hash;
use App\Mail\VerifyMail;
use Auth;
use Mail;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'role_id', 'user_name', 'user_surname', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function verifyUser()
    {
        return $this->hasOne('App\VerifyUser');
    }

    public function address()
    {
        return $this->hasMany('App\Address');
    }

    public function order()
    {
        return $this->hasMany('App\Order');
    }

    public function role()
    {
        return $this->hasOne('App\Role');
    }

    public static function create_user($name,$role_id,$user_name,$user_surname,$email,$password) {
        if(!User::get_user_by_field('email',$email)) {
            return User::create([
                'name' => $name,
                'role_id' => $role_id,
                'user_name' => $user_name,
                'user_surname' => $user_surname,
                'email' => $email,
                'password' => Hash::make($password),
            ]);
        } else {
            return false;
        }
    }

    public static function login_user($id) {
        Auth::loginUsingId($id, TRUE);
    }

    public static function check_auth() {
        return Auth::check() && Auth::user()->role_id != 3;
    }

    public function check_user_exists($id) {
        $user = User::find($id);
        if($user == null) {
            return false;
        }
        return $user;
    }

    public static function get_user_by_field($field,$value) {
        $user = User::where($field,$value)->first();
        if($user == null) {
            return false;
        }
        return $user;
    }

    public static function set_guest_user_and_cookie() {
        if(!Auth::Check()) {
            $laravel_session = isset($_COOKIE['sss']) ? $_COOKIE['sss'] : 'guest'.strtotime(now());
            $user = User::get_user_by_field('name',$laravel_session);
            if(!$user) {
                $user = User::create_user($laravel_session,3,'Guest','Guest',$laravel_session,'123456789');
                echo "<script>setCookieID='$laravel_session';</script>";
            }
            User::login_user($user->id);
        }
    }

    public static function send_verification_email($user) {
        Mail::to($user->email)
            // ->cc('')
            ->bcc('emblemasparacoche@gmail.com')
            ->queue(new VerifyMail($user));
    }
}
