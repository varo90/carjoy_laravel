<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use App\User;
use Auth;

class Address extends Model
{
    use Notifiable;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'id','created_at','updated_at',
    ];
    
    public function user()
    {
        return $this->belongsTo('App\User');
    }
    
    public function country_id()
    {
        return $this->hasOne('App\Country');
    }

    static function set_field($address,$field_name) {
        $value = Address::old_field($field_name);
        if($value == '') {
            if($address != false) {
                $value = $address->$field_name;
            } 
        }
        if((User::check_auth()) && (isset(Auth::user()[$field_name])) && ($value == '')) {
            return Auth::user()[$field_name];
        }
        return $value;
    }

    static function old_field($field_name) {
        if(request()->has('old_fields')) {
            return request()['old_fields'][$field_name];
        } else {
            return '';
        }
    }
}
