<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Shopping_cart;
use App\Product;
use Auth;

class ShoppingCartsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $shc = new Shopping_cart();
        $shopping_cart = $shc->get_user_shopping_cart();

        foreach($shopping_cart as $product) {
            if($product->quantity < $product->cartquantity) {
                Shopping_cart::where('id',$product->id_shc)
                                ->update(['quantity' => $product->quantity]);
                $shopping_cart = $shc->get_user_shopping_cart();
            } else if($product->cartquantity < 1) {
                Shopping_cart::where('id',$product->id_shc)
                                ->update(['quantity' => 1]);
                $shopping_cart = $shc->get_user_shopping_cart();
            }
        }

        $shopping_cart_total_price = $shc->get_user_shopping_cart_total_price();

        $arr = [
            'shopping_cart' => $shopping_cart,
            'shopping_cart_total_price' => $shopping_cart_total_price
        ];
        if (\Request::is('shoppingcart')) { 
            return view('parts/topcart',$arr);
        } else {
            if($shopping_cart->count() > 0){
                return view('cart',$arr);
            } else {
                return redirect('/');
            }
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $attributes = $this->validateItem();
        $user_id = $attributes['user_id'] = Auth::user()->id;

        $stock_product = Product::find($attributes['product_id'])->quantity;
        $added = Shopping_cart::where('user_id',$user_id)->where('product_id',$attributes['product_id'])->where('active',1)->count();

        $inserted = 1;
        if($added == 0) {
            if($attributes['quantity'] > $stock_product) {
                $attributes['quantity'] = $stock_product;
            }
            Shopping_cart::create($attributes);
        } else {
            $quantity_added = Shopping_cart::where('user_id',$user_id)->where('product_id',$attributes['product_id'])->where('active',1)->get()[0]->quantity;
            if($attributes['quantity'] + $quantity_added <= $stock_product) {
                Shopping_cart::where('user_id',$user_id)->where('product_id',$attributes['product_id'])->where('active',1)->increment('quantity', $attributes['quantity']);
            } else {
                Shopping_cart::where('user_id',$user_id)->where('product_id',$attributes['product_id'])->where('active',1)->update(['quantity' => $stock_product]);
                $inserted = 0;
            }
        }

        return response()->json(['inserted' => $inserted,'udsinstock' => round($stock_product)]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Shopping_cart  $shopping_carts
     * @return \Illuminate\Http\Response
     */
    public function show(Shopping_cart $shopping_cart)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Shopping_cart  $shopping_carts
     * @return \Illuminate\Http\Response
     */
    public function edit(Shopping_cart $shopping_cart)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Shopping_cart  $shopping_carts
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $id = $request->input('id');
        $sr = $request->input('sr');
        
        $shopping_cart_item = Shopping_cart::where('id',$id)->where('user_id',Auth::user()->id);
        if($sr == 'r') {
            // Num of items in cart can't be negative
            $quantity = Shopping_cart::find($id)->quantity;
            if($quantity > 1) {
                $shopping_cart_item->decrement('quantity',1);
            }
        } else if($sr == 's') {
            // Can't put in cart more product than total stock
            $cart_product = Shopping_cart::find($id);
            $product_quantity = Product::find($cart_product->product_id)->quantity;
            if($cart_product->quantity < $product_quantity) {
                $shopping_cart_item->increment('quantity',1);
            }
        }
  
        return response()->json([
            'success' => 1
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Shopping_cart  $shopping_carts
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Shopping_cart::where('id',$id)->where('user_id',Auth::user()->id)->update(['active' => 0]);
  
        return response()->json([
            'success' => 1
        ]);
    }

    public function validateItem() {
        return request()->validate([
            'product_id' => ['required','numeric'],  //LARAVEL VALIDATION RULES
            'quantity' => ['required','numeric'] //LARAVEL VALIDATION RULES
        ]);
    }
}
