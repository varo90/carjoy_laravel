<?php

namespace App\Http\Controllers;

use App\order_statuses;
use Illuminate\Http\Request;

class OrderStatusesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\order_statuses  $order_statuses
     * @return \Illuminate\Http\Response
     */
    public function show(order_statuses $order_statuses)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\order_statuses  $order_statuses
     * @return \Illuminate\Http\Response
     */
    public function edit(order_statuses $order_statuses)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\order_statuses  $order_statuses
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, order_statuses $order_statuses)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\order_statuses  $order_statuses
     * @return \Illuminate\Http\Response
     */
    public function destroy(order_statuses $order_statuses)
    {
        //
    }
}
