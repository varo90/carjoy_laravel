<?php

namespace App\Http\Controllers;

use App\product_sell_prices;
use Illuminate\Http\Request;

class ProductSellPricesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\product_sell_prices  $product_sell_prices
     * @return \Illuminate\Http\Response
     */
    public function show(product_sell_prices $product_sell_prices)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\product_sell_prices  $product_sell_prices
     * @return \Illuminate\Http\Response
     */
    public function edit(product_sell_prices $product_sell_prices)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\product_sell_prices  $product_sell_prices
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, product_sell_prices $product_sell_prices)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\product_sell_prices  $product_sell_prices
     * @return \Illuminate\Http\Response
     */
    public function destroy(product_sell_prices $product_sell_prices)
    {
        //
    }
}
