<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\User;
use App\Product;
use App\Product_sell_price;
use App\BrandProduct;
use Auth;

class SearchController extends Controller
{

    var $select;
    
    public function __construct() {
        !Auth::check() ? User::set_guest_user_and_cookie() : '';
    }

    public function filter(Request $request)
    {
        $category = ($request->has('category')) ? $request['category'] : '';
        $view = ($request->has('view')) ? $request['view'] : 'grid';
        $orderBy = ($request->has('orderby')) ? $request['orderby'] : 'newer';
        $products = Product::get_all_products()->with('Brand')->where('status',1)->where('quantity','>',0);
        $this->select = ['product_sell_prices.sell_price','product_images.path','products.*'];

        if($request->has('category') && $category != '') {
            $products = $products->leftJoin('category_product','products.id','category_product.product_id')->leftJoin('categories','category_product.category_id','categories.id')->where('categories.name',$request['category']);
        }

        $searchText = $this->searchText($products);

        $slider_price = $this->get_price_slider_ranges($products);

        $this->get_orderer($products,$orderBy);

        $selected_brands_ids = array();
        if($request->has('brands') && $request['brands'] != NULL) {
            $products_fake = $products->select($this->select)->distinct()->get();
            $selected_brands_ids = explode(',',$request['brands']);
            $products = $products->leftJoin('brand_product','products.id','=','brand_product.product_id');
            $products = $products->whereIn('brand_id',$selected_brands_ids);
        }

        $products = $products->select($this->select)->distinct()->paginate(16);

        if(isset($products_fake)) {
            $brands = $this->get_involved_brands($products_fake);
        } else {
            $brands = $this->get_involved_brands($products);
        }

        $arr = [
            'products'=>$products->appends(Input::except('page','_token')),
            'category'=>$category,
            'view'=>$view,
            'search'=>$searchText,
            'orderBy'=>$orderBy,
            'slider_price'=>$slider_price,
            'brands'=>$brands,
            'selected_brands_ids'=>$selected_brands_ids
        ];

        return view('shop',$arr);
    }

    public function searchText(&$products) {

        if(!request()->has('search') || request()['search'] == '') { return ''; }

        $searchText = $this->validateSearch()['search'];
        $searchWords = explode(' ',$searchText);

        foreach($searchWords as $word){
            $product = $products->where('name', 'LIKE', '%'.$word.'%');
        }
        // foreach($searchWords as $word){
        //     $products->orWhere('name', 'LIKE', '%'.$word.'%');
        // }

        $product = $products->distinct();
        return $searchText;
    }

    public function get_price_slider_ranges(&$products) {
        $cheapest_product = $products->min('sell_price');
        $expensive_product = $products->max('sell_price');

        if(request()->has('min_price') && request()->has('max_price')) {
            $range_min_price = request()['min_price'];
            $range_max_price = request()['max_price'];
            $products = $products->where('product_sell_prices.sell_price','>=',request()['min_price'])->where('product_sell_prices.sell_price','<=',request()['max_price']);
        } else {
            $range_min_price = $cheapest_product;
            $range_max_price = $expensive_product;
        }

        $slider_price = [
            'cheapest_product'=>$cheapest_product,
            'expensive_product'=>$expensive_product,
            'range_min_price'=>$range_min_price,
            'range_max_price'=>$range_max_price
        ];
        return $slider_price;
    }

    public function get_orderer(&$products,$orderBy) {
        if($orderBy == 'newer') {
            $products = $products->orderBy('created_at','desc');
        } else if($orderBy == 'older') {
            $products = $products->orderBy('created_at','asc');
        } else if($orderBy == 'expensive') {
            $products = $products->orderBy('product_sell_prices.sell_price','desc');
        } else if($orderBy == 'economic') {
            $products = $products->orderBy('product_sell_prices.sell_price','asc');
        } else if($orderBy == 'discount') {
            $products = $products->orderBy('product_sell_prices.percentage_discount','desc');
            $this->select[] = 'product_sell_prices.percentage_discount';
        } else if($orderBy == 'best_seller') {
            $products = $products->orderBy('times_sold','desc');
        } else {
            $products = $products->orderBy('created_at','desc');
        }
    }

    public function get_involved_brands($products) {
        $brands = [];
        $brands_ids = [];
        foreach($products as $product) {
            foreach($product->Brand as $brand) {
                if(!in_array($brand->id,$brands_ids)) {
                    $brands[] = $brand;
                    $brands_ids[] = $brand->id;
                }
            }
        }
        return $brands;
    }

    public function validateSearch() {
        return request()->validate([
            'search' => ['max:100','regex:/(^[A-Za-z0-9 ]+$)+/'],  //LARAVEL VALIDATION RULES
        ]);
    }
}
