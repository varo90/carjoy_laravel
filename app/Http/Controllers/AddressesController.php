<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Address;
use App\User;
use Auth;

class AddressesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(!User::check_auth()) { return redirect('/login'); }
        $type = request()->type;

        return view('scripts/address_form',compact('type'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $fields = $this->validate_address();
        $fields['user_id'] = Auth::user()->id;
        
        // Set as main address if it's the first address this user adds
        $addresses = Address::where('user_id', Auth::user()->id)->where('active',1)->where('type',$fields['type'])->count();
        if($addresses == 0) {
            $fields['is_main_address'] = 1;
        }

        $address = Address::create($fields);
        return redirect('account')->with(['added_address'=>1,'type'=>$fields['type']]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Address  $addresses
     * @return \Illuminate\Http\Response
     */
    public function show(Address $address)
    {
        return $address;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Address  $address
     * @return \Illuminate\Http\Response
     */
    public function edit(Address $address)
    {
        if(!User::check_auth()) { return redirect('/login'); }
        if(Auth::user()->id != $address->user_id) { return redirect('/'); }

        return view('scripts/address_form',compact('address'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Address  $address
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Address $address)
    {
        $fields = $this->validate_address();
        $fields['user_id'] = Auth::user()->id;
        $address->update($fields);
        return redirect('account')->with(['edited_address'=>1,'type'=>$fields['type']]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Address  $address
     * @return \Illuminate\Http\Response
     */
    public function destroy(Address $address)
    {
        $address->update(['active' => 0]);
        return response()->json(['deleted' => 1]);
    }

    public function setmainaddress($id) {
        $address = Address::find($id);
        $address->update(['is_main_address'=>1]);
        $rest_of_addresses = Address::where('user_id', Auth::user()->id)->where('type',request()['type'])->where('id','<>',$id);
        $rest_of_addresses->update(['is_main_address'=>0]);

        return back()->with(['mainaddress'=>1,'type'=>request()['type']]);
    }

    public function validate_address() {
        $fields = request()->validate([
            'user_name' => ['required', 'string', 'min:2', 'max:255'],
            'user_surname' => ['required', 'string', 'min:2', 'max:255'],
            'address1' => ['required', 'string', 'min:2', 'max:255'],
            'postcode' => ['required', 'string', 'min:2', 'max:255'],
            'city' => ['required', 'string', 'min:2', 'max:255'],
            'country_id' => ['required', 'integer'],
            'nif' => ['max:30'],
            'phone' => ['max:30'],
            'company' => ['max:255'],
            'comments' => ['max:255'],
            'type' => ['required','max:255'],
        ]);
        return $fields;
    }
}
