<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Product;
use App\Product_sell_price;
use App\ProductRelation;
use App\Product_image;
use App\CategoryProduct;
use App\BrandProduct;

class ProductsController extends Controller
{
    public function index() {
        $selected_results = (request()->has('res')) ? request()['res'] : 10;
        $status = (request()->has('status')) ? request()['status'] : 10;
        $featured = (request()->has('featured')) ? request()['featured'] : 10;
        $orderby = (request()->has('orderby')) ? request()['orderby'] : '';
        if(request()->has('order')) {
            if(request()['order'] == '') {
                $order = 'DESC';
            } else if(request()['order'] == 'DESC') {
                $order = 'ASC';
            } else{
                $order = '';
            }
        } else {
            $order = '';
        }

        $products = Product::get_all_products();

        if($status != 10) $products = $products->where('status',$status);
        if($featured != 10) $products = $products->where('featured',$featured);
        if($orderby != '' && $order != '') $products = $products->orderBy($orderby,$order);

        $products = $products->Paginate($selected_results);

        return view('admin/products', compact('products','selected_results','status','featured','orderby','order'));
    }

    public function create()
    {
        return view('admin/new_product',['product'=>false]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $fields = request()->validate(['name' => ['required', 'string', 'min:2', 'max:255']]);
        $fields['slug'] = str_slug($fields['name'], "-");
        $fields['status'] = 0;
        $product = Product::create($fields);
        return redirect('/admin3012/products/'.$product->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $prod = Product::with('Product_image')->with('Category')->with('Brand')->select('product_sell_prices.sell_price','product_sell_prices.undiscounted_sell_price','product_sell_prices.percentage_discount','products.id AS product_id','products.*')
        ->leftJoin('product_sell_prices', function ($q) {
            $q->where('product_sell_prices.id', \DB::raw("(select psp.id FROM product_sell_prices psp WHERE psp.product_id = products.id ORDER BY psp.created_at DESC LIMIT 1)"));
        });
        $product = $prod->find($id);

        $selectedCategories = [];
        foreach($product->Category as $category) {
            $selectedCategories[] = $category->id;
        }

        $selectedBrands = [];
        foreach($product->Brand as $brand) {
            $selectedBrands[] = $brand->id;
        }
        
        $related_products1 = ProductRelation::select('product_b_id AS product_id')->where('product_a_id',$product->id)->get()->toArray();
        $related_products2 = ProductRelation::select('product_a_id AS product_id')->where('product_b_id',$product->id)->get()->toArray();
        $related_products_ids = array_merge($related_products1,$related_products2);
        $related_products = $prod->whereIn('products.id',$related_products_ids)->get();

        return view('admin/product',compact('product','related_products','selectedCategories','selectedBrands'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        try {
            if(request()['field'] == 'sell_price') {
                Product_sell_price::create(['product_id'=>request()['id'],'sell_price'=>request()['value']]);
            } else if(request()['field'] == 'name') {
                $slug = str_slug(request()['value'], "-");
                Product::find(request()['id'])->update([request()['field']=>request()['value'] , 'slug'=>$slug]);
            } else if(request()['field'] == 'undiscounted_sell_price' || request()['field'] == 'percentage_discount') {
                $psp = Product_sell_price::where('product_id',request()['id'])->orderBy('created_at','DESC')->first();
                if(request()['field'] == 'undiscounted_sell_price') {
                    $price = $psp->sell_price;
                    $undiscounted_price = request()['value'];
                    $percentage_discount = -1*((($psp->sell_price-$undiscounted_price)*100)/$psp->sell_price);
                } else if (request()['field'] == 'percentage_discount') {
                    $price = $psp->sell_price-(($psp->sell_price*request()['value'])/100);
                    $undiscounted_price = $psp->sell_price;
                    $percentage_discount = request()['value'];
                }
                Product_sell_price::create(['product_id'=>request()['id'],'sell_price'=>$price,'undiscounted_sell_price'=>$undiscounted_price,'percentage_discount'=>$percentage_discount]);
            } else if(request()['field'] == 'is_main_image') {
                Product_image::where('product_id',request()['id'])->update(['is_main_image'=>0]);
                Product_image::find(request()['value'])->update(['is_main_image'=>1]);
            } else {
                Product::find(request()['id'])->update([request()['field']=>request()['value']]);
            }
        } catch (\Illuminate\Database\QueryException $e) {
            return $e;
        }
        return request()['field'] . ' actualizado correctamente.';
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        //
    }

    public function upload_image(Request $request) {
        $input = request()->validate([
            'imagenes' => 'image|mimes:jpeg,png,jpg,gif,svg'
        ]);
        $product_id = request()['product_id'];

        if($files=$request->file('images')){
            $num_of_images = Product_image::where('product_id',$product_id)->count();
            $is_main_image = $num_of_images > 0 ? 0 : 1;
            foreach($files as $file){
                $name=time().'.'.$file->getClientOriginalName();
                $file->move('images/products',$name);
                $path = 'images/products/'.$name;
                Product_image::create(['product_id'=>$product_id,'path'=>$path,'is_main_image'=>$is_main_image]);
                $is_main_image = 0;
            }
        }

        // return back()->with('success','Image Upload successfully');
        return redirect('/admin3012/products/'.$product_id);
    }

    public function delete_image($id) {
        Product_image::find($id)->delete();
    }

    public function update_categories() {
        try {
            $product_id = request()['product_id'];
            $categories = request()['ids'];
            CategoryProduct::where('product_id',$product_id)->delete();
            
            if($categories != null) {
                foreach($categories as $category_id) {
                    CategoryProduct::create(['category_id'=>$category_id,'product_id'=>$product_id]);
                }
            }
            return 'Categorías actualizadas';
        } catch (\Illuminate\Database\QueryException $e) {
            return $e;
        }
    }

    public function update_brands() {
        try {
            $product_id = request()['product_id'];
            $brands = request()['ids'];
            BrandProduct::where('product_id',$product_id)->delete();
            
            if($brands != null) {
                foreach($brands as $brand_id) {
                    BrandProduct::create(['brand_id'=>$brand_id,'product_id'=>$product_id]);
                }
            }
            return 'Marcas actualizadas';
        } catch (\Illuminate\Database\QueryException $e) {
            return $e;
        }
    }
}
