<?php

namespace App\Http\Controllers;

use App\Shipping_methods;
use Illuminate\Http\Request;

class ShippingMethodsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Shipping_methods  $shipping_methods
     * @return \Illuminate\Http\Response
     */
    public function show(Shipping_methods $shipping_methods)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Shipping_methods  $shipping_methods
     * @return \Illuminate\Http\Response
     */
    public function edit(Shipping_methods $shipping_methods)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Shipping_methods  $shipping_methods
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Shipping_methods $shipping_methods)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Shipping_methods  $shipping_methods
     * @return \Illuminate\Http\Response
     */
    public function destroy(Shipping_methods $shipping_methods)
    {
        //
    }
}
