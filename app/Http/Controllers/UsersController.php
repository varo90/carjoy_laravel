<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\VerifyMail;
use App\User;
use Auth;
use Hash;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($email)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function user_account() {
        if(!User::check_auth()) { return redirect('/login'); }
        $user = User::with('address')->with('order')->find(Auth::user()->id);
        return view('user_account',compact('user'));
    }

    public function change_user_attr($attr) {
        if(!User::check_auth()) { return redirect('/login'); }
        return view('scripts/change_user_attr',compact('attr'));
    }

    public function update_user_attr() {
        $user_logged = User::check_auth();
        $user = User::find(Auth::user()->id);
        $password_match = Hash::check(request()['curr_password'], $user->password);
        if(!$user_logged) { return redirect('/login'); }
        if(!$password_match) { return redirect('/account'); }

        if(request()->has('user_name')) {
            $validate = [
                'user_name' => ['required', 'string', 'min:2', 'max:50'],
                'user_surname' => ['required', 'string', 'min:2', 'max:100'],
            ];
            $attr = request()->validate($validate);

            $user->update([
                'user_name' => $attr['user_name'],
                'user_surname' => $attr['user_surname']
            ]);
        } else if(request()->has('password')) {
            $validate = [
                'password' => ['required', 'string', 'min:8', 'confirmed'],
            ];
            $attr = request()->validate($validate);
            $user->update(['password' => Hash::make($attr['password'])]);
        }

        return redirect('account');
    }

    public function set_password() {
        if (strpos(Auth::user()->email, '@') === false) { return redirect('/'); }
        $attr = request()->validate([
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
        $user = User::where('id',Auth::user()->id)->update(['role_id'=>2,'email_verified_at'=>NOW(),'password'=>Hash::make($attr['password'])]);

        return redirect('account');
    }

    public function send_verification_email() {
        $user = (request()->has('email')) ? User::where('email',request()['email'])->first() : Auth::User();
        if($user->email_verified_at == NULL) {
            User::send_verification_email($user);
        }
        return back();
    }
}
