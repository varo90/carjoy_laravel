<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Brand;
use App\Product;
use App\Product_sell_price;
use App\Shopping_cart;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $brands = Brand::All();
        $featured_products = Product::get_all_products()->where('status',1)->where('quantity','>',0)->where('featured',1)->get();

        $arr = [
            'brands' => $brands,
            'featured_products' => $featured_products,
        ];
        
        return view('index',$arr);
    }
}
