<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Country;
use App\Shopping_cart;
use App\Shipping_method;
use App\Product;
use App\Order;
use App\Order_address;
use App\User;
use Auth;

class CheckoutController extends Controller
{
    public function index() {
        $arr = $this->get_data();
        if(!$arr) { return redirect('/login'); }
        return view('checkout',$arr);
    }

    public function get_order_details() {
        $arr = $this->get_data();
        return view('parts/order_details_checkout',$arr);
    }

    public function payment_options() {
        $arr = $this->get_data();
        return view('parts/payment',$arr);
    }

    public function get_data() {
        $currentPath = Route::getFacadeRoot()->current()->uri();
        if(strpos($currentPath, 'quickbuy') !== false) {
            $id = (!request()->has('p') || !is_numeric(request()['p'])) ? 1 : request()['p'];
            $shopping_cart = $this->get_product_info_like_shopping_cart($id);
            $quantity = $shopping_cart->first()['cartquantity'] = (!request()->has('quantity') || !is_numeric(request()['quantity'])) ?
                                                        $shopping_cart->first()->recommended_quantity : request()['quantity'];
            $shopping_cart_total_price = ($shopping_cart->first()->sell_price)*$quantity;
        } else {
            $shopping_cart = Shopping_cart::get_user_shopping_cart();
            if($shopping_cart->count() < 1) { return false; }

            $shopping_cart_total_price = Shopping_cart::get_user_shopping_cart_total_price();
            if($shopping_cart_total_price == null) { return false; }

            $shopping_cart_total_price = $shopping_cart_total_price->totalPrice;
            if($shopping_cart_total_price < 0) { return false; }
        }

        $shipper_id = request()->has('shipper') ? request()['shipper'] : 2;
        $email = $this->get_user_email();

        $arr = [
            'shipping_methods' => Shipping_method::all(),
            'selected_shipping_method' => $shipper_id,
            'shopping_cart' => $shopping_cart,
            'shopping_cart_total_price' => $shopping_cart_total_price,
            'email' => $email
        ];
        return $arr;
    }

    public function get_product_info_like_shopping_cart($product_id) {
        return Product::select('products.*','product_sell_prices.*')
                        ->distinct()
                        ->leftJoin('product_sell_prices', function ($q) {
                            $q->where('product_sell_prices.id', DB::raw("(select psp.id from product_sell_prices psp WHERE psp.product_id = products.id ORDER BY psp.created_at DESC LIMIT 1)"));
                            })
                        ->where('products.id',$product_id)
                        ->where('status',1)
                        ->where('products.quantity','>',0)
                        ->get();
    }

    // public function payment($order_id) {
    //     $order = Order::select('orders.*','shipping_methods.name','users.email')
    //                     ->with('OrderProduct')
    //                     ->leftJoin('users','orders.user_id','users.id')
    //                     ->leftJoin('shipping_methods','orders.shipping_method_id','shipping_methods.id')
    //                     ->find($order_id);
    //     // dd($order);
    //     return view('payment',compact('order'));
    // }

    public function get_user_email() {
        if(User::check_auth()){
            return Auth::user()->email;
        } else {
            return request()->has('email') ? request()['email'] : '';
        }
    }
}
