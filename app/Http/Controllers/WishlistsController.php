<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Wishlist;
use Auth;

class WishlistsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $wishlist = new Wishlist();
        $wishlist_products = $wishlist->get_user_wishlist();
        // $wishlist_products = Wishlist::where('user_id',Auth::user()->id)->get();
        // dd($wishlist_products);
        return view('wishlist', compact('wishlist_products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $attributes = $this->validateItem();

        // $attributes['user_id'] = auth()->id();
        $attributes['user_id'] = Auth::user()->id;

        $added = Wishlist::where('user_id',Auth::user()->id)->where('product_id',$attributes['product_id'])->where('active',1)->count();

        $inserted = 0;
        if($added == 0) {
            Wishlist::create($attributes);
            $inserted = 1;
        }

        return response()->json(['inserted' => $inserted]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Wishlist $wishlist
     * @return \Illuminate\Http\Response
     */
    public function show(Wishlist $wishlist)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Wishlist $wishlist
     * @return \Illuminate\Http\Response
     */
    public function edit(Wishlist $wishlist)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Wishlist $wishlist
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Wishlist $wishlist)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Wishlist  $wishlist
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Wishlist::where('id',$id)->where('user_id',Auth::user()->id)->update(['active' => 0]);
  
        return response()->json([
            'success' => 1
        ]);
    }

    public function show_wishlist_header() {
        return view('parts/wishlist_header');
    }

    public function validateItem() {
        return request()->validate([
            'product_id' => ['required','numeric'],  //LARAVEL VALIDATION RULES
            // 'quantity' => ['required','numeric'] //LARAVEL VALIDATION RULES
        ]);
    }
}
