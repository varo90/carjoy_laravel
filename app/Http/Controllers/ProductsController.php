<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\ProductRelation;
use App\Wishlist;
use App\User;
use Auth;

class ProductsController extends Controller
{
    public function __construct() {
        !Auth::check() ? User::set_guest_user_and_cookie() : '';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // if($request->has('search')) {
        //     $search = $request['search'];
        // } else {
        //     $search = '';
        // }
        // $view = 'grid';
        // return Redirect::route('search', ['search' => $search, 'view' => $view]);
        // $products = Product::where('status',1)->where('quantity','>',0)->Paginate(15);
        // $view = 'grid';
        // $search = '';
        // return view('shop',['products'=>$products, 'view'=>$view, 'search'=>$search]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $product = Product::get_all_products()->where('slug',$slug)->where('status',1)->where('quantity','>',0)->first();
        $en_wishlist = Wishlist::where('user_id',Auth::user()->id)->where('product_id',$product->id)->where('active',1)->get()->count();
        $related_products1 = ProductRelation::select('product_b_id AS product_id')->where('product_a_id',$product->id)->get()->toArray();
        $related_products2 = ProductRelation::select('product_a_id AS product_id')->where('product_b_id',$product->id)->get()->toArray();
        $related_products_ids = array_merge($related_products1,$related_products2);

        $related_products = Product::get_all_products()->whereIn('products.id',$related_products_ids)->where('status',1)->where('quantity','>',0)->get();

        return view('product',['product'=>$product,'en_wishlist'=>$en_wishlist,'related_products'=>$related_products]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        //
    }

    public function quickview($id)
    {
        $id_arr = explode('__',$id);
        $id = end($id_arr);
        $product = Product::get_all_products()->find($id);
        $en_wishlist = Wishlist::where('user_id',Auth::user()->id)->where('product_id',$id)->where('active',1)->get()->count();

        $arr = [
            'product' => $product,
            'en_wishlist' => $en_wishlist
        ];

        return view('scripts/quickview',$arr);
    }

    public function get_product_list(Request $request) {
        $selected_list = $request['selected_list'];
        $limit = $request['nitems'];

        $products = Product::get_all_products();
        $products = $products->where('status',1)->where('quantity','>',0);
        
        if($selected_list == 'best-seller') {
            $products = $products->orderBy('times_sold','desc');
        } else if($selected_list == 'new-arrival') {
            $products = $products->orderBy('created_at','desc');
        } else if($selected_list == 'special-offer') {
            $products = $products->whereNotNull('percentage_discount')->orderBy('product_sell_prices.percentage_discount','desc');
        }
        $products = $products->limit($limit)->get();

        return view('parts/list_of_products',compact('products'));
    }
}
