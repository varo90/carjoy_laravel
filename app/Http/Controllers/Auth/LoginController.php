<?php

// https://www.5balloons.info/user-email-verification-and-account-activation-in-laravel-5-5/

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\User;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    // protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function showLoginForm()
    {
        if(!session()->has('url.intended'))
        {
            session(['url.intended' => url()->previous()]);
        }
        return view('auth.login');    
    }

    public function authenticated(Request $request, $user)
    {
        if ($user->email_verified_at == NULL) { // La fecha de validación del correo hace de flag
            $email = $user->email;
            auth()->logout();
            return back()->with('warning', 'Tienes que confirmar tu dirección de correo. Por favor, revisa tu el email que te hemos enviado. Si no has recibido ningún correo haz <a href="/sendverifyemail?email='.$email.'">clic aquí</a> y te lo enviaremos de nuevo.');
        }
        return redirect()->intended($this->redirectPath());
    }

    public function redirectTo() {
        if(!User::check_auth()) {
            return '/login';
        } else {
            return back();
        }
    }
}
