<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\VerifyUser;
use App\Mail\VerifyMail;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'user_name' => ['required', 'string', 'min:2', 'max:50'],
            'user_surname' => ['required', 'string', 'min:2', 'max:100'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'accept-terms' => ['required'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $mail = explode('@', $data['email']);
        $user = User::create([
            'name' => $mail[0],
            'role_id' => 2,
            'user_name' => $data['user_name'],
            'user_surname' => $data['user_surname'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);

        $verifyUser = VerifyUser::create([
            'user_id' => $user->id,
            'token' => str_random(40)
        ]);

        User::send_verification_email($user);

        return $user;
    }

    public function verifyUser($token) {
        $verifyUser = VerifyUser::where('token', $token)->first();
        if(isset($verifyUser) ){
            $user = $verifyUser->user;
            if(!$user->verified) {
                $verifyUser->user->email_verified_at = now();   // La fecha de validación del correo hace de flag
                $verifyUser->user->save();
                $status = "Tu email ha sido verificado. Ahora puedes iniciar sesión.";
            }else{
                $status = "Tu email ya ha sido verificado. Ahora puedes iniciar sesión.";
            }
        }else{
            return redirect('/login')->with('warning', "Lo sentimos, su email no ha podido ser verificado.");
        }

        return redirect('/login')->with('status', $status);
    }

    protected function registered(Request $request, $user)
    {
        $this->guard()->logout();
        return redirect('/login')->with('status', 'Te hemos enviado un email de activación. Por favor, accede a tu correo y haz clic en el link para verificarlo. Si no has recibido ningún correo haz clic <a href="/sendverifyemail?email='.$email.'">aquí</a> y te lo enviaremos de nuevo.');
    }
}
