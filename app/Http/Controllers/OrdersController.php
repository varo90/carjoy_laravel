<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Stripe\Stripe;
use Stripe\Customer;
use Stripe\Charge;
use App\Rules\PaymentSubmited;
use App\Product;
use App\Order;
use App\User;
use App\Order_address;
use App\Shipping_method;
use App\OrderProduct;
use App\Order_history;
use App\Address;
use App\Shopping_cart;
use App\Order_status;
use App\UserLogin;
use App\Jobs\SendUserEmailToSetAPassword;
use App\Mail\NewOrderMail;
use DB;
use PDF;
use Auth;
use Mail;

class OrdersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!$this->validate_payment()) return back()->withInput(Input::all())->withErrors('Debe seleccionar un método de pago.');
        $fields = $this->validate_address();

        $same_billing_address = request()->has('same_billing_address');
        $new_user = $this->get_user($user,$fields);
        
        if($new_user) {
            $shipping_address = $this->create_address($fields,$user,'SHIPPING');
            $billing_address = $same_billing_address ? false : $this->create_address($fields,$user,'BILLING');
        } else {
            $shipping_address = $this->get_address($fields,$user,'SHIPPING');
            $billing_address = $same_billing_address ? false : $this->get_address($fields,$user,'BILLING');
        }

        $selected_shipping_method = $fields['selected_shipping'];
        $shipping_address_arr = is_array($shipping_address) ? $shipping_address : $shipping_address->toArray();

        if(!$same_billing_address) {
            $billing_address_arr = is_array($billing_address) ? $billing_address : $billing_address->toArray();
        }

        $order_shipping_address = Order_address::create($shipping_address_arr);
        $order_billing_address = $same_billing_address ? $order_shipping_address : Order_address::create($billing_address_arr);

        $iva = config('constants.options.IVA');
        $shipping_price = Shipping_method::find($fields['selected_shipping'])->price;
        $payment_type = $this->get_payment_type();

        if(strpos(url()->previous(), 'quickbuy') !== false) {
            $params = $this->get_url_params(url()->previous());
            $product = Product::get_all_products()->find($params['product_id']);
            $product_id = $params['product_id'];
            $quantity = $params['quantity'];
            $grand_total = ($product->sell_price*$quantity)+$shipping_price;
        } else {
            $grand_total = Shopping_cart::get_user_shopping_cart_total_price()->totalPrice+$shipping_price;
        }

        $order = Order::create(['shipping_method_id'=>$fields['selected_shipping'],'payment_method_id'=>$payment_type,'user_id'=>$user->id,'shipment_address_id'=>$order_shipping_address->id,'billing_address_id'=>$order_billing_address->id,'total_price'=>$grand_total,'tax'=>$iva,'shipping_price'=>$shipping_price]);

        if(strpos(url()->previous(), 'quickbuy') !== false) {
            $quantity = $quantity <= $product->quantity ? $quantity : $product->quantity;
            Product::where('id',$product_id)->decrement('quantity', $quantity);
            OrderProduct::create(['product_id'=>$product_id,'order_id'=>$order->id,'quantity'=>$quantity,'unit_price'=>$product->sell_price,'tax_amount'=>$iva]);
        } else {
            foreach(Shopping_cart::get_user_shopping_cart() as $product) {
                $quantity = $product->cartquantity <= $product->quantity ? $product->cartquantity : $product->quantity;
                Product::where('id',$product->product_id)->decrement('quantity', $quantity);
                OrderProduct::create(['product_id'=>$product->product_id,'order_id'=>$order->id,'quantity'=>$product->cartquantity,'unit_price'=>$product->sell_price,'tax_amount'=>$iva]);
                Shopping_cart::where('id',$product->id_shc)->update(['active'=>2]);
            } 
        }

        $status = $this->payment($grand_total) ? 3 : 2;

        if($status == 3) User::where('id',$user->id)->update(['stripe_card_token'=>request()->stripeToken]);    //Save token

        Order_history::create(['order_id'=>$order->id,'order_status_id'=>$status]);

        Mail::to($user->email)
            // ->cc('')
            ->bcc('emblemasparacoche@gmail.com')
            ->queue(new NewOrderMail($order->id));

        if(!User::check_auth() && strpos(Auth::User()->email, '@')) { 
            User::login_user($user->id);
            if(Auth::User()->role_id == 3) {
                dispatch(new SendUserEmailToSetAPassword($user))->delay(Carbon::now()->addMinutes(1));
            }
        }

        return redirect('/order_complete/'.$order->id);
    }

    public function get_url_params($url) {
        $params = explode('?',$url)[1];
        $product_quantity = explode('&',$params);
        $product_param = $product_quantity[0];
        $quantity_param = $product_quantity[1];
        $product_id = explode('=',$product_param)[1];
        $quantity = explode('=',$quantity_param)[1];
        return ['product_id'=>$product_id,'quantity'=>$quantity ];
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Order $order)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order)
    {
        //
    }

    public function order_complete($id) {
        $order = Order::get_order_details($id);
        if(!Auth::check() || !Auth::User()->id == $order->first()->user_id) { return redirect('/'); }
        $shipping_method = Shipping_method::find($order->first()->shipping_method_id);
        $shipping_address = Order_address::find($order->first()->shipment_address_id);
        $billing_address = $order->first()->shipment_address_id == $order->first()->billing_address_id ? $shipping_address : Order_address::find($order->first()->billing_address_id);

        return view('order_complete',compact('order','shipping_method','shipping_address','billing_address'));
    }



    public function validate_address() {
        $fields_to_validate = [
            'user_name' => ['required', 'string', 'min:2', 'max:255'],
            'user_surname' => ['required', 'string', 'min:2', 'max:255'],
            'address1' => ['required', 'string', 'min:2', 'max:255'],
            'postcode' => ['required', 'string', 'min:2', 'max:255'],
            'city' => ['required', 'string', 'min:2', 'max:255'],
            'country_id' => ['required', 'integer'],
            'nif' => ['required', 'max:30'],
            'phone' => ['max:30'],
            'company' => ['max:255'],
            'comments' => ['max:255'],
        ];
        if(request()->has('email')) {
            $fields_to_validate['email'] = ['required', 'email', 'min:2', 'max:255'];
        }
        if(!request()->has('same_billing_address')) {
            $fields_to_validate['user_name_bll'] = ['required', 'string', 'min:2', 'max:255'];
            $fields_to_validate['user_surname_bll'] = ['required', 'string', 'min:2', 'max:255'];
            $fields_to_validate['address1_bll'] = ['required', 'string', 'min:2', 'max:255'];
            $fields_to_validate['postcode_bll'] = ['required', 'string', 'min:2', 'max:255'];
            $fields_to_validate['city_bll'] = ['required', 'string', 'min:2', 'max:255'];
            $fields_to_validate['country_id_bll'] = ['required', 'integer'];
            $fields_to_validate['nif_bll'] = ['required', 'max:30'];
            $fields_to_validate['phone_bll'] = ['max:30'];
            $fields_to_validate['company_bll'] = ['max:255'];
            $fields_to_validate['comments_bll'] = ['max:255'];
        }
        if(request()->has('selected_shipping') ) {
            $fields_to_validate['selected_shipping'] = ['required', 'integer'];
        }
        $fields = request()->validate($fields_to_validate);
        return $fields;
    }


    public function get_user(&$user,$fields) {
        $new_user = 0;
        if(!User::check_auth()) {
            $user = User::get_user_by_field('email',$fields['email']);
            if(!$user) {
                $user = User::create_user((explode('@',$fields['email'])[0]),3,$fields['user_name'],$fields['user_surname'],$fields['email'],'123456789');
                $new_user = 1;
            }
        } else {
            $user = Auth::User();
        }
        return $new_user;
    }

    public function get_address($fields,$user,$type) {
        if(request()->has(strtolower($type).'_address')) {
            return Address::find(request()[strtolower($type).'_address']);
        } else {
            return $this->create_address($fields,$user,$type);
        }
    }

    public function create_address($fields,$user,$type) {
        $is_main = (Address::where('user_id',$user->id)->where('type',$type)->count() == 0) ? 1 : 0;
        $bll = $type == 'BILLING' ? '_bll' : '';
        $address = ['user_id'=>$user->id,'company'.$bll=>$fields['company'.$bll],'type'=>$type,'user_name'.$bll=>$fields['user_name'.$bll],'user_surname'.$bll=>$fields['user_surname'.$bll],'nif'.$bll=>$fields['nif'.$bll],'address1'.$bll=>$fields['address1'.$bll],'postcode'.$bll=>$fields['postcode'.$bll],'city'.$bll=>$fields['city'.$bll],'country_id'.$bll=>$fields['country_id'.$bll],'phone'.$bll=>$fields['phone'.$bll],'comments'.$bll=>$fields['comments'.$bll],'is_main_address'=>$is_main,'active'=>1];
        Address::create($address);
        return $address;
    }

    public function payment($grand_total)
    {
        try {
            Stripe::setApiKey(config('services.stripe.secret'));
            $customer = Customer::create(array(
                'email' => request()->stripeEmail,
                'source' => request()->stripeToken
            ));
            $charge = Charge::create(array(
                'customer' => $customer->id,
                'amount' => $grand_total*100,
                'currency' => 'eur'
            ));
            return 1;
        } catch (\Exception $ex) {
            // return $ex->getMessage();
            return 0;
        }
    }

    public function validate_payment() {
        return request()->has('stripeToken') && request()->has('stripeTokenType') && request()->has('stripeEmail');
    }

    public function get_payment_type() {
        if(request()['stripeTokenType'] == 'card') {
            return 1;
        } else {
            return 0;
        }
    }

    public function pdf_bill($id)
    {
        if(!User::check_auth() || Order::find($id)->user_id != Auth::user()->id) { return back(); }
        $order = Order::select('orders.id AS order_id','orders.*','order_addresses.*')->leftjoin('order_addresses','orders.billing_address_id','=','order_addresses.id')->with('OrderProduct')->find($id);
        $products = OrderProduct::select('products.id','products.name','products.slug','order_products.quantity','order_products.unit_price','order_products.tax_amount')->leftjoin('products','order_products.product_id','=','products.id')->where('order_id',$id)->get();
        // return view('scripts/pdf_bill',compact('order'));
        // view()->share('order',$order);

        // if(request()->has('download')){
            $pdf = PDF::loadView('scripts/pdf_bill',compact('order','products'));
            return $pdf->download('factura_emblemasparacoche.pdf');
        // }
    }
}
