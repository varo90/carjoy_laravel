<?php

namespace App\Http\Controllers;

use App\Consignments;
use Illuminate\Http\Request;

class ConsignmentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Consignments  $consignments
     * @return \Illuminate\Http\Response
     */
    public function show(Consignments $consignments)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Consignments  $consignments
     * @return \Illuminate\Http\Response
     */
    public function edit(Consignments $consignments)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Consignments  $consignments
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Consignments $consignments)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Consignments  $consignments
     * @return \Illuminate\Http\Response
     */
    public function destroy(Consignments $consignments)
    {
        //
    }
}
