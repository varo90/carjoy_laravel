<?php

// http://oneordinaryblog.com/php-laravel/laravel-stats-tracker-part-1
// https://github.com/antonioribeiro/tracker

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TrackerController extends Controller
{
    public function show(){

        if(!(\Auth::check()) || !(\Auth::user()->hasRole('admin'))) {
            return redirect('/');
        }

        $pageViews = \Tracker::pageViews(60 * 24 * 120);

        $pageViewsPerCountry = \Tracker::pageViewsByCountry(60 * 24 * 120);

        $allData = [$pageViews,$pageViewsPerCountry];

        return view('tracker/index', compact('allData'));
    }
}