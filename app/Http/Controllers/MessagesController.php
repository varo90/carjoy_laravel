<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
Use Redirect;
use App\Message;
use App\Topic;
use App\Mail\MessageSent;

class MessagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $attributes = $this->validateMessage();

        Message::create($attributes);
        
        \Mail::to(['alvaro_sm_8@hotmail.com',$request['email']])->queue(
            new MessageSent($attributes)
        )->subject('Gracias por contactar con emblemasparacoche');

        return redirect('messagesent')->with(compact('attributes'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Message  $messages
     * @return \Illuminate\Http\Response
     */
    public function show(Message $messages)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Message  $messages
     * @return \Illuminate\Http\Response
     */
    public function edit(Message $messages)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Message  $messages
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Message $messages)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Message  $messages
     * @return \Illuminate\Http\Response
     */
    public function destroy(Message $messages)
    {
        //
    }

    public function contact()
    {
        $topics = Topic::All();
        return view('contact',compact('topics'));
    }

    public function messagesent()
    {
        if(session()->has('attributes')) {
            return view('messagesent');
        } else {
            return redirect('/');
        }
    }

    public function validateMessage() {
        return request()->validate([
            'name' => ['required','min:2','max:50'],  //LARAVEL VALIDATION RULES
            'email' => ['required','email','max:100'], //LARAVEL VALIDATION RULES
            'topic_id' => ['required','numeric'], //LARAVEL VALIDATION RULES
            'subject' => ['max:50'], //LARAVEL VALIDATION RULES
            'message' => ['required','min:2','max:1000'] //LARAVEL VALIDATION RULES
        ]);
    }
}
