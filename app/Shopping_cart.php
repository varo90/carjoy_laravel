<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;
use Auth;

class Shopping_cart extends Model
{
    use Notifiable;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
            'id','created_at','updated_at',
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function product()
    {
        return $this->hasMany('App\Product');
    }

    public static function get_user_shopping_cart() {
        return Shopping_cart::select('products.id as product_id','products.*','product_images.*','product_sell_prices.*','shopping_carts.id AS id_shc','shopping_carts.quantity AS cartquantity')
                            ->distinct()
                            ->leftJoin('products', 'products.id', 'shopping_carts.product_id')
                            ->leftJoin('product_images', 'products.id', 'product_images.product_id')
                            ->leftJoin('product_sell_prices', function ($q) {
                                $q->where('product_sell_prices.id', DB::raw("(select psp.id from product_sell_prices psp WHERE psp.product_id = products.id ORDER BY psp.created_at DESC LIMIT 1)"));
                              })
                            ->where('user_id',Auth::user()->id)
                            ->where('active',1)
                            ->where('status',1)
                            ->where('products.quantity','>',0)
                            ->where('is_main_image',1)
                            ->get();
    }

    public static function get_user_shopping_cart_total_price() {
        return Shopping_cart::select(DB::raw('SUM(product_sell_prices.sell_price * shopping_carts.quantity) as totalPrice'))
                            ->distinct()
                            ->leftJoin('products', 'products.id', 'shopping_carts.product_id')
                            ->leftJoin('product_sell_prices', function ($q) {
                                $q->where('product_sell_prices.id', DB::raw("(select psp.id from product_sell_prices psp WHERE psp.product_id = products.id ORDER BY psp.created_at DESC LIMIT 1)"));
                            })
                            ->where('user_id',Auth::user()->id)
                            ->where('active',1)
                            ->where('status',1)
                            ->where('products.quantity','>',0)
                            ->first();
    }
}
