@extends('layouts/layout')

@section('title','Artículos en la lista de deseos')

@section('content')

<!-- Start page content -->
<section id="page-content" class="page-wrapper">

    <!-- SHOP SECTION START -->
    <div class="shop-section mb-80">
        <div class="container">
            <div class="row">
                <div class="tab-pane active" id="wishlist">
                    <div class="wishlist-content">
                        @if($wishlist_products->count() == 0)
                            <div class="center"><h2>Aún no hay productos en tu lista de deseos.</h2></div>
                        @else
                            <form action="#">
                                <div class="table-content table-responsive mb-50">
                                    <table class="text-center">
                                        <thead>
                                            <tr>
                                                <th class="product-thumbnail">Producto</th>
                                                <th class="product-price">Precio</th>
                                                <th class="product-stock">Stock</th>
                                                <th class="product-add-cart">Añadir al carrito</th>
                                                <th class="product-remove">Eliminar</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($wishlist_products as $product)
                                                <tr>
                                                    <td class="product-thumbnail">
                                                        <div class="pro-thumbnail-img">
                                                            <img src="{{ URL::asset($product->path) != NULL ? URL::asset($product->path) : URL::asset('img/cart/1.jpg') }}" alt="{{ $product->name }}">
                                                        </div>
                                                        <div class="pro-thumbnail-info text-left">
                                                            <h6 class="product-title-2">
                                                                <a href="/products/{{$product->slug }}" title="{{$product->name}}">{{ str_limit($product->name, $limit = 41, $end = '...') }}</a>
                                                            </h6>
                                                            {{-- <p>Brand: Brand Name</p>
                                                            <p>Model: Grand s2</p>
                                                            <p> Color: Black, White</p> --}}
                                                        </div>
                                                    </td>
                                                    <td class="product-price">{{ number_format((float)($product->sell_price), 2, ',', '.')  }} €</td>
                                                    <td class="product-stock text-uppercase">
                                                        @if($product->quantity > 0 && $product->status == 1)
                                                            {{ 'En stock: ' . round($product->quantity) . ' Uds.' }}
                                                        @else
                                                            <font color="red">No disponible</font>
                                                        @endif
                                                    </td>
                                                    <td class="product-add-cart">
                                                        <input type="hidden" name="_token" id="token__{{$product->id}}" value="{{ csrf_token() }}">
                                                        <input name="qty" id="qty__{{$product->id}}" type="hidden" value="{{ $product->recommended_quantity }}">
                                                        <a id="cart__{{$product->id}}" class="btn_pr cart_btn" title="Añadir {{ $product->recommended_quantity }} uds. al carrito"><i class="zmdi zmdi-shopping-cart-plus"></i></a>
                                                        {{-- <a class="" title="Añadir 1 unidad al carrito">
                                                            <i class="zmdi zmdi-shopping-cart-plus"></i>
                                                        </a> --}}
                                                    </td>
                                                    <td class="product-remove">
                                                        <meta name="csrf-token-{{$product->id_wsh}}" content="{{ csrf_token() }}">
                                                        <a class="delcart_w" data-id="{{ $product->id_wsh }}">
                                                            <i class="zmdi zmdi-close"></i>
                                                        </a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </form>
                        @endif
                    </div>
                </div>
                <!-- wishlist end -->
            </div>
        </div>
    </div>
    <!-- SHOP SECTION END -->             

</section>

@endsection