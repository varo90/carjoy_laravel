@extends('layouts/layout')

@section('title','Login | Registro')

@section('content')

<div id="page-content" class="page-wrapper">
    
    <!-- LOGIN SECTION START -->
    <div class="login-section mb-80">
        <div class="container">
            <div class="registered-customers">
                {{-- <h6 class="widget-title border-left mb-20">{{ __('USUARIO REGISTRADO') }}</h6> --}}

                @if(old('whichform') == 'login')
                    @include('common/errors')
                @endif

                @include('common/verify_account')

                <form method="POST" action="{{ route('login') }}">
                    @csrf
                    <div class="login-account p-30 box-shadow">
                        <p>¿Aún no tienes cuenta? <a href="/register">Regístrate aquí</a></p>
                        <input type="hidden" name="whichform" value="login">

                        <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ (old('whichform') == 'login') ? old('email') : '' }}" placeholder="Tu Email" required autofocus>

                        <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="Contraseña" required>

                        <div class="row">
                            <div class="col-sm-8 col-xs-12">
                                @if (Route::has('password.request'))
                                    <p><small>
                                        <a href="{{ route('password.request') }}">
                                            {{ __('¿Olvidaste tu contraseña?') }}
                                        </a>
                                    </small></p>
                                @endif
                            </div>
                            <div class="col-sm-4 col-xs-12">
                                <button class="submit-btn-1 btn-hover-1 f-right" type="submit">{{ __('Login') }}</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection