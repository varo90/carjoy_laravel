@extends('layouts/layout')

@section('title','Login | Registro')

@section('content')

<div id="page-content" class="page-wrapper">
    
    <!-- LOGIN SECTION START -->
    <div class="login-section mb-80">
        <div class="container">
            <div class="justify-content-center">
                <div class="card">
                    {{-- <div class="card-header">{{ __('Register') }}</div> --}}

                    <div class="new-customers">
                        <form method="POST" action="{{ route('register') }}">
                            @csrf
                            {{-- <h6 class="widget-title border-left mb-20">{{ __('NUEVO CLIENTE') }}</h6> --}}

                            @if(old('whichform') == 'register')
                                @include('common/errors')
                            @endif
                            <div class="login-account p-30 box-shadow">
                                    {{-- <p>{{ __('Si aún no tienes cuenta, regístrate:') }}</p> --}}
                                <h4>Registro</h4>
                                <input type="hidden" name="whichform" value="register">
                                <input id="email" type="email" name='email' value="{{ (old('whichform') == 'register') ? old('email') : '' }}" maxlength="255" placeholder="E-Mail *">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <input type="text" name="user_name" placeholder="Nombre *" value="{{ old('user_name') }}" minlength="2" maxlength="255" required>
                                    </div>
                                    <div class="col-sm-8">
                                        <input type="text" name="user_surname" placeholder="Apellidos *" value="{{ old('user_surname') }}" minlength="2" maxlength="255" required>
                                    </div>
                                </div>
                                <input type="password" name="password" placeholder="Contraseña *" minlength="8" required>
                                <input type="password" name="password_confirmation" placeholder="Confirma contraseña *" minlength="8" required>
                                <div class="row">
                                    <div class="col-xs-12 terms-conditions-chkbx">
                                        <label>
                                            <input type="checkbox" name="accept-terms" required>
                                            He leído y acepto la <a href="privacy-policy" target="_blank">Política de privacidad</a> y los <a href="sales-terms" target="_blank">Términos y condiciones</a>
                                        </label>
                                    </div>
                                    <div class="col-xs-12">
                                        <button class="submit-btn-1 mt-20 btn-hover-1 f-right" type="submit" value="register">{{ __('REGÍSTRATE') }}</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
