@extends('layouts/layout')

@section('title','Contacto')

@section('content')

<section id="page-content" class="page-wrapper">

    <div class="message-box-section mt--50 mb-80">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="message-box box-shadow white-bg">
                        @if(session()->has('attributes'))
                            <?php $attributes = session()->get('attributes') ?>
                            <h2>¡Mensaje enviado!</h2><br>
                            <p>Muchas gracias por tu mensaje, {{$attributes['name']}}</p>
                            <p>Nos pondremos en contacto contigo lo antes posible a tu dirección de correo: {{$attributes['email']}}</p>
                            <br>
                            <center>
                                <a href="/">Volver al inicio</button>
                            </center>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>

@endsection