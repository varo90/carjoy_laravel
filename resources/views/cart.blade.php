@extends('layouts/layout')

@section('title','Artículos en el carrito')

@section('content')

<!-- Start page content -->
<section id="page-content" class="page-wrapper">

    <!-- SHOP SECTION START -->
    <div class="shop-section mb-80">
        <div class="container">
            <div class="row">
                {{-- <div class="col-md-2 col-sm-12">
                    <ul class="cart-tab">
                        <li>
                            <a class="active" href="#shopping-cart" data-toggle="tab">
                                <span>01</span>
                                Carrito
                            </a>
                        </li>
                        <li>
                            <a href="#wishlist" data-toggle="tab">
                                <span>02</span>
                                Lista de deseos
                            </a>
                        </li>
                        <li>
                            <a href="#checkout" data-toggle="tab">
                                <span>02</span>
                                Envío y pago
                            </a>
                        </li>
                        <li>
                            <a href="#order-complete" data-toggle="tab">
                                <span>03</span>
                                Pedido completado
                            </a>
                        </li>
                    </ul>
                </div> --}}
                <div class="col-md-12 col-sm-12">
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <!-- shopping-cart start -->
                        <div class="tab-pane active" id="shopping-cart">
                            <div class="shopping-cart-content">
                                <div class="table-content table-responsive mb-50">
                                    <table class="text-center">
                                        <thead>
                                            <tr>
                                                <th class="product-thumbnail">Producto</th>
                                                <th class="product-price">Precio</th>
                                                <th class="product-quantity">Cantidad</th>
                                                <th class="product-subtotal">Total</th>
                                                <th class="product-remove">Quitar</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $total_price = 0; ?>
                                            @foreach($shopping_cart as $product)
                                                <tr>
                                                    <td class="product-thumbnail">
                                                        <div class="pro-thumbnail-img">
                                                            <img src="{{ URL::asset($product->path) != NULL ? URL::asset($product->path) : URL::asset('img/cart/1.jpg') }}" alt="{{ $product->name }}">
                                                        </div>
                                                        <div class="pro-thumbnail-info text-left">
                                                            <h6 class="product-title-2">
                                                                <a href="/products/{{$product->slug }}" title="{{$product->name}}">{{ str_limit($product->name, $limit = 41, $end = '...') }}</a>
                                                            </h6>
                                                            {{-- <p>Brand: Brand Name</p>
                                                            <p>Model: Grand s2</p>
                                                            <p> Color: Black, White</p> --}}
                                                        </div>
                                                    </td>
                                                    <td class="product-price">{{ round($product->sell_price,2)  }} €</td>
                                                    <td class="product-quantity">
                                                        <a class="lessquantity_c cquantity {{ ($product->cartquantity <= 1) ? 'red-font' : ''}}" data-id="{{ $product->id_shc }}">-</a> 
                                                        {{ round($product->cartquantity,2)  }}
                                                        <a class="sumquantity_c cquantity {{ ($product->cartquantity >= $product->quantity) ? 'red-font' : ''}}" data-id="{{ $product->id_shc }}">+</a> 
                                                        {{-- <div class="cart-plus-minus f-left">
                                                            <input type="text" value="{{ round($product->cartquantity,2)  }}" name="qtybutton" class="cart-plus-minus-box">
                                                        </div>  --}}
                                                    </td>
                                                    <?php $product_total_price = $product->sell_price*$product->cartquantity; ?>
                                                    <td class="product-subtotal">{{ round($product_total_price,2)  }} €</td>
                                                    <td class="product-remove">
                                                        <meta name="csrf-token-{{$product->id_shc}}" content="{{ csrf_token() }}">
                                                        <a class="delcart_c" data-id="{{ $product->id_shc }}">
                                                            <i class="zmdi zmdi-close"></i>
                                                        </a>
                                                    </td>
                                                </tr>
                                                <?php $total_price += $product_total_price; ?>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <div class="row">
                                    {{-- <div class="col-md-6">
                                        <div class="coupon-discount box-shadow p-30 mb-50">
                                            <h6 class="widget-title border-left mb-20">coupon discount</h6>
                                            <p>Enter your coupon code if you have one!</p>
                                            <input type="text" name="name" placeholder="Enter your code here.">
                                            <button class="submit-btn-1 black-bg btn-hover-2" type="submit">apply coupon</button>
                                        </div>
                                    </div> --}}
                                    <div class="col-md-6">
                                        <div class="payment-details box-shadow p-30 mb-50">
                                            <h6 class="widget-title border-left mb-20">Desglose</h6>
                                            <table>
                                                <tr>
                                                    <td class="td-title-1">Subtotal</td>
                                                    <td class="td-title-2">{{ number_format((float)($price_no_tax = $total_price / ((100+Config::get('constants.options.IVA'))/100)), 2, ',', '.') }} €</td>
                                                </tr>
                                                <tr>
                                                    <td class="td-title-1">IVA</td>
                                                    <td class="td-title-2">{{ number_format((float)($tax = ($total_price - $price_no_tax)), 2, ',', '.') }} €</td>
                                                </tr>
                                                <tr>
                                                    <td class="order-total">Total</td>
                                                    <td class="order-total-price">{{ number_format((float)($total_price), 2, ',', '.') }} €</td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        @if($shopping_cart->count() > 0)
                                            <a href="/checkout" class="button extra-small next f-right" tabindex="-1">
                                                <span class="text-uppercase">Siguiente</span>
                                            </a>
                                        @endif
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        {{-- <div class="culculate-shipping box-shadow p-30">
                                            <h6 class="widget-title border-left mb-20">culculate shipping</h6>
                                            <p>Enter your coupon code if you have one!</p>
                                            <div class="row">
                                                <div class="col-sm-4 col-xs-12">
                                                    <input type="text"  placeholder="Country">
                                                </div>
                                                <div class="col-sm-4 col-xs-12">
                                                    <input type="text"  placeholder="Region / State">
                                                </div>
                                                <div class="col-sm-4 col-xs-12">
                                                    <input type="text"  placeholder="Post code">
                                                </div>
                                                <div class="col-md-12">
                                                    <button class="submit-btn-1 black-bg btn-hover-2">get a quote</button>   
                                                </div>
                                            </div>
                                        </div> --}}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- shopping-cart end -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- SHOP SECTION END -->             

</section>
<!-- End page content -->

@endsection