@extends('admin/layouts/layout')

@section('title','Producto '.$product->slug)
@section('title_page','Editar ' . $product->name)

@section('content')

    <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
    <div id="updated_correctly"></div>
    <div class="product_features">
        <input data-id="{{$product->id}}" type="text" name="name" class="changeable mb-2" value="{{$product->name}}">
        <textarea data-id="{{$product->id}}" name="short_description" class="changeable" rows="2" cols="50">{{$product->short_description}}</textarea>
        <textarea data-id="{{$product->id}}" name="description" class="ckeditor changeable" id="description" rows="10" cols="80">{{$product->description}}</textarea>
    </div>
    <div class="product_images mt-40">
        <h3>Imágenes</h3>
        @foreach($product->Product_image as $product_image)
            <label>
                <input data-id="{{$product->id}}" type="radio" name="is_main_image" class="changeable" value="{{$product_image->id}}" {{ $product_image->is_main_image ? 'checked' : '' }}>
                <div class="container_img">
                    <img class="clickable {{ $product_image->is_main_image ? 'main_image' : '' }}" src="{{ URL::asset($product_image->path) }}" width="150">
                    <button data-id="{{$product_image->id}}" class="btn btn-danger delete_img"><i class="fas fa-times"></i></button>
                </div>
            </label>
        @endforeach
        <div>
            <form class="form-events" method="POST" action="/admin3012/products/images" enctype="multipart/form-data">
                {{ csrf_field() }}
                <input type="hidden" name="product_id" value="{{$product->id}}">
                <div>
                    <div class="form-group">
                        <input required type="file" name="images[]" placeholder="Producto" multiple>
                    </div>
                    <button type="submit" class="btn btn-primary btn-icon-split">
                        <span class="icon text-white-50">
                            <i class="fas fa-upload"></i>
                        </span>
                        <span class="text">Subir imágenes</span>
                    </button>
                </div>
            </form>
            
        </div>
    </div>

    <div class="row mt-40">
        <div class="col-xs-4">
            <h3>Categoría(s)</h3>
            <div id="categories_updated_correctly"></div>
            <select id='pre-selected-categories' data-id="{{$product->id}}" multiple='multiple'>
                @foreach(\App\Category::All() as $category)
                    <option value='{{$category->id}}' {{ in_array($category->id,$selectedCategories) ? 'selected' : '' }}>{{$category->name}}</option>
                @endforeach
            </select>
        </div>
        <div class="col-xs-4">
            <h3>Marca(s)</h3>
            <div id="brands_updated_correctly"></div>
            <select id='pre-selected-brands' data-id="{{$product->id}}" multiple='multiple'>
                @foreach(\App\Brand::All() as $brand)
                    <option value='{{$brand->id}}' {{ in_array($brand->id,$selectedBrands) ? 'selected' : '' }}>{{$brand->name}}</option>
                @endforeach
            </select>
        </div>
    </div>

    <div class="table-responsive mt-40">
        <table class="table table-bordered" width="100%" cellspacing="0">
            <thead>
                <tr>
                    <th># Uds.</th>
                    <th>€ Venta</th>
                    <th>€ Sin descuento</th>
                    <th>% descuento</th>
                    <th>Activo</th>
                    <th>Destacado</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th># Uds.</th>
                    <th>€ Venta</th>
                    <th>€ Sin descuento</th>
                    <th>% descuento</th>
                    <th>Activo</th>
                    <th>Destacado</th>
                </tr>
            </tfoot>
            <tbody>
                <td><input data-id="{{$product->id}}" type="number" name="quantity" class="changeable" value="{{ round($product->quantity) }}"> Uds.</td>
                <td><input data-id="{{$product->id}}" type="number" name="sell_price" class="changeable" value="{{ round($product->sell_price) }}"> €</td>
                <td><input data-id="{{$product->id}}" type="number" name="undiscounted_sell_price" class="changeable" value="{{ round($product->undiscounted_sell_price) }}"> €</td>
                <td><input data-id="{{$product->id}}" type="number" name="percentage_discount" class="changeable" value="{{ round($product->percentage_discount) }}"> %</td>
                <td>
                    <select data-id="{{$product->id}}" name="status" class="changeable">
                        <option value="1" {{ $product->status ? 'selected' : '' }}>Activo</option> 
                        <option value="0" {{ $product->status ? '' : 'selected' }}>Inactivo</option>
                    </select>
                </td>
                <td>
                    <select data-id="{{$product->id}}" name="featured" class="changeable">
                        <option value="1" {{ $product->featured ? 'selected' : '' }}>Sí</option> 
                        <option value="0" {{ $product->featured ? '' : 'selected' }}>No</option>
                    </select>
                </td>
            </tbody>
        </table>
    </div>

@endsection