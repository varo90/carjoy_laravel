<!-- Sidebar -->
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
      <div class="sidebar-brand-icon rotate-n-15">
        <i class="fas fa-laugh-wink"></i>
      </div>
      <div class="sidebar-brand-text mx-3">The backdoor</div>
    </a>

    @if(Auth::User()->role_id == 1)
      <!-- Divider -->
      <hr class="sidebar-divider my-0">

      <!-- Nav Item - Dashboard -->
      <li class="nav-item {{ (Request::is('admin3012')) ? 'active' : '' }}">
        <a class="nav-link" href="/admin3012">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Dashboard</span></a>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Heading -->
      <div class="sidebar-heading">
        Cosas que administrar
      </div>

      <li class="nav-item {{ (Request::is('admin3012/products') || Request::is('admin3012/products/*')) ? 'active' : '' }}">
        <a class="nav-link" href="/admin3012/products">
          <i class="fab fa-product-hunt"></i>
          <span>Productos</span></a>
      </li>

      <li class="nav-item {{ (Request::is('admin3012/categories')) ? 'active' : '' }}">
        <a class="nav-link" href="/admin3012/categories">
          <i class="far fa-folder-open"></i>
          <span>Categorías</span></a>
      </li>

      <li class="nav-item {{ (Request::is('admin3012/brands')) ? 'active' : '' }}">
        <a class="nav-link" href="/admin3012/brands">
          <i class="far fa-copyright"></i>
          <span>Marcas</span></a>
      </li>

      <li class="nav-item {{ (Request::is('admin3012/users')) ? 'active' : '' }}">
        <a class="nav-link" href="/admin3012/users">
          <i class="fas fa-users"></i>
          <span>Usuarios</span></a>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider">
    @endif

    @if(Auth::User()->role_id == 1 || Auth::User()->role_id == 4)
      <!-- Heading -->
      <div class="sidebar-heading">
        Contabilidad
      </div>

      <!-- Nav Item - Charts -->
      <li class="nav-item {{ (Request::is('admin3012/sales')) ? 'active' : '' }}">
        <a class="nav-link" href="admin3012/sales">
          <i class="fas fa-cash-register"></i>
          <span>Ventas</span></a>
      </li>

      <!-- Nav Item - Tables -->
      <li class="nav-item {{ (Request::is('admin3012/consignments')) ? 'active' : '' }}">
        <a class="nav-link" href="admin3012/consignments">
          <i class="fas fa-boxes"></i>
          <span>Pedidos</span></a>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider d-none d-md-block">
    @endif

    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
      <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

  </ul>
  <!-- End of Sidebar -->