@extends('admin/layouts/layout')

@section('title','Nuevo producto')
@section('title_page','Nuevo producto')

@section('content')

        <form id="new_product_form" method="POST" action="/admin3012/products">
            <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
            <div id="updated_correctly"></div>
            <div class="product_features">
                <input type="text" name="name" class="mb-2" value="" placeholder="Dale un nombre...">
            </div>
            <div>
                <button class="btn btn-primary btn-icon-split">
                    <span class="icon text-white-50">
                        <i class="fas fa-plus"></i>
                    </span>
                    <span class="text">Crear</span>
                </button>
            </div>
        </form>

@endsection