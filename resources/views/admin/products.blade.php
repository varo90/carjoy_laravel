@extends('admin/layouts/layout')

@section('title','Productos')
@section('title_page','Productos')

@section('content')

    {{-- <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">
        <h1>Productos</h1>
    </nav> --}}

    <div id="updated_correctly"></div>
    <div class="table-responsive">
        <div>
            @php $results = [10,50,100,200,500,1000]; @endphp
            <form id="filters" method="GET" action="/admin3012/products">
                <label for="res"># Resultados:</label>
                <select id="res" name="res" onchange="submit()">
                    @foreach($results as $result)
                        <option value="{{ $result }}" {{ $result == $selected_results ? 'selected' : '' }}>{{ $result }}</option>
                    @endforeach
                </select>
                <label for="status">Status:</label>
                <select id="status" name="status" onchange="submit()">
                    <option value="10" {{ $status == 10 ? 'selected' : '' }}>Todos</option>
                    <option value="1" {{ $status == 1 ? 'selected' : '' }}>Activos</option>
                    <option value="0" {{ $status == 0 ? 'selected' : '' }}>Inactivos</option>
                </select>
                <label for="featured">Destacados:</label>
                <select id="featured" name="featured" onchange="submit()">
                    <option value="10" {{ $featured == 10 ? 'selected' : '' }}>Todos</option>
                    <option value="1" {{ $featured == 1 ? 'selected' : '' }}>Destacados</option>
                    <option value="0" {{ $featured == 0 ? 'selected' : '' }}>No destacados</option>
                </select>
                {{-- <input type="hidden" name="sortby" value="{{$sortby}}"> --}}

            </form>
        </div>
        <table class="table table-bordered" width="100%" cellspacing="0">
            <thead>
                <tr>
                    <th>Img</th>
                    <th>Id</th>
                    <th>Nombre</th>
                    @php $caret = ''; if($orderby == 'quantity') { if($order == 'DESC') { $caret = '-down'; } else if($order == 'ASC') { $caret = '-up'; } }  @endphp
                    <th data-id="quantity" data-order="{{$orderby == 'quantity' ? $order : ''}}" class="clickable orderby"><i class="fas fa-sort{{ $caret }}"></i> # Uds.</th>

                    @php $caret = ''; if($orderby == 'sell_price') { if($order == 'DESC') { $caret = '-down'; } else if($order == 'ASC') { $caret = '-up'; } }  @endphp
                    <th data-id="sell_price" data-order="{{$orderby == 'sell_price' ? $order : ''}}" class="clickable orderby"><i class="fas fa-sort{{ $caret }}"></i> € Venta</th>

                    @php $caret = ''; if($orderby == 'undiscounted_sell_price') { if($order == 'DESC') { $caret = '-down'; } else if($order == 'ASC') { $caret = '-up'; } }  @endphp
                    <th data-id="undiscounted_sell_price" data-order="{{$orderby == 'undiscounted_sell_price' ? $order : ''}}" class="clickable orderby"><i class="fas fa-sort{{ $caret }}"></i> € Sin descuento</th>

                    @php $caret = ''; if($orderby == 'percentage_discount') { if($order == 'DESC') { $caret = '-down'; } else if($order == 'ASC') { $caret = '-up'; } }  @endphp
                    <th data-id="percentage_discount" data-order="{{$orderby == 'percentage_discount' ? $order : ''}}" class="clickable orderby"><i class="fas fa-sort{{ $caret }}"></i> % descuento</th>
                    <th>Activo</th>
                    <th>Destacado</th>
                    @php $caret = ''; if($orderby == 'created_at') { if($order == 'DESC') { $caret = '-down'; } else if($order == 'ASC') { $caret = '-up'; } }  @endphp
                    <th data-id="created_at" data-order="{{$orderby == 'created_at' ? $order : ''}}" class="clickable orderby"><i class="fas fa-sort{{ $caret }}"></i> Creado</th>

                    @php $caret = ''; if($orderby == 'updated_at') { if($order == 'DESC') { $caret = '-down'; } else if($order == 'ASC') { $caret = '-up'; } }  @endphp
                    <th data-id="updated_at" data-order="{{$orderby == 'updated_at' ? $order : ''}}" class="clickable orderby"><i class="fas fa-sort{{ $caret }}"></i> Ult. Actualización</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th>Img</th>
                    <th>Id</th>
                    <th>Nombre</th>
                    <th># Uds.</th>
                    <th>€ Venta</th>
                    <th>€ Sin descuento</th>
                    <th>% descuento</th>
                    <th>Activo</th>
                    <th>Destacado</th>
                    <th>Creado</th>
                    <th>Ult. Actualización.</th>
                </tr>
            </tfoot>
            <tbody>
                <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                @foreach($products as $product)
                    <tr>
                        <td class="update_product clickable" data-id="{{$product->id}}"><img src="{{ $product->path != NULL ? URL::asset($product->path) : URL::asset('img/product/1.jpg') }}" width="70"></td>
                        <td class="update_product clickable" data-id="{{$product->id}}">{{ $product->id }} </td>
                        <td class="update_product clickable" data-id="{{$product->id}}">{{ $product->name }}</td>
                        <td><input data-id="{{$product->id}}" type="number" name="quantity" class="changeable" value="{{ round($product->quantity) }}"> Uds.</td>
                        <td><input data-id="{{$product->id}}" type="number" name="sell_price" class="changeable" value="{{ round($product->sell_price) }}"> €</td>
                        <td><input data-id="{{$product->id}}" type="number" name="undiscounted_sell_price" class="changeable" value="{{ round($product->undiscounted_sell_price) }}"> €</td>
                        <td><input data-id="{{$product->id}}" type="number" name="percentage_discount" class="changeable" value="{{ round($product->percentage_discount) }}"> %</td>
                        <td>
                            <select data-id="{{$product->id}}" name="status" class="changeable">
                                <option value="1" {{ $product->status ? 'selected' : '' }}>Activo</option> 
                                <option value="0" {{ $product->status ? '' : 'selected' }}>Inactivo</option>
                            </select>
                        </td>
                        <td>
                            <select data-id="{{$product->id}}" name="featured" class="changeable">
                                <option value="1" {{ $product->featured ? 'selected' : '' }}>Sí</option> 
                                <option value="0" {{ $product->featured ? '' : 'selected' }}>No</option>
                            </select>
                        </td>
                        <td>{{ date('H:i:s d/m/Y',strtotime($product->created_at)) }}</td>
                        <td>{{ date('H:i:s d/m/Y',strtotime($product->updated_at)) }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        <div class="shop-pagination box-shadow text-center ptblr-10-30">
            {{ $products->links() }}
        </div>
        <div>
            <a href="/admin3012/products/create" class="btn btn-primary btn-icon-split f-right" target="_blank">
                <span class="icon text-white-50">
                    <i class="fas fa-plus"></i>
                </span>
                <span class="text">Añadir nuevo</span>
            </a>
        </div>
    </div>

@endsection