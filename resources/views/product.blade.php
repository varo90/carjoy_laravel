@extends('layouts/layout')

@section('title',$product->name)

@section('content')
<?php 
    use App\User;
?>
    <section id="page-content" class="page-wrapper">
        {{-- {{dd($product->Product_images)}} --}}
        <div id="product-page" class="container">
            <div class="row">
                <div class="col-md-9 col-xs-12">
                    <!-- single-product-area start -->
                    <div class="single-product-area mb-80">
                        <div class="row">
                            <!-- imgs-zoom-area start -->
                            <div class="col-md-5 col-sm-5 col-xs-12">
                                <div class="imgs-zoom-area">
                                    <img id="zoom_03" src="{{ URL::asset($product->path) }}" data-zoom-image="{{ URL::asset($product->path) }}" alt="{{$product->name}}">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div id="gallery_01" class="carousel-btn slick-arrow-3 mt-30">
                                                @foreach($product->Product_image as $image)
                                                    <div class="p-c">
                                                        <a data-image="{{ URL::asset($image->path) }}" data-zoom-image="{{ URL::asset($image->path) }}">
                                                            <img class="zoom_03" src="{{ URL::asset($image->path) }}" alt="{{$product->name}}">
                                                        </a>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- imgs-zoom-area end -->
                            <!-- single-product-info start -->
                            <div class="col-md-7 col-sm-7 col-xs-12"> 
                                <div class="single-product-info">
                                    <h2 class="text-black-1">{{ $product->name}} </h2>
                                    {{-- <h6 class="brand-name-2">brand name</h6> --}}
                                    
                                    <hr>
                                    <!-- single-pro-color-rating -->
                                    {{-- <div class="single-pro-color-rating clearfix">
                                        <div class="sin-pro-color f-left">
                                            <p class="color-title f-left">Color</p>
                                            <div class="widget-color f-left">
                                                <ul>
                                                    <li class="color-1"><a href="#"></a></li>
                                                    <li class="color-2"><a href="#"></a></li>
                                                    <li class="color-3"><a href="#"></a></li>
                                                    <li class="color-4"><a href="#"></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div> --}}
                                    @php 
                                        $sell_price = $product->Product_sell_price->last()->sell_price;
                                        $undiscounted_sell_price = $product->Product_sell_price->last()->undiscounted_sell_price;
                                    @endphp
                                    <div class="s-price-box">
                                        <h3 class="pro-price">
                                            Precio: {{ number_format((float)($sell_price), 2, ',', '.').' €' }}
                                            <span class="old-price">{{ (!is_null($undiscounted_sell_price) && $undiscounted_sell_price > $sell_price) ? number_format(($undiscounted_sell_price), 2, ',', '.').'€' : '' }}</span>
                                        </h3>
                                    </div>
                                    
                                    <hr>
                                    <!-- plus-minus-pro-action -->
                                    <div class="plus-minus-pro-action clearfix">
                                        <div class="sin-plus-minus f-left clearfix">
                                            <p class="color-title f-left">Uds.</p>
                                            <div class="f-left">
                                                <input id="qty" name="qty" class="" type="number" value="{{ $product->recommended_quantity }}"  min="0" max="{{ $product->quantity }}" step="1">
                                            </div>   
                                        </div>
                                        {{-- <div class="sin-pro-action f-right">
                                            <ul class="action-button">
                                                <li>
                                                    <a id="wishlist__{{$product->id}}" class="btn_pr wishlist_btn" title="Wishlist"><i class="zmdi zmdi-favorite"></i></a>
                                                </li>
                                                <li>
                                                    <a href="#" data-toggle="modal" data-target="#productModal" title="Quickview" tabindex="0"><i class="zmdi zmdi-zoom-in"></i></a>
                                                </li>
                                                <li>
                                                    <a href="#" title="Compare" tabindex="0"><i class="zmdi zmdi-refresh"></i></a>
                                                </li>
                                                <li>
                                                    <a href="#" title="Add to cart" tabindex="0"><i class="zmdi zmdi-shopping-cart-plus"></i></a>
                                                </li>
                                            </ul>
                                        </div> --}}
                                    </div>
                                    <hr>
                                    <div class="inline">
                                        <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                                        <input type="hidden" name="prdd" id="prdd" value="{{ $product->id }}">
                                        <a href="/quickbuy?p={{ $product->id }}&quantity={{ $product->recommended_quantity }}" class="button quick_buy" tabindex="-1">
                                            <i class="zmdi zmdi-forward"></i><span class="text-uppercase">COMPRA RÁPIDA</span>
                                        </a>
                                        <a class="button single_add_to_cart_button" tabindex="-1">
                                            <i class="zmdi zmdi-shopping-cart-plus"></i><span class="text-uppercase">AÑADIR AL CARRITO</span>
                                        </a>
                                        <div id="added_to_wishlist">
                                            @if($en_wishlist == 0)
                                                <a class="button single_add_to_wishlist_button" title="Añadir a la lista de deseos" tabindex="-1">
                                                    <i class="zmdi zmdi-favorite"></i>
                                                </a>
                                            @else
                                                <p><i class="zmdi zmdi-favorite"></i> En tu lista de deseos</p>
                                            @endif
                                        </div>
                                    </div>
                                    
                                    {{-- <hr>
                                    <div>
                                        <a href="#" class="button extra-small button-black" tabindex="-1">
                                            <span class="text-uppercase">Buy now</span>
                                        </a>
                                    </div> --}}
                                </div>
                            </div>
                            <!-- single-product-info end -->
                        </div>
                        <!-- single-product-tab -->
                        <div class="row">
                            <div class="col-md-12">
                                <!-- hr -->
                                <hr>
                                <div class="single-product-tab">
                                    <ul class="reviews-tab mb-20">
                                        <li  class="active"><a href="#description" data-toggle="tab">Descripción</a></li>
                                        <li ><a href="#information" data-toggle="tab">Información</a></li>
                                        {{-- <li ><a href="#reviews" data-toggle="tab">Valoraciones</a></li> --}}

                                        {{-- <div class="pro-rating sin-pro-rating f-right">
                                            @php $star = $product->reviews_avg;  @endphp
                                            @include('common/rating')
                                            <span class="text-black-5">( {{ $product->Product_review->count() }} Valoraciones )</span>
                                        </div> --}}
                                    </ul>

                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane active" id="description">
                                                {!! $product->description !!}
                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="information">
                                            {{ $product->short_description }}
                                        </div>
                                        {{-- <div role="tabpanel" class="tab-pane" id="reviews">
                                            <!-- reviews-tab-desc -->
                                            <div class="reviews-tab-desc">
                                                <!-- single comments -->
                                                @foreach($product->Product_review as $review)
                                                    <div class="media mt-30">
                                                        <div class="media-left">
                                                            <a href="#"><img class="media-object" src="{{ URL::asset('img/author/1.jpg') }}" alt="#"></a>
                                                        </div>
                                                        <div class="media-body">
                                                            <div class="clearfix">
                                                                <div class="name-commenter pull-left">
                                                                    <h6 class="media-heading">
                                                                        {{ User::find($review->user_id)->user_name }}</h6>
                                                                    <p class="mb-10">{{ date('d-m-Y \a \l\a\s H:i',strtotime($review->created_at)) }}</p>
                                                                </div>
                                                                <div class="pull-right">
                                                                    <div class="pro-rating sin-pro-rating f-right">
                                                                        @php $star = $review->vote; @endphp
                                                                        @include('common/rating')
                                                                    </div>
                                                                    <ul class="reply-delate">
                                                                        <li><a href="#">Reply</a></li>
                                                                        <li>/</li>
                                                                        <li><a href="#">Delate</a></li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                            <p class="mb-0">{{ $review->review }}</p>
                                                        </div>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div> --}}
                                    </div>
                                </div>
                                <!--  hr -->
                                <hr>
                            </div>
                        </div>
                    </div>
                    <!-- single-product-area end -->
                    @if($related_products->count() > 0)
                        <div class="related-product-area">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="section-title text-left mb-40">
                                        <h2 class="uppercase">Productos relacionados</h2>
                                        {{-- <h6>There are many variations of passages of brands available,</h6> --}}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="active-related-product">
                                    @foreach($related_products as $product)
                                        <div class="col-xs-12">
                                            @include('parts/item')
                                        </div>
                                    @endforeach
                                </div>   
                            </div>
                        </div>
                    @endif
                </div>
                <div class="col-md-3 col-xs-12">
                    <!-- widget-product -->
                    @if($related_products->count() > 0)
                        <aside class="widget widget-product box-shadow">
                            <h6 class="widget-title border-left mb-20">Productos relacionados</h6>
                            <!-- product-item start -->
                            @foreach($related_products as $count => $product)
                                <div class="product-item">
                                    <div class="product-img">
                                        <a href="/products/{{$product->slug}}">
                                            <img src="{{ URL::asset($product->path) }}" alt="{{$product->name}}"/>
                                        </a>
                                    </div>
                                    <div class="product-info">
                                        <h6 class="product-title">
                                            <a href="/products/{{$product->slug}}">{{$product->name}}</a>
                                        </h6>
                                        <h3 class="pro-price">{{$product->sell_price}} €</h3>
                                    </div>
                                </div>
                                @php if($count > 3) break; @endphp
                            @endforeach
                            <!-- product-item end -->
                        </aside>
                    @endif
                </div>
            </div>
        </div>
    </section>
        
@endsection