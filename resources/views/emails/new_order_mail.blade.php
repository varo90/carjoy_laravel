@component('mail::message')
Tu pedido en www.emblemasparacoche.com
@php
    $order = App\Order::get_order_details($order_id);
    $shipping_method = App\Shipping_method::find($order->first()->shipping_method_id);
    $shipping_address = App\Order_address::find($order->first()->shipment_address_id);
    $billing_address = $order->first()->shipment_address_id == $order->first()->billing_address_id ? $shipping_address : App\Order_address::find($order->first()->billing_address_id);
@endphp

<div class="address_tag">
    <div class="row">
        @php $equal_addresses = $order->first()->shipment_address_id == $order->first()->billing_address_id; @endphp
        <div class="col-sm-6 col-xs-12">
            <b><u>Dirección de envío{{ $equal_addresses ? ' y facturación' : '' }}:</u></b><br>
            {{ $shipping_address->user_name }} {{ $shipping_address->user_surname }}<br>
            {{ $shipping_address->address1 }}<br>
            {{ $shipping_address->postcode }} {{ $shipping_address->city }}<br>
            {!! $shipping_address->country_id != 69 ? \App\Country::find($shipping_address->country_id)->name_spa . '<br>' : '' !!}
            {{ $shipping_address->nif }}<br>
        </div>
        <div class="col-sm-6 col-xs-12">
            @if(!$equal_addresses)
                <b><u>Dirección de facturación:</u></b><br>
                {{ $billing_address->user_name }} {{ $billing_address->user_surname }}<br>
                {{ $billing_address->address1 }}<br>
                {{ $billing_address->postcode }} {{ $billing_address->city }}<br>
                {!! $billing_address->country_id != 69 ? \App\Country::find($billing_address->country_id)->name_spa . '<br>' : '' !!}
                {{ $billing_address->nif }}<br>
            @endif
        </div>
    </div>
</div>

@component('mail::table')
    |Nombre | €/Ud | Uds | | Subtotal |
    |:----: | -----:| ---: | | --------:|
    @foreach($order as $product)
        |{{$product->name}} | {{number_format((float)($product->unit_price), 2, ',', '.')}} | {{round($product->order_quantity)}} | | {{ number_format((float)($product_total_price = $product->unit_price*$product->order_quantity), 2, ',', '.') }} €|
    @endforeach
    |Total del pedido (Con IVA) | | | | {{ number_format((float)($order->first()->total_price - $order->first()->shipping_price), 2, ',', '.') }} €|
    |Envío ({{$shipping_method->name}}) | | | | {{ number_format((float)($shipment = $order->first()->shipping_price), 2, ',', '.') }} €|
    |Total | | | | {{ $grand_total = number_format((float)($order->first()->total_price), 2, ',', '.') }} €|
@endcomponent

@component('mail::button', ['color' => 'mybutton', 'url' => url('/account')])
Verlo en Mi cuenta
@endcomponent

Gracias,<br>
{{ config('app.name') }}
@endcomponent
