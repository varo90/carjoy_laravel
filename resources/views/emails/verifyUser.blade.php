@component('mail::message')
Hola {{$user['user_name']}},

Te has registrado en emblemasparacoche.com. Ahora tan sólo tienes que verificar tu cuenta:

@component('mail::button', ['color' => 'mybutton','url' => url('user/verify', $user->verifyUser->token)])
Verificar Email
@endcomponent

Gracias,<br>
{{ config('app.name') }}
@endcomponent