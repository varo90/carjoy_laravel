@component('mail::message')
Hola {{ $attributes['name'] }},

Muchas gracias por ponerte en contacto con nosotros, te responderemos a la mayor brevedad.

Copia de tu mensaje:

{{$attributes['subject']}}

{{$attributes['message']}}

@component('mail::button', ['url' => 'emblemasparacoche.es'])
Tu cuenta de usuario
@endcomponent

Gracias,<br>
{{ config('app.name') }}
@endcomponent
