@extends('layouts/layout')

@section('title','Política de privacidad')

@section('content')

<div id="page-content" class="page-wrapper">

    <div class="shop-section mb-80">
        <div class="container privacy-policy">

            <h1>POLÍTICA DE PRIVACIDAD</h1>
        
            <p>Por medio de este sitio web, se recogen datos de carácter personal necesarios para la gestión y mantenimiento de algunos de nuestros servicios.</p>
            <p>Le informamos que EL TITULAR DEL SITIO WEB (Responsable del tratamiento) cumple con la Ley Orgánica 3/2018, de 5 de diciembre, de Protección de Datos de Carácter Personal (en adelante, LOPD), el Reglamento UE 2016/679, de Protección de Datos (en adelante, RGPD) y demás normativa vigente y de aplicación en cada momento, velando por garantizar un correcto uso y tratamiento de los datos personales del usuario.</p>
            <p>De conformidad con el artículo 13 del RGPD y artículo 11 LOPD, esta Política de Privacidad se aplica a los tratamientos de datos de carácter personal que THE MACNIFICOS STORE, S.L., realiza como Responsable del tratamiento, en relación con los datos que los usuarios y/o clientes (personas físicas) facilitan como consecuencia de la solicitud de información y/o contratación de los servicios y/o productos que presta y comercializa a través de su web, o recabados en cualquiera de las secciones del sitio web.</p>
            <h2>IDENTIDAD DEL RESPONSABLE DEL TRATAMIENTO DE SUS DATOS PERSONALES</h2>
            <ul>
                <li>Denominación Social: THE MACNIFICOS STORE, S.L.</li>
                <li>Marca comercial: THE MACNIFICOS</li>
                <li>Domicilio: C/ GIRONA, 157-159, 08037 BARCELONA</li>
                <li>CIF: B64673155</li>
                <li>Email: <a href="mailto:info@macnificos.com">info@macnificos.com</a></li>
                <li>Delegado de protección de Datos (DPO): SERVICIOS NORMATIVOS LEGALES, S.L. <a href="mailto:dpo@sibgroup.es">dpo@sibgroup.es</a></li>
            </ul>
            <h2>FINALIDADES DEL TRATAMIENTO DE SUS DATOS PERSONALES</h2>
            <p>Sus datos personales se utilizarán con la finalidad genérica de la gestión y control de la relación comercial establecida y, específicamente para:</p>
            <ul>
                <li>Gestionar el acceso completo y la utilización correcta de los servicios y/o productos por parte de los usuarios de los mismos.</li>
                <li>Para comunicar con los usuarios en respuesta a incidencias, solicitudes, comentarios y preguntas que nos realice a través de los servicios y/o productos, así como a los formularios de contacto de nuestra página web (incluidos, correos electrónicos y/o las llamadas telefónicas).</li>
                <li>Para proporcionar, actualizar, mantener y proteger los servicios y/o productos y actividades.</li>
                <li>Para ofrecer nuevos servicios y/o productos, ofertas especiales o actualizaciones.</li>
                <li>En su caso, gestionar procesos de selección de personal en procesos selectivos de trabajadores y/o colaboradores.</li>
                <li>Comunicaciones: Podremos enviarle correos electrónicos, mensajes y otros tipos de comunicación en referencia a los servicios y/o productos, cuestiones técnicas y cambios en los mismos. Estas comunicaciones se consideran parte de los servicios y/o productos y no puede renunciar a ellas.</li>
                <li>Comunicaciones Comerciales (Marketing): Podremos utilizar sus datos para contactarle, tanto por vía electrónica como no electrónica, para realizar encuestas, obtener su opinión sobre el servicio prestado, y, ocasionalmente, para notificarle cambios, desarrollos importantes de los servicios y/o productos, ofertas y/o promociones de nuestros servicios y/o productos o de terceras empresas relacionadas con nosotros. Estas ofertas comerciales serán, en todo caso, autorizadas de forma expresa y separada por el usuario que, puede revocar en cualquier momento su consentimiento a recibir estas notificaciones utilizando el mecanismo implementado al efecto en las mismas, o bien, desde las preferencias de la cuenta de cliente.</li>
            </ul>
            <p>No trataremos sus datos personales para ninguna otra finalidad más allá de las descritas anteriormente salvo que venga impuesto por ley o exista algún requerimiento judicial.</p>
            <h2>PLAZO DE CONSERVACIÓN</h2>
            <p>Los datos personales proporcionados se conservarán y tratarán mientras se mantenga la relación de prestación de servicios y/o productos, sin perjuicio de la posibilidad de ejercitar sus derechos en materia de protección de datos (acceso, rectificación, supresión, oposición, limitación, portabilidad y a no ser objeto de decisiones individuales automatizadas).</p>
            <p>Los datos personales de las personas interesadas en la recepción de información sobre los servicios y/o productos se mantendrán en el sistema de forma indefinida en tanto el interesado no solicite su supresión.</p>
            <h2>LEGITIMACIÓN DEL TRATAMIENTO.</h2>
            <p>THE MACNIFICOS STORE, S.L. trata sus datos personales de acuerdo con las dos (2) siguientes bases legales: Consentimiento del interesado (1) y el interés legítimo (2).</p>
            <ol>
                <li>El consentimiento del interesado prestado para las finalidades anteriormente descritas, que se solicitará:</li>
            </ol>
            <ul>
                <li>Antes de proceder a tratar sus datos,</li>
                <li>En el proceso de registro como cliente o,</li>
                <li>En el momento de enviar comunicaciones de THE MACNIFICOS STORE, S.L.</li>
            </ul>
            <p>Las solicitudes de información que nos haga llegar requieren que el interesado nos facilite de forma voluntaria los datos necesarios para poder atenderle o prestarle los servicios y/o productos. No obstante, el interesado puede libremente negarse a facilitarnos esos datos o, posteriormente, revocar el consentimiento previamente otorgado para tratar sus datos, si bien esa negativa implicará la imposibilidad de que podamos atender su petición.</p>
            <ol start="2">
                <li>El Interés legítimo de THE MACNIFICOS STORE, S.L. en mantener y fidelizar a sus clientes y/o usuarios y atender mejor sus expectativas o intereses previamente manifestados, ej.: mejorar productos, gestionar solicitudes, consultas o reclamaciones, ofrecer productos similares a los contratados, informar sobre promociones etc., sin perjuicio del cumplimiento por parte de THE MACNIFICOS STORE, S.L. del resto de obligaciones relativas al envío de comunicaciones comerciales por vía electrónica.</li>
            </ol>
            <p>THE MACNIFICOS STORE, S.L. entiende que, al facilitarnos esos datos, el interesado garantiza y se hace responsable de la veracidad, actualidad y exactitud de los mismos y que acepta y consiente expresamente en su tratamiento para las finalidades antes descritas.</p>
            <h2>DESTINATARIOS DE CESIONES O TRANSFERENCIAS.</h2>
            <p>Con carácter general, THE MACNIFICOS STORE, S.L. no cederá sus datos personales a terceros salvo que estemos obligados legalmente a ello o usted nos lo haya autorizado expresamente al hacer uso de nuestros servicios y/o productos.</p>
            <h2>DERECHOS DE LAS PERSONAS INTERESADAS.</h2>
            <p>La normativa de protección de datos permite que puedas ejercer ante el responsable del tratamiento tus derechos de acceso, rectificación, oposición, supresión (“derecho al olvido”), limitación del tratamiento, portabilidad y de no ser objeto de decisiones individualizadas:</p>
            <ul>
                <li>Derecho de acceso: Permite al interesado conocer y obtener información sobre sus datos de carácter personal sometidos a tratamiento.</li>
                <li>Derecho de rectificación: Permite corregir errores, modificar los datos que resulten ser inexactos o incompletos y garantizar la certeza de sus datos personales.</li>
                <li>Derecho de supresión: Permite solicitar la eliminación de los datos objeto de tratamiento cuando ya no sean necesarios para la ejecución o prestación del servicio.</li>
                <li>Derecho de oposición: Permite al interesado que no se lleve a cabo el tratamiento de sus datos de carácter personal o su cese, salvo motivos legítimos o para el ejercicio o defensa de posibles reclamaciones, en cuyo caso los mantendremos bloqueados durante el plazo correspondiente.</li>
                <li>Derecho de oposición al envío de publicidad: Los interesados podrán oponerse al envío de comunicaciones comerciales. En ese caso puede revocar en cualquier momento su consentimiento a recibir estas notificaciones.</li>
                <li>Limitación del tratamiento: En determinadas circunstancias, los interesados podrán solicitar la limitación del tratamiento de sus datos, en cuyo caso únicamente se conservarán para el ejercicio o defensa de reclamaciones.</li>
                <li>Portabilidad de los datos: las personas interesadas pueden solicitar recibir los datos que le incumban y que nos haya facilitado o que –siempre que técnicamente sea posible– se los enviemos a otro responsable de tratamiento de su elección, en un formato estructurado de uso común y lectura mecánica.</li>
                <li>Derecho a no ser objeto de decisiones individuales automatizadas (incluida la elaboración de perfiles): derecho a no ser objeto de una decisión basada en el tratamiento automatizado que produzca efectos o afecte significativamente.</li>
                <li>Posibilidad de retirar el consentimiento: El interesado tiene derecho a retirar en cualquier momento el consentimiento prestado, sin que ello afecte a la licitud del tratamiento basado en el consentimiento prestado en el momento de facilitarnos sus datos.</li>
            </ul>
            <p>Si desea ejercer alguno de los derechos expuestos, rogamos que se ponga en contacto con nosotros a través de los datos de contacto que aparecen en “Identificación del Responsable del tratamiento de los datos personales”, en el que incluya la siguiente información:  Nombre y apellidos, número de DNI, dirección postal y electrónica de contacto, indicando el tipo de derecho y especificando las condiciones del mismo.</p>
            <p>Posibilidad de reclamar ante la Autoridad de Control: THE MACNIFICOS STORE, S.L. le informa igualmente del derecho que le asiste de presentar una reclamación ante la Agencia Española de Protección de Datos (<a href="http://www.agpd.es">www.agpd.es</a>) si considera que el tratamiento no se ajusta a la normativa vigente.</p>
            <h2>INFORMACIÓN ADICIONAL.</h2>
            <p>Conservación de determinados datos</p>
            <p>THE MACNIFICOS STORE, S.L. informa que, en cumplimiento de las disposiciones de la Ley 25/2007, de 18 de octubre, de conservación de datos relativos a las comunicaciones electrónicas y a las redes públicas de comunicaciones, deberá proceder a retener y conservar determinados datos de tráfico generados durante el desarrollo de las comunicaciones con la finalidad de cederlos a las autoridades legitimadas, cuando concurran las circunstancias legales en ella previstas.</p>
            <p>Datos registrados</p>
            <p>Nuestros servidores recopilan información de manera automática cuando el usuario utiliza los servicios de las webs. Estos datos de registro pueden incluir la dirección IP, la identificación del dispositivo desde el que se accede a los productos, el sistema operativo, la configuración del dispositivo.</p>
            <p>Medidas de seguridad</p>
            <p>Además, se le informa que EL TITULAR DEL SITIO WEB (Responsable del tratamiento) tiene implantadas las medidas de seguridad de índole técnica y organizativas necesarias para garantizar la seguridad de sus datos de carácter personal y evitar su alteración, pérdida y tratamiento y/o acceso no autorizado, habida cuenta del estado de la tecnología, la naturaleza de los datos almacenados y los riesgos a que están expuestos, ya provengan de la acción humana o del medio físico o natural. Asimismo, se ha establecido medidas adicionales en orden a reforzar la confidencialidad e integridad de la información en su organización. Manteniendo continuamente la supervisión, control y evaluación de los procesos para asegurar el respeto a la privacidad de los datos.</p>
        </div>
    </div>
</div>

@endsection