
    <!-- START FOOTER AREA -->
    <footer id="footer" class="footer-area">
        <div class="footer-top">
            <div class="container-fluid">
                <div class="plr-185">
                    <div class="footer-top-inner gray-bg">
                        <div class="row">
                            <div class="col-lg-4 col-md-5 col-sm-4">
                                <div class="single-footer footer-about">
                                    <div class="footer-logo">
                                        <img src="{{ URL::asset('img/logo/logo.png') }}" alt="">
                                    </div>
                                    <div class="footer-brief">
                                        <p>Somos especialistas en la venta de emblemas, placas, insignias, pegatinas, etc, de diversas marcas.</p>
                                        <p>Nos distingue la atención al cliente para asesorar y solucionar dudas y problemas, así como la experiencia y conocimiento en profundidad de los artículos que vendemos.</p>
                                    </div>
                                    {{-- <ul class="footer-social">
                                        <li>
                                            <a class="facebook" href="" title="Facebook"><i class="zmdi zmdi-facebook"></i></a>
                                        </li>
                                        <li>
                                            <a class="google-plus" href="" title="Google Plus"><i class="zmdi zmdi-google-plus"></i></a>
                                        </li>
                                        <li>
                                            <a class="twitter" href="" title="Twitter"><i class="zmdi zmdi-twitter"></i></a>
                                        </li>
                                        <li>
                                            <a class="rss" href="" title="RSS"><i class="zmdi zmdi-rss"></i></a>
                                        </li>
                                    </ul> --}}
                                </div>
                            </div>
                            <div class="col-lg-2 hidden-md hidden-sm">
                                <div class="single-footer">
                                    <h4 class="footer-title border-left">Productos</h4>
                                    <ul class="footer-menu">
                                        <li>
                                            <a href="/search?orderBy=newest"><i class="zmdi zmdi-circle"></i><span>Nuevos</span></a>
                                        </li>
                                        <li>
                                            <a href="/search?orderBy=discount"><i class="zmdi zmdi-circle"></i><span>Mejores descuentos</span></a>
                                        </li>
                                        <li>
                                            <a href="/search?orderBy=best_seller"><i class="zmdi zmdi-circle"></i><span>Best Sellers</span></a>
                                        </li>
                                        {{-- <li>
                                            <a href="#"><i class="zmdi zmdi-circle"></i><span>Popular Products</span></a>
                                        </li>
                                        <li>
                                            <a href="#"><i class="zmdi zmdi-circle"></i><span>Manufactirers</span></a>
                                        </li>
                                        <li>
                                            <a href="#"><i class="zmdi zmdi-circle"></i><span>Suppliers</span></a>
                                        </li>
                                        <li>
                                            <a href="#"><i class="zmdi zmdi-circle"></i><span>Special Products</span></a>
                                        </li> --}}
                                    </ul>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-3 col-sm-4">
                                <div class="single-footer">
                                    <h4 class="footer-title border-left">Políticas</h4>
                                    <ul class="footer-menu">
                                        <li>
                                            <a href="/returns_policy"><i class="zmdi zmdi-circle"></i><span>De devoluciones</span></a>
                                        </li>
                                        <li>
                                            <a href="/shipments_policy"><i class="zmdi zmdi-circle"></i><span>De envíos</span></a>
                                        </li>
                                        <li>
                                            <a href="/privacy_policy"><i class="zmdi zmdi-circle"></i><span>De privacidad</span></a>
                                        </li>
                                        <li>
                                            <a href="/sales_terms"><i class="zmdi zmdi-circle"></i><span>T&C</span></a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4">
                                <div class="single-footer">
                                    <h4 class="footer-title border-left">Solucionamos</h4>
                                    <ul class="footer-menu">
                                        <li>
                                            <a href="/contact"><i class="zmdi zmdi-circle"></i><span>Contáctanos</span></a>
                                        </li>
                                    </ul>
                                    {{-- <div class="footer-message">
                                        <form action="#">
                                            <input type="text" name="name" placeholder="Your name here...">
                                            <input type="text" name="email" placeholder="Your email here...">
                                            <textarea class="height-80" name="message" placeholder="Your messege here..."></textarea>
                                            <button class="submit-btn-1 mt-20 btn-hover-1" type="submit">submit message</button> 
                                        </form>
                                    </div> --}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-bottom black-bg">
            <div class="container-fluid">
                <div class="plr-185">
                    <div class="copyright">
                        <div class="row">
                            <div class="col-sm-6 col-xs-12">
                                <div class="copyright-text">
                                    <p>&copy; <a href="https://freethemescloud.com/" target="_blank">Free themes Cloud</a> 2016. All Rights Reserved.</p>
                                </div>
                            </div>
                            <div class="col-sm-6 col-xs-12">
                                <ul class="footer-payment text-right">
                                    <li>
                                        <a href="#"><img src="{{ URL::asset('img/payment/1.jpg') }}" alt=""></a>
                                    </li>
                                    <li>
                                        <a href="#"><img src="{{ URL::asset('img/payment/2.jpg') }}" alt=""></a>
                                    </li>
                                    <li>
                                        <a href="#"><img src="{{ URL::asset('img/payment/3.jpg') }}" alt=""></a>
                                    </li>
                                    <li>
                                        <a href="#"><img src="{{ URL::asset('img/payment/4.jpg') }}" alt=""></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- END FOOTER AREA -->
</div>
<!-- Body main wrapper end -->


<!-- Placed JS at the end of the document so the pages load faster -->

<!-- jquery latest version -->
<script src="{{ URL::asset('js/vendor/jquery-3.1.1.min.js') }}"></script>
<!-- jquery-ui latest version -->
<script src="{{ URL::asset('js/vendor/jquery-ui.min.js') }}"></script>
<!-- Bootstrap framework js -->
<script src="{{ URL::asset('js/bootstrap.min.js') }}"></script>
<!-- Bootbox framework js -->
<script src="{{ URL::asset('js/bootbox.min.js') }}"></script>
<!-- jquery.nivo.slider js -->
<script src="{{ URL::asset('lib/js/jquery.nivo.slider.js') }}"></script>
<!-- All js plugins included in this file. -->
<script src="{{ URL::asset('js/plugins.js') }}"></script>
<!-- Main js file that contents all jQuery plugins activation. -->
<script src="{{ URL::asset('js/main.js') }}"></script>
<!-- Custom js properties. -->
<script src="{{ URL::asset('js/custom.js') }}"></script>

    <div class="cookiewarning bg-warning">
        {{-- <h3 class="white_font">Stiamo utilizzando i cookie</h3> --}}
        <div class="row">
            <div class="col-xs-12 col-sm-11">
                <p class="white_font">Utilizamos cookies para asegurar que damos la mejor experiencia al usuario en nuestro sitio web, siempre de acuerdo con nuestra <a href="/privacy_policy" target="_blank">política de privacidad</a>. Si continúa utilizando este sitio asumiremos que está de acuerdo.</p>
            </div>
            <div class="col-xs-12 col-sm-1">
                <span class="btn btn-warning btn-xl">Aceptar</span>
            </div>
        </div>
    </div>
</body>

</html>
