<!doctype html>
<html class="no-js" lang="es">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>@yield('title')</title>
    <meta name="description" content="¡Tal y como suena! Si necesitas sustituir un emblema de tu vehículo, o prefieres cambiarlo para dar a tu coche un toque personal; ya sea en las ruedas, el capó, el maletero, las llaves, etc. Con nosotros aciertas, disponemos de las insignias que necesitas para que tu vehículo parezca nuevo y que además puedas presumir de marca.">
    <meta name="keywords" content="coche, vehículo, emblema, logo, insignia, bmw, mercedes, citroen, audi, mini">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="REFRESH" content="{{120*60}}">

    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="{{ URL::asset('img/icon/favicon.png') }}">

    <!-- All CSS Files -->
    <!-- Bootstrap fremwork main css -->
    <link rel="stylesheet" href="{{ URL::asset('css/bootstrap.min.css') }}">
    <!-- jquery-ui -->
    <link rel="stylesheet" href="{{ URL::asset('css/jquery-ui.min.css') }}">
    <!-- Nivo-slider css -->
    <link rel="stylesheet" href="{{ URL::asset('lib/css/nivo-slider.css') }}">
    <!-- This core.css file contents all plugings css file. -->
    <link rel="stylesheet" href="{{ URL::asset('css/core.css') }}">
    <!-- Theme shortcodes/elements style -->
    <link rel="stylesheet" href="{{ URL::asset('css/shortcode/shortcodes.css') }}">
    <!-- Theme main style -->
    <link rel="stylesheet" href="{{ URL::asset('css/style.css') }}">
    <!-- Responsive css -->
    <link rel="stylesheet" href="{{ URL::asset('css/responsive.css') }}">
    <!-- Template color css -->
    <link rel="stylesheet" href="{{ URL::asset('css/color/color-core.css') }}" data-style="styles">
    <!-- User style -->
    <link rel="stylesheet" href="{{ URL::asset('css/custom.css') }}">

    <!-- Modernizr JS -->
    <script src="{{ URL::asset('js/vendor/modernizr-2.8.3.min.js') }}"></script>
    <script>
        product_tab_section = 0;
        grid_view = 0;
        priceeconomicprod = 0;
        priceexpensiveprod = 0;
        priceeconomicprod_selected = 0;
        priceexpensiveprod_selected = 0;
        nselectbrands = 0;
        open_bootbox_address = 0;
        setCookieID=0;
    </script>
</head>

<body>
    <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->  

    <!-- Body main wrapper start -->
    <div class="wrapper">

        @php
        App\User::set_guest_user_and_cookie();
        @endphp
        <!-- START HEADER AREA -->
        <header class="header-area header-wrapper">
            <!-- header-top-bar -->
            {{-- <div class="header-top-bar">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-6 hidden-xs">
                            <div class="call-us">
                                <p class="mb-0 roboto">(+88) 01234-567890</p>
                            </div>
                        </div>
                        <div class="col-sm-6 col-xs-12">
                            <div class="top-link clearfix">
                                <ul class="link f-right">
                                    @if(App\User::check_auth())
                                        <li>
                                            <a href="account">
                                                <i class="zmdi zmdi-account"></i>
                                                {{ __('Mi cuenta') }}
                                            </a>
                                        </li>
                                    @endif
                                    <li class="wishlist_header">
                                        @include('parts/wishlist_header')
                                    </li>
                                    <li>
                                        @if(App\User::check_auth())
                                            <a class="dropdown-item" href="{{ route('logout') }}"
                                                onclick="event.preventDefault();
                                                            document.getElementById('logout-form').submit();">
                                                <i class="zmdi zmdi-arrow-right"></i>{{ __('Logout') }}
                                            </a>
        
                                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                @csrf
                                            </form>
                                        @else
                                            <a href="/login">
                                                <i class="zmdi zmdi-lock"></i>
                                                Login
                                            </a>
                                        @endif
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div> --}}
            <!-- header-middle-area -->
            <div id="sticky-header" class="header-middle-area plr-40 sticky">
                <div class="container-fluid">
                    <div class="full-width-mega-dropdown">
                        <div class="row">
                            <!-- logo -->
                            <div class="col-md-2 col-sm-6 col-xs-12">
                                <div class="logo">
                                    <a href="/">
                                        <img src="{{ URL::asset('img/logo/logo.png') }}" alt="main logo">
                                    </a>
                                </div>
                            </div>
                            <!-- primary-menu -->
                            <div class="col-md-7 hidden-sm hidden-xs">
                                @if (!Request::is('cart') && !Request::is('checkout'))
                                    <nav id="primary-menu">
                                        <ul class="main-menu text-center">
                                            @foreach(App\Category::All() as $category)
                                                <li>
                                                    <a href="/search?category={{$category->name}}">{{$category->name}}</a>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </nav>
                                @endif
                            </div>
                            <!-- header-search & total-cart -->
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                @if (!Request::is('cart') && !Request::is('checkout'))
                                    <div class="row search-top-cart f-right">
                                        <!-- header-search -->
                                        <div class="col-sm-2 col-xs-12 header-search f-left">
                                            <div class="header-search-inner">
                                                <button class="search-toggle">
                                                    <i class="zmdi zmdi-search"></i>
                                                </button>
                                                <form action="/search/" method="POST">
                                                    <div class="top-search-box">
                                                        <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                                                        <input id="search_hdr" type="text" name="search" maxlength="100" placeholder="Busca aquí tu producto..." />
                                                        <button type="submit">
                                                            <i class="zmdi zmdi-search"></i>
                                                        </button>
                                                    </div>
                                                </form> 
                                            </div>
                                        </div>
                                        <div id="topcart" class="col-xs-4 total-cart f-left">
                                        </div>
                                        <div class="col-xs-2 wishlist icon_header_1 f-left">
                                            <div class="wishlist_header icon_header_2">
                                                @include('parts/wishlist_header')
                                            </div>
                                        </div>
                                        @if(App\User::check_auth())
                                            <div class="col-xs-2 account icon_header_1 f-left">
                                                <div class="account_header icon_header_2">
                                                    <a href="/account" title="Mi cuenta" class="icon_header_3">
                                                        <i class="zmdi zmdi-account"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        @endif
                                        <div class="col-xs-2 loginout icon_header_1 f-left">
                                            <div class="icon_header_2">
                                                @if(App\User::check_auth())
                                                    <a class="dropdown-item icon_header_3" title="Logout" href="{{ route('logout') }}"
                                                        onclick="event.preventDefault();
                                                                    document.getElementById('logout-form').submit();">
                                                        <i class="zmdi zmdi-arrow-right"></i>
                                                    </a>
                
                                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                        @csrf
                                                    </form>
                                                @else
                                                    <a href="/login" title="Login" class="icon_header_3">
                                                        <i class="zmdi zmdi-lock"></i>
                                                    </a>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <!-- END HEADER AREA -->
        <!-- START MOBILE MENU AREA -->
        <div class="mobile-menu-area hidden-lg hidden-md">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="mobile-menu">
                            <nav id="dropdown">
                                <ul>
                                    <li>
                                        <a href="/categories/capo">Capó</a>
                                    </li>
                                    <li>
                                        <a href="/categories/maletero">Maletero</a>
                                    </li>
                                    <li>
                                        <a href="/categories/maletero">Ruedas</a>
                                    </li>
                                    <li>
                                        <a href="/categories/maletero">Volante</a>
                                    </li>
                                    <li>
                                        <a href="/categories/maletero">Llaves</a>
                                    </li>
                                    <li>
                                        <a href="/categories/maletero">Motos</a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END MOBILE MENU AREA -->