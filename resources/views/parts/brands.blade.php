<!-- BY BRAND SECTION START-->
<div class="by-brand-section mb-80">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-title text-left mb-40">
                    <h2 class="uppercase">Marcas</h2>
                    {{-- <h6>There are many variations of passages of brands available,</h6> --}}
                </div>
            </div>
        </div>
        <div class="by-brand-product">
            <div class="row active-by-brand slick-arrow-2">
                @foreach($brands as $brand)
                    <div class="col-xs-12">
                        <div class="single-brand-product">
                            <a data-id="{{$brand->id}}" title="{{$brand->name}}" class="clickable search_brand"><img src="{{ ($brand->logo != NULL && $brand->logo != '') ? $brand->logo : 'img/product/5.jpg'}}" alt="{{$brand->name}}"></a>
                            {{-- <h3 class="brand-title text-gray">
                                <a href="#">Brand name</a>
                            </h3> --}}
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
<!-- BY BRAND SECTION END -->