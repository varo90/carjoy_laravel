<div class="panel panel-cj mt-20">
    <div class="panel-heading">
        <div class="row">
            <div id="{{$address->id}}" class="col-xs-10 textadd clicable_addr">
                {{$address->address1}}, {{$address->postcode}}, {{$address->city}}
            </div>
            <div class="col-xs-1">
                @if($address->is_main_address == 1)
                    <a class="main_address f-right" title="Dirección principal"><i class="zmdi zmdi-star"></i></a>
                @else
                    <meta name="type-{{$address->id}}" content="{{ $address->type }}">
                    <a data-id="{{$address->id}}" title="Marcar como principal" class="main_address make_main_address f-right">
                        <i class="zmdi zmdi-star-outline"></i>
                    </a>
                @endif
            </div>
            <div class="col-xs-1">
                <meta name="csrf-token-{{$address->id}}" content="{{ csrf_token() }}">
                <a data-id="{{$address->id}}" title="Eliminar" class="delete_addr f-right">
                    <i class="zmdi zmdi-delete"></i>
                </a>
            </div>
        </div>
    </div>
    {{-- <div class="panel-body">Panel Content</div> --}}
</div>