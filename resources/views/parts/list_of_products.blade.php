<div class="row">
    @foreach($products as $product)
        <div class="col-md-3 col-sm-4 col-xs-12">
            @include('parts/item')
        </div>
    @endforeach
</div>  