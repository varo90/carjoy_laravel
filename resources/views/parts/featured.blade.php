<!-- FEATURED PRODUCT SECTION START -->
<div class="featured-product-section mb-50">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-title text-left mb-40">
                    <h2 class="uppercase">Destacados</h2>
                    {{-- <h6>There are many variations of passages of brands available,</h6> --}}
                </div>
            </div>
        </div>
        <div class="featured-product">
            <div class="row active-featured-product slick-arrow-2">
                @foreach($featured_products as $product)
                    <div class="col-xs-12">
                        @include('parts.item')
                    </div>
                @endforeach
            </div>
        </div>          
    </div>            
</div>
<!-- FEATURED PRODUCT SECTION END -->