<!-- PRODUCT TAB SECTION START -->
<div class="product-tab-section mb-50">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-12 col-xs-12">
                <div class="section-title text-left mb-40">
                    <h2 class="uppercase">Listas de productos</h2>
                    {{-- <h6>There are many variations of passages of brands available,</h6> --}}
                </div>
            </div>
            <div class="col-md-6 col-sm-12 col-xs-12">
                <div class="pro-tab-menu text-right">
                    <!-- Nav tabs -->
                    <ul class="" >
                        {{-- <li><a href="#popular-product" data-toggle="tab">Populares</a></li> --}}
                        <li class="active"><a href="#best-seller" data-toggle="tab">Más vendidos</a></li>
                        <li><a href="#new-arrival" data-toggle="tab">Novedades</a></li>
                        <li><a href="#special-offer"  data-toggle="tab">Mejores ofertas</a></li>
                    </ul>
                </div>                       
            </div>
        </div>
        <div class="product-tab">
            <!-- Tab panes -->
            <div class="tab-content">
                <!-- popular-product start -->
                {{-- <div class="tab-pane active" id="popular-product">
                    <div class="row">
                        @foreach($populars as $product)
                            <div class="col-md-3 col-sm-4 col-xs-12">
                                @include('parts/item')
                            </div>
                        @endforeach
                    </div>
                </div> --}}
                <!-- popular-product end -->
                <!-- best-seller start -->
                <div class="tab-pane active" id="best-seller">
                    <div class="row">
                        @foreach($best_sellers as $product)
                            <div class="col-md-3 col-sm-4 col-xs-12">
                                @include('parts/item')
                            </div>
                        @endforeach
                    </div>                                        
                </div>
                <!-- best-seller end -->
                <!-- new-arrival start -->
                <div class="tab-pane" id="new-arrival">
                    <div class="row">
                        @foreach($new_arrivals as $product)
                            <div class="col-md-3 col-sm-4 col-xs-12">
                                @include('parts/item')
                            </div>
                        @endforeach
                    </div>                                        
                </div>
                <!-- new-arrival end -->
                <!-- special-offer start -->
                <div class="tab-pane" id="special-offer">
                    <div class="row">
                        @foreach($biggest_offers as $product)
                            <div class="col-md-3 col-sm-4 col-xs-12">
                                @include('parts/item')
                            </div>
                        @endforeach
                    </div>                                        
                </div>
                <!-- special-offer end -->
            </div>
        </div>
    </div>
</div>
<!-- PRODUCT TAB SECTION END -->