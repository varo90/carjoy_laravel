<div class="product-item">
    <div class="product-img">
        <a href="products/{{$product->slug}}">
            <img src="{{ ($product->path != '' && $product->path != NULL) ? URL::asset($product->path) : URL::asset('img/product/1.jpg') }}" alt=""/>
        </a>
    </div>
    <div class="product-info">
        <h6 class="product-title">
            <a href="products/{{$product->slug}}" title="{{$product->name}}">{{$product->name}}</a>
        </h6>
        {{-- <div class="pro-rating">
            @php $product->reviews_avg; @endphp
            @include('common/rating')
        </div> --}}
        <h3 class="pro-price">{{ number_format((float)($product->sell_price), 2, ',', '.') }} €</h3>
        <ul class="action-button">
            <li>
                <a id="wishlist__{{$product->id}}" class="btn_pr wishlist_btn" title="Añadir a Wishlist"><i class="zmdi zmdi-favorite"></i></a>
            </li>
            <li>
                <a id="quickview__{{$product->id}}" class="btn_pr quickview_btn" title="Ojear"><i class="zmdi zmdi-zoom-in"></i></a>
            </li>
            <li>
                <input type="hidden" name="_token" id="token__{{$product->id}}" value="{{ csrf_token() }}">
                <input name="qty" id="qty__{{$product->id}}" type="hidden" value="{{ $product->recommended_quantity }}">
                <a id="cart__{{$product->id}}" class="btn_pr cart_btn" title="Añadir al carrito"><i class="zmdi zmdi-shopping-cart-plus"></i></a>
            </li>
            <li>
                <a href="/quickbuy?p={{ $product->id }}&quantity={{ $product->recommended_quantity }}" title="Compra rápida"><i class="zmdi zmdi-forward"></i></a>
            </li>
        </ul>
    </div>
</div>