{{-- <h6 class="widget-title border-left mb-20">Detalles del pedido</h6> --}}
<h4>Resumen del pedido:</h4>
<table>
    @foreach($shopping_cart as $product)
        <tr>
            <td class="td-title-1">{{ str_limit($product->name, $limit = 61, $end = '...') }} {{ $product->cartquantity > 1 ? ' ( x' . round($product->cartquantity) . ' )' : '' }}</td>
            <td class="td-title-2">{{ number_format((float)($product_total_price = $product->sell_price*$product->cartquantity), 2, ',', '.') }} €</td>
        </tr>
    @endforeach
    <tr>
        <td class="td-title-1">Total del carrito (Con IVA)</td>
        <td class="td-title-2">{{ number_format((float)($shopping_cart_total_price), 2, ',', '.') }} €</td>
    </tr>
    <tr>
        <td class="td-title-1">Envío ({{$shipping_methods->find($selected_shipping_method)->name}})</td>
        <td class="td-title-2">{{ number_format((float)($shipment = $shipping_methods->find($selected_shipping_method)->price), 2, ',', '.') }} €</td>
    </tr>
    <tr>
        <td class="order-total">Total</td>
        <td class="order-total-price">{{ $grand_total = number_format((float)($shopping_cart_total_price + $shipment), 2, ',', '.') }} €</td>
    </tr>
</table>