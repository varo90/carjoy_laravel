<div class="payment-section mt-40">
    <h4>3. Método de pago:</h4>
    <div id="cardpay" class="panel panel-default">
        <div class="panel-heading">
            <div class="row">
                <div class="col-xs-1">
                    <i class="zmdi zmdi-card"></i>
                </div>
                <div class="col-xs-11 clickable">
                    Pago con tarjeta de crédito/débito
                    {{-- <script
                        src="https://checkout.stripe.com/checkout.js" class="stripe-button"
                        data-key="pk_test_R2hHlKZg8LuMcQz4GfZYzGuA"
                        data-amount="{{($shopping_cart_total_price+$shipping_methods->find($selected_shipping_method)->price)*100}}"
                        data-email="{{$email}}"
                        data-name="emblemasparacoche.com"
                        data-description="Pago con tarjeta"
                        data-image="https://stripe.com/img/documentation/checkout/marketplace.png"
                        data-locale="auto"
                        data-currency="eur">
                    </script> --}}
                    <script src="{{ URL::asset('js/stripe-checkout.js') }}"></script>

                    <script>
                        var handler = StripeCheckout.configure({
                            key: 'pk_test_R2hHlKZg8LuMcQz4GfZYzGuA',
                            image: 'https://stripe.com/img/documentation/checkout/marketplace.png',
                            locale: 'auto',
                            token: function(token) {
                                // You can access the token ID with `token.id`.
                                // Get the token ID to your server-side code for use.
                                $form = $('#checkout_form');
                                $form.append($('<input type="hidden" name="stripeToken" />').val(token.id));
                                $form.append($('<input type="hidden" name="stripeTokenType" />').val(token.type));
                                $form.append($('<input type="hidden" name="stripeEmail" />').val(token.email));

                                // $form.get(0).submit();
                            }
                            });

                            document.getElementById('cardpay').addEventListener('click', function(e) {
                                // Open Checkout with further options:
                                handler.open({
                                    name: 'emblemasparacoche.com',
                                    description: 'Pago con tarjeta',
                                    currency: 'eur',
                                    email: "{{$email}}",
                                    amount: {{($shopping_cart_total_price+$shipping_methods->find($selected_shipping_method)->price)*100}}
                                });
                                e.preventDefault();
                            });

                            // Close Checkout on page navigation:
                            window.addEventListener('popstate', function() {
                            handler.close();
                        });
                    </script>
                </div>
            </div>
        </div>
    </div>
    {{-- <div id="transferpay" class="panel panel-default">
        <div class="panel-heading">
            <div class="row">
                <div class="col-xs-1">
                    <i class="zmdi zmdi-refresh"></i>
                </div>
                <div class="col-xs-11 clickable">
                    Transferencia bancaria
                </div>
            </div>
        </div>
    </div>
    <div id="paypalpay" class="panel panel-default">
        <div class="panel-heading">
            <div class="row">
                <div class="col-xs-1">
                    <i class="zmdi zmdi-paypal"></i>
                </div>
                <div class="col-xs-11 clickable">
                    Paypal
                </div>
            </div>
        </div>
    </div> --}}
</div>