{{-- <a href="/wishlist">
    <i class="zmdi zmdi-favorite"></i>
    Lista de deseos ({{ App\Wishlist::where('user_id',Auth::user()->id)->where('active',1)->count() }})
</a> --}}
<a href="/wishlist" title="Lista de deseos" class="icon_header_3">
    <span id="numitemsinwislist" class="wishlist-quantity">{{ str_pad(App\Wishlist::where('user_id',Auth::user()->id)->where('active',1)->count(),2,'0',STR_PAD_LEFT) }}</span><br>
    <span class="wishlist-icon"><i class="zmdi zmdi-favorite"></i></span>
</a>