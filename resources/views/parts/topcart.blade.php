<?php $num_items = $shopping_cart->count(); ?>
<div class="total-cart-in">
    <div class="cart-toggler">
        <a>
            <span id="numitemsincart" class="cart-quantity">{{ str_pad($num_items,2,'0',STR_PAD_LEFT) }}</span><br>
            <span class="cart-icon"><i class="zmdi zmdi-shopping-cart-plus"></i></span><br>
            <span class="cart-price">{{ $num_items > 0 ? number_format((float)$shopping_cart_total_price->totalPrice, 2, ',', '.') : 0 }} €</span><br>
        </a>                            
    </div>
    <ul>
        <li>
            <div class="top-cart-inner your-cart">
                <h5 class="text-capitalize">Tu carrito</h5>
            </div>
        </li>
        <li>
            @if($num_items > 0)
                <div class="total-cart-pro">
                    <?php $total_price = 0; ?>
                    @foreach($shopping_cart as $product)
                        <div class="single-cart clearfix">
                            <div class="cart-img f-left">
                                <a href="/products/{{$product->slug}}">
                                    <img src="{{ URL::asset($product->path) != NULL ? URL::asset($product->path) : URL::asset('img/cart/1.jpg') }}" with="100" height="111" alt="{{$product->name}}" />
                                </a>
                                <div class="del-icon">
                                    <meta name="csrf-token-{{$product->id_shc}}" content="{{ csrf_token() }}">
                                    <a class="delcart" data-id="{{ $product->id_shc }}">
                                        <i class="zmdi zmdi-close"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="cart-info f-left">
                                <h6 class="text-capitalize">
                                    <a href="/products/{{$product->slug }}" title="{{$product->name}}">{{ str_limit($product->name, $limit = 26, $end = '...') }}</a>
                                </h6>
                                <p>
                                    Precio: 
                                    {{-- @if($product->cartquantity > 0) --}}
                                        <a class="lessquantity cquantity {{ ($product->cartquantity <= 1) ? 'red-font' : ''}}" data-id="{{ $product->id_shc }}">-</a> 
                                    {{-- @endif --}}
                                    {{ round($product->cartquantity,2)  }} 
                                    {{-- @if($product->cartquantity < $product->quantity) --}}
                                        <a class="sumquantity cquantity {{ ($product->cartquantity >= $product->quantity) ? 'red-font' : ''}}" data-id="{{ $product->id_shc }}">+</a> 
                                    {{-- @endif --}}
                                    x {{ round($product->sell_price,2)  }} =
                                    <?php $product_total_price = $product->sell_price*$product->cartquantity; ?>
                                    <b>{{ round($product_total_price,2)  }} €</b><br>
                                </p>
                                {{-- <p>
                                    <span>Brand <strong>:</strong></span>Brand Name
                                </p>
                                <p>
                                    <span>Model <strong>:</strong></span>Grand s2
                                </p>
                                <p>
                                    <span>Color <strong>:</strong></span>Black, White
                                </p> --}}
                            </div>
                        </div>
                        <?php $total_price += $product_total_price; ?>
                    @endforeach
                </div>
            @endif
        </li>
        <li>
            <div class="top-cart-inner subtotal">
                <h4 class="text-uppercase g-font-2">
                    Total  =  
                    <span>{{ $num_items > 0 ? number_format((float)$total_price, 2, ',', '.') : 0 }}€</span>
                </h4>
            </div>
        </li>
        @if($num_items > 0)
            <li>
                <div class="top-cart-inner view-cart">
                    <h4 class="text-uppercase">
                        <a href="/cart">Ver carrito</a>
                    </h4>
                </div>
            </li>
            <li>
                <div class="top-cart-inner check-out">
                    <h4 class="text-uppercase">
                        <a href="/checkout">Hacer pedido</a>
                    </h4>
                </div>
            </li>
        @endif
    </ul>
</div>