{{-- <script
    src="https://checkout.stripe.com/checkout.js" class="stripe-button"
    data-key="{!! env('STRIPE_PK') !!}"
    data-amount={{$order->total_price*100}}
    data-name="Stripe Demo"
    data-email="{{$order->email}}"
    data-description="Online course about integrating Stripe"
    data-image="https://stripe.com/img/documentation/checkout/marketplace.png"
    data-locale="auto"
    data-currency="eur">
</script> --}}
{{-- <script src="https://checkout.stripe.com/checkout.js"></script>

<button id="customButton">Pagar con tarjeta</button>

<script>
    var handler = StripeCheckout.configure({
        key: 'pk_test_R2hHlKZg8LuMcQz4GfZYzGuA',
        image: 'https://stripe.com/img/documentation/checkout/marketplace.png',
        locale: 'auto',
        token: function(token) {
            // You can access the token ID with `token.id`.
            // Get the token ID to your server-side code for use.
        }
    });

    document.getElementById('customButton').addEventListener('click', function(e) {
        // Open Checkout with further options:
        handler.open({
            image: '{{ URL::asset('img/logo/logo.png') }}',
            name: 'emblemasparacoche.es',
            description: 'Pago express',
            email: '{{$order->email}}',
            currency: 'eur',
            amount: {{$order->total_price*100}}
        });
        // handler.open({
        //     name: 'emblemasparacoche.es',
        //     description: 'Pago express',
        //     email: '{{$order->email}}',
        //     currency: 'eur',
        //     amount: {{$order->total_price*100}}
        // });
        e.preventDefault();
    });

        // Close Checkout on page navigation:
    window.addEventListener('popstate', function() {
        handler.close();
    });
</script> --}}