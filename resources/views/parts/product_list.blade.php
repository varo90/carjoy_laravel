<!-- PRODUCT TAB SECTION START -->
<div class="product-tab-section mb-50">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-12 col-xs-12">
                <div class="section-title text-left mb-40">
                    <h2 class="uppercase">Listas de productos</h2>
                    {{-- <h6>There are many variations of passages of brands available,</h6> --}}
                </div>
            </div>
            <div class="col-md-6 col-sm-12 col-xs-12">
                <div class="pro-tab-menu text-right">
                    <!-- Nav tabs -->
                    <ul>
                        <meta name="csrf-token" content="{{ csrf_token() }}">
                        <li class="active"><a class="tab-list" data-id="best-seller" data-toggle="tab">Más vendidos</a></li>
                        <li><a class="tab-list" data-id="new-arrival" data-toggle="tab">Novedades</a></li>
                        <li><a class="tab-list" data-id="special-offer" data-toggle="tab">Mejores ofertas</a></li>
                    </ul>
                </div>                       
            </div>
        </div>
        <div id="list_of_products"></div>
    </div>
</div>
<!-- PRODUCT TAB SECTION END -->
<script>
    product_tab_section = 1;
</script>