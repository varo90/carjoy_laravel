<div class="shop-list product-item">
    <div class="product-img">
        <a href="single-product.html">
            <img src="{{ ($product->path != '' && $product->path != NULL) ? URL::asset($product->path) : URL::asset('img/product/1.jpg') }}" alt="{{$product->name}}"/>
        </a>
    </div>
    <div class="product-info">
        <div class="clearfix">
            <h6 class="product-title f-left">
                <a href="products/{{$product->slug}}">{{$product->name}}</a>
            </h6>
            {{-- <div class="pro-rating f-right">
                @php $product->reviews_avg; @endphp
                @include('common/rating')
            </div> --}}
        </div>
        {{-- <h6 class="brand-name mb-30">Brand Name</h6> --}}
        <h3 class="pro-price">{{ number_format((float)($product->sell_price), 2, ',', '.') }} €</h3>
        <p>{{ $product->short_description }}</p>
        <ul class="action-button">
            <li>
                <a id="wishlist__{{$product->id}}" class="btn_pr wishlist_btn" title="Wishlist"><i class="zmdi zmdi-favorite"></i></a>
            </li>
            <li>
                <a id="quickview__{{$product->id}}" class="btn_pr quickview_btn"><i class="zmdi zmdi-zoom-in"></i></a>
            </li>
            {{-- <li>
                <a href="#" title="Compare"><i class="zmdi zmdi-refresh"></i></a>
            </li> --}}
            <li>
                <input type="hidden" name="_token" id="token__{{$product->id}}" value="{{ csrf_token() }}">
                <input name="qty" id="qty__{{$product->id}}" type="hidden" value="{{ $product->recommended_quantity }}">
                <a id="cart__{{$product->id}}" class="btn_pr cart_btn"><i class="zmdi zmdi-shopping-cart-plus"></i></a>
            </li>
        </ul>
    </div>
</div>