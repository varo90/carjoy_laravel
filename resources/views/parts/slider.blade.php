<!-- START SLIDER AREA -->
<div class="slider-area  plr-185  mb-80">
    <div class="container-fluid">
        <div class="slider-content">
            <div class="row">
                <div class="active-slider-1 slick-arrow-1 slick-dots-1">
                    <!-- layer-1 Start -->
                    <div class="col-md-12">
                        <div class="layer-1">
                            <div class="slider-img">
                                <img src="img/slider/slider-1/bannerpulsera.jpg" alt="">
                            </div>
                            <div class="slider-info gray-bg">
                                <div class="slider-info-inner">
                                    <h1 class="slider-title-1 text-uppercase text-black-1">Piezas de arte</h1>
                                    <div class="slider-brief text-black-2">
                                        <p>Cada uno de nuestros emblemas tienen su razón de ser. No solamente están hechos de materiales de calidad, altamente resitentes y duraderos; es que queremos que puedas resaltar la imagen de tu coche a tu gusto y con tu toque personal. Ahí es donde marcas la diferencia, donde puedes captar las miradas. Es... Puro arte.</p>
                                    </div>
                                    {{-- <a href="#" class="button extra-small button-black">
                                        <span class="text-uppercase">Buy now</span>
                                    </a> --}}
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- layer-1 end -->
                    <!-- layer-1 Start -->
                    <div class="col-md-12">
                        <div class="layer-1">
                            <div class="slider-img">
                                <img src="img/slider/slider-1/bannerchefondo.jpg" alt="">
                            </div>
                            <div class="slider-info gray-bg">
                                <div class="slider-info-inner">
                                    <h1 class="slider-title-1 text-uppercase text-black-1">Los tenemos todos</h1>
                                    <div class="slider-brief text-black-2">
                                        <p>Tengas el coche que tengas, disponemos de lo que buscas para que lleves tu marca con orgullo.</p>
                                    </div>
                                    {{-- <a href="#" class="button extra-small button-black">
                                        <span class="text-uppercase">Buy now</span>
                                    </a> --}}
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- layer-1 end -->
                    <!-- layer-1 Start -->
                    <div class="col-md-12">
                        <div class="layer-1">
                            <div class="slider-img">
                                <img src="img/slider/slider-1/bannerayuda.jpg" alt="">
                            </div>
                            <div class="slider-info gray-bg">
                                <div class="slider-info-inner">
                                    <h1 class="slider-title-1 text-uppercase text-black-1">Te ayudamos</h1>
                                    <div class="slider-brief text-black-2">
                                        <p>Si tienes dudas tan sólo facilítanos tu modelo de coche y las medidas para que te ayudemos a encontrar lo que buscas.</p>
                                    </div>
                                    <a href="/contact" class="button extra-small button-black">
                                        <span class="text-uppercase">Consúltanos</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- layer-1 end -->
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END SLIDER AREA -->