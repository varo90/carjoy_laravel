@extends('layouts/layout')

@section('title','Búsqueda de productos')

@section('content')
<section id="page-content" class="page-wrapper">
    <!-- SHOP SECTION START -->
    <div class="shop-section mb-80">
        <div class="container">
            <div class="row">
                <div class="col-md-9 col-md-push-3 col-sm-12">
                    <form action="/search/" method="POST">
                        <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                        <input type="hidden" name="view" id="view" value="{{ $view }}">
                        <input type="hidden" name="category" id="view" value="{{ $category }}">
                        <div id="search-box" class="inner-addon right-addon">
                            <i class="zmdi zmdi-search"></i>
                            <input id="search" type="text" name="search" class="form-control" value="{{ $search }}" maxlength="100" placeholder="Busca aquí tu producto..." />
                        </div>
                    </form> 
                    <div class="shop-content">

                        <!-- shop-option start -->
                        <div class="shop-option box-shadow mb-30 clearfix">
                            <!-- Nav tabs -->
                            <ul class="shop-tab f-left" role="tablist">
                                <li class="{{ $view == 'grid' ? 'active' : '' }}">
                                    <a id="grid" class="submit" data-toggle="tab"><i class="zmdi zmdi-view-module"></i></a>
                                </li>
                                <li class="{{ $view == 'list' ? 'active' : '' }}">
                                    <a id="list" class="submit" data-toggle="tab"><i class="zmdi zmdi-view-list-alt"></i></a>
                                </li>
                            </ul>
                            <!-- short-by -->
                            <div class="short-by f-left text-center">
                                <span>Ordenar por:</span>
                                <?php $optionsOrderBy = ['newer'=>'Más nuevos','older'=>'Más antiguos','expensive'=>'Más caros','economic'=>'Más baratos','discount'=>'Mejores descuentos','best_seller'=>'Best Seller'] ?>
                                <select id="ordersearch">
                                    @foreach($optionsOrderBy as $key => $value)
                                        <option value="{{ $key }}" {{ $orderBy==$key ? 'selected' : '' }}>{{ $value }}</option>
                                    @endforeach
                                </select> 
                            </div> 
                            <!-- showing -->
                            <div class="showing f-right text-right">
                                <span>{{ $products->count() }} Productos</span>
                            </div>                                   
                        </div>
                        <!-- shop-option end -->

                        <!-- Tab Content start -->
                        <div class="tab-content">
                            <!-- grid-view -->
                            @if( $view == 'grid' ? 'active' : '' )
                                <div role="tabpanel" class="tab-pane active" id="grid-view">
                                    @include('parts/list_of_products')
                                </div>
                            @elseif( $view == 'list' ? 'active' : '' )
                                <!-- list-view -->
                                <div role="tabpanel" class="tab-pane active" id="list-view">
                                    <div class="row">
                                        @foreach($products as $product)
                                            <div class="col-md-12">
                                                @include('parts/item_list')
                                            </div>
                                        @endforeach
                                    </div>                                        
                                </div>
                            @endif
                        </div>
                        <!-- Tab Content end -->
                        <!-- shop-pagination start -->
                        <div class="shop-pagination box-shadow text-center ptblr-10-30">
                            {{ $products->links() }}
                        </div>
                        {{-- <ul class="shop-pagination box-shadow text-center ptblr-10-30">
                            <li><a href="#"><i class="zmdi zmdi-chevron-left"></i></a></li>
                            <li><a href="#">01</a></li>
                            <li><a href="#">02</a></li>
                            <li><a href="#">03</a></li>
                            <li><a href="#">...</a></li>
                            <li><a href="#">05</a></li>
                            <li class="active"><a href="#"><i class="zmdi zmdi-chevron-right"></i></a></li>
                        </ul> --}}
                        <!-- shop-pagination end -->
                    </div>
                </div>
                <div class="col-md-3 col-md-pull-9 col-sm-12">
                    <!-- price-filter -->
                    <aside class="widget shop-filter box-shadow mb-10">
                        <h6 class="widget-title border-left mb-20">Precio</h6>
                        <div class="price_filter">
                            <div class="price_slider_amount">
                                <input type="submit"  value="Rango :"/> 
                                <input type="text" id="amount" name="price" placeholder="Añade tu precio" /> 
                            </div>
                            <div id="slider-range"></div>
                        </div>
                    </aside>
                    <!-- brand -->
                    @if(!empty($brands))
                        <aside class="widget operating-system box-shadow mb-30">
                            <h6 class="widget-title border-left mb-20">Marca</h6>
                            @foreach($brands as $brand)
                                <label><input type="checkbox" class="chck-brands" name="chck-brand" value="{{ $brand->id }}" {{ in_array($brand->id,$selected_brands_ids) ? 'checked' : '' }}>{{ $brand->name }}</label><br>
                            @endforeach
                        </aside>
                    @endif
                    {{-- <!-- widget-product -->
                    <aside class="widget widget-product box-shadow">
                        <h6 class="widget-title border-left mb-20">recent products</h6>
                        <!-- product-item start -->
                        <div class="product-item">
                            <div class="product-img">
                                <a href="single-product.html">
                                    <img src="{{ URL::asset('img/product/4.jpg') }}" alt=""/>
                                </a>
                            </div>
                            <div class="product-info">
                                <h6 class="product-title">
                                    <a href="single-product.html">Product Name</a>
                                </h6>
                                <h3 class="pro-price">$ 869.00</h3>
                            </div>
                        </div>
                        <!-- product-item end -->
                        <!-- product-item start -->
                        <div class="product-item">
                            <div class="product-img">
                                <a href="single-product.html">
                                    <img src="{{ URL::asset('img/product/8.jpg') }}" alt=""/>
                                </a>
                            </div>
                            <div class="product-info">
                                <h6 class="product-title">
                                    <a href="single-product.html">Product Name</a>
                                </h6>
                                <h3 class="pro-price">$ 869.00</h3>
                            </div>
                        </div>
                        <!-- product-item end -->
                        <!-- product-item start -->
                        <div class="product-item">
                            <div class="product-img">
                                <a href="single-product.html">
                                    <img src="{{ URL::asset('img/product/12.jpg') }}" alt=""/>
                                </a>
                            </div>
                            <div class="product-info">
                                <h6 class="product-title">
                                    <a href="single-product.html">Product Name</a>
                                </h6>
                                <h3 class="pro-price">$ 869.00</h3>
                            </div>
                        </div>
                        <!-- product-item end -->                               
                    </aside> --}}
                </div>
            </div>
        </div>
    </div>
    <!-- SHOP SECTION END -->  
</section>
    <script type="text/javascript">
        priceeconomicprod = {{ $slider_price['cheapest_product'] }};
        priceexpensiveprod = {{ $slider_price['expensive_product'] }};
        priceeconomicprod_selected = {{ $slider_price['range_min_price'] }};
        priceexpensiveprod_selected = {{ $slider_price['range_max_price'] }};
        nselectbrands = {{ count($brands) }};
    </script>

@endsection