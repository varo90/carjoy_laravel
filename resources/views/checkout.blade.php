@extends('layouts/layout')

@section('title','Checkout')

@section('content')

<?php
    use App\Address;
    $address = isset($address) ? $address : false;
    $main_shipping_address = false;
    $main_billing_address = false;
?>

<!-- Start page content -->
<section id="page-content" class="page-wrapper">
    <div class="shop-section mb-80">
        <div class="container">
            <!-- checkout start -->
            <div class="tab-pane" id="checkout">
                <div class="checkout-content box-shadow p-30">
                    <form id="checkout_form" method="POST" action="/orders">
                        @csrf
                        @include('common/errors')
                        <div class="row">
                            <!-- billing details -->
                            <div class="col-md-6">
                                @if(App\User::check_auth())
                                    <?php
                                        $shipping_addresses = App\Address::where('user_id',Auth::user()->id)->where('type','SHIPPING')->get();
                                        $billing_addresses = App\Address::where('user_id',Auth::user()->id)->where('type','BILLING')->get();
                                    ?>
                                    <div class="sha_checkout">
                                        @if($shipping_addresses->count() > 0)
                                            <h4>Tus direcciones de envío:</h4>
                                            @foreach($shipping_addresses as $address)
                                                <?php if($address->is_main_address) { $main_shipping_address = $address; } ?>
                                                <div class="address_radio">
                                                    <label>
                                                        <input type="radio" id="sha-{{ $address->id }}" name="shipping_address" value="{{ $address->id }}" {{ $address->is_main_address ? 'checked' : ''}}>
                                                        {{ $address->address1 }}; {{ $address->postcode }}; {{ $address->city }}
                                                    </label>
                                                </div>
                                            @endforeach
                                        @endif
                                    </div>
                                @else
                                    <p>¿Ya tienes cuenta? <a href="/login">Accede aquí</a></p>
                                @endif
                                <div class="shipping_address mt-20">
                                    <h4>1. Dirección de envío/facturación:</h4>
                                    <h5>Dirección de envío:</h5>
                                    <div class="row">
                                        <div class="col-xs-6">
                                            <input id="user_name" type="text" name="user_name" placeholder="Nombre *" value="{{ !$main_shipping_address ? old('user_name') : $main_shipping_address->user_name }}" minlength="2" maxlength="255" required>
                                        </div>
                                        <div class="col-xs-6">
                                            <input id="user_surname" type="text" name="user_surname" placeholder="Apellidos *" value="{{ !$main_shipping_address ? old('user_surname') : $main_shipping_address->user_surname }}" minlength="2" maxlength="255" required>
                                        </div>
                                    </div>
                                    <input id="address1" type="text" name='address1' value="{{ !$main_shipping_address ? old('address1') : $main_shipping_address->address1 }}" placeholder="Dirección *" minlength="2" required>
                                    <div class="row">
                                        <div class="col-xs-3">
                                            <input id="postcode" type="text" name='postcode' value="{{ !$main_shipping_address ? old('postcode') : $main_shipping_address->postcode }}" placeholder="Código postal *" minlength="5" required>
                                        </div>
                                        <div class="col-xs-5">
                                            <input id="city" type="text" name='city' value="{{ !$main_shipping_address ? old('city') : $main_shipping_address->city }}" placeholder="Localidad *" minlength="2" required>
                                        </div>
                                        <div class="col-xs-4">
                                            <?php $countrycode = $main_shipping_address != false ? $main_shipping_address->country_id : 69 ?>
                                            <select id="country" name="country_id" class="custom-select">
                                                @foreach(App\Country::orderBy('name_spa')->get() as $country)
                                                    <option value="{{ $country->id }}" {{ $country->id == $countrycode ? 'selected' : '' }}>{{ $country->name_spa }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    @if(!App\User::check_auth())
                                        <input id="email" type="email" name='email' placeholder="Email *" value="{{ !$main_shipping_address ? old('email') : $main_shipping_address->email }}" maxlength="255" required>
                                    @endif
                                    <div class="row">
                                        <div class="col-xs-4">
                                            <input id="nif" type="text" name='nif' placeholder="N.I.F. / N.I.E. / C.I.F. *" value="{{ !$main_shipping_address ? old('nif') : $main_shipping_address->nif }}" maxlength="30" required>
                                        </div>
                                        <div class="col-xs-4">
                                            <input id="company" type="text" name='company' placeholder="Empresa" value="{{ !$main_shipping_address ? old('company') : $main_shipping_address->company }}" maxlength="255">
                                        </div>
                                        <div class="col-xs-4">
                                            <input id="phone" type="text" name='phone' placeholder="Teléfono" value="{{ !$main_shipping_address ? old('phone') : $main_shipping_address->phone }}" maxlength="30">
                                        </div>
                                    </div>
                                    <input id="comments" type="text" name='comments' placeholder="Comentario para la empresa de transportes" value="{{ !$main_shipping_address ? old('comments') : $main_shipping_address->comments }}" maxlength="255">
                                </div>
                                @if(App\User::check_auth())
                                    <div class="blla_checkout mt-20">
                                        @if($billing_addresses->count() > 0)
                                            <h4 class="mt-20">Tus direcciones de facturación:</h4>
                                            @foreach($billing_addresses as $address)
                                                <?php if($address->is_main_address) { $main_billing_address = $address; } ?>
                                                <div class="address_radio">
                                                    <label>
                                                        <input type="radio" id="sha-{{ $address->id }}" name="billing_address" value="{{ $address->id }}" {{ $address->is_main_address ? 'checked' : ''}}>
                                                        {{ $address->address1 }}; {{ $address->postcode }}; {{ $address->city }}
                                                    </label>
                                                </div>
                                            @endforeach
                                        @endif
                                    </div>
                                @endif
                                {{-- {{dd($main_billing_address)}} --}}
                                <div class="same_billing_address">
                                    <label>
                                        <input type="checkbox" name="same_billing_address" {{ $main_billing_address != false ? '' : 'checked' }}>
                                        La dirección de facturación es la misma que la de envío
                                    </label>
                                </div>
                                <div class="billing_address mt-20 {{ $main_billing_address != false ? '' : 'hidden' }}">
                                    <h5>Dirección de facturación:</h5>
                                    <div class="row">
                                        <div class="col-xs-6">
                                            <input id="user_name_bll" type="text" name="user_name_bll" class="input_bll" placeholder="Nombre *" value="{{ !$main_billing_address ? old('user_name_bll') : $main_billing_address->user_name }}" maxlength="255" {{ $main_billing_address != false ? 'required' : '' }}>
                                        </div>
                                        <div class="col-xs-6">
                                            <input id="user_surname_bll" type="text" name="user_surname_bll" class="input_bll" placeholder="Apellidos *" value="{{ !$main_billing_address ? old('user_surname_bll') : $main_billing_address->user_surname }}" maxlength="255" {{ $main_billing_address != false ? 'required' : '' }}>
                                        </div>
                                    </div>
                                    <input id="address1_bll" type="text" name='address1_bll' class="input_bll" value="{{ !$main_billing_address ? old('address1_bll') : $main_billing_address->address1 }}" placeholder="Dirección *" {{ $main_billing_address != false ? 'required' : '' }}>
                                    <div class="row">
                                        <div class="col-xs-3">
                                            <input id="postcode_bll" type="text" name='postcode_bll' class="input_bll" value="{{ !$main_billing_address ? old('postcode_bll') : $main_billing_address->postcode }}" placeholder="Código postal *" {{ $main_billing_address != false ? 'required' : '' }}>
                                        </div>
                                        <div class="col-xs-5">
                                            <input id="city_bll" type="text" name='city_bll' class="input_bll" value="{{ !$main_billing_address ? old('city_bll') : $main_billing_address->city }}" placeholder="Localidad *" {{ $main_billing_address != false ? 'required' : '' }}>
                                        </div>
                                        <div class="col-xs-4">
                                            <?php $countrycode = $main_billing_address != false ? $main_billing_address->country_id : 69 ?>
                                            <select id="country_id_bll" name="country_id_bll" class="custom-select">
                                                @foreach(App\Country::orderBy('name_spa')->get() as $country)
                                                    <option value="{{ $country->id }}" {{ $country->id == $countrycode ? 'selected' : '' }}>{{ $country->name_spa }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-4">
                                            <input id="nif_bll" type="text" name='nif_bll' class="input_bll" placeholder="N.I.F. / N.I.E. / C.I.F. *" value="{{ !$main_billing_address ? old('nif_bll') : $main_billing_address->nif }}" maxlength="30" {{ $main_billing_address != false ? 'required' : '' }}>
                                        </div>
                                        <div class="col-xs-4">
                                            <input id="company_bll" type="text" name='company_bll' placeholder="Empresa" value="{{ !$main_billing_address ? old('company_bll') : $main_billing_address->company }}" maxlength="255">
                                        </div>
                                        <div class="col-xs-4">
                                            <input id="phone_bll" type="text" name='phone_bll' placeholder="Teléfono" value="{{ !$main_billing_address ? old('phone_bll') : $main_billing_address->phone }}" maxlength="30">
                                        </div>
                                    </div>
                                    <input id="comments_bll" type="text" name='comments_bll' placeholder="Comentario para la factura" value="{{ !$main_billing_address ? old('comments_bll') : $main_billing_address->comments }}" maxlength="255">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="shipping_methods mt-20">
                                    <h4>2. Elige el método de envío:</h4>
                                    <input id="#selected_shipping" type="hidden" name="selected_shipping" value="{{$selected_shipping_method}}">
                                    <table id="table_shipping_methods" class="mt-20">
                                        @foreach($shipping_methods as $shipping_method)
                                            <tr class="{{$shipping_method->id == $selected_shipping_method ? 'active' : ''}}">
                                                <td class="hidden">{{$shipping_method->id}}</td>
                                                <td class="td-title-1"><img src="{{ URL::asset($shipping_method->image) }}" width="120px"></td>
                                                <td class="td-title-1">{{$shipping_method->name}}</td>
                                                <td class="td-title-1">{{$shipping_method->min_transit_days}} - {{$shipping_method->max_transit_days}} días laborables</td>
                                                <td class="td-title-1">{{number_format((float)($shipping_method->price), 2, ',', '.')}} €</td>
                                            </tr>
                                        @endforeach
                                    </table>
                                </div>
                                <div class="payment-method">
                                    @include('parts/payment')
                                </div>
                                <div class="payment-details mt-40 pl-10 mb-50">
                                    @include('parts/order_details_checkout')
                                </div> 
                                <button class="submit-btn-1 mt-30 btn-hover-1 f-right" type="submit">¡Listo!</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- checkout end -->
        </div>
    </div>
</section>
<!-- End page content -->
@endsection