@extends('layouts/layout')

@section('title','Cuenta de usuario de ' . $user->user_name)

@section('content')

<!-- Start page content -->
<div id="page-content" class="page-wrapper">

    <!-- LOGIN SECTION START -->
    <div class="login-section mb-80">
        <div class="container">
            {{-- @include('common/errors') --}}
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    @if($errors->any())
                        <?php
                            $errores = json_encode($errors->all());
                            $old_fields = json_encode(old());
                        ?>
                        <script>
                            @if(old('addoredit') == 'edit')
                                id_addr = '{{ old('id') }}';
                            @endif
                            open_bootbox_address = '{{ old('addoredit') }}';
                            address_form_errors = {!! $errores !!};
                            old_address_fields = {!! $old_fields !!}
                        </script>
                        <div class="panel panel-danger mt-20">
                            <div class="panel-heading">No se ha podido añadir o editar la dirección porque hay errores en el formulario.</div>
                            {{-- <div class="panel-body">Panel Content</div> --}}
                        </div>
                    @endif
                    @if(session()->has('added_address'))
                        <div class="panel panel-success mt-20">
                            <div class="panel-heading">Una nueva dirección {{ (session()->get('type') == 'SHIPPING') ? 'de envío' : 'de facturación' }} ha sido añadida con éxito</div>
                            {{-- <div class="panel-body">Panel Content</div> --}}
                        </div>
                    @elseif(session()->has('edited_address'))
                        <div class="panel panel-success mt-20">
                            <div class="panel-heading">La dirección {{ (session()->get('type') == 'SHIPPING') ? 'de envío' : 'de facturación' }} ha sido editada con éxito</div>
                            {{-- <div class="panel-body">Panel Content</div> --}}
                        </div>
                    @elseif(session()->has('mainaddress'))
                        <div class="panel panel-success mt-20">
                            <div class="panel-heading">Una nueva dirección {{ (session()->get('type') == 'SHIPPING') ? 'de envío' : 'de facturación' }} ha sido seleccionada como principal</div>
                            {{-- <div class="panel-body">Panel Content</div> --}}
                        </div>
                    @endif
                    <div class="my-account-content" id="accordion2">
                        <!-- My Personal Information -->
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion2" href="#personal_info">
                                        <div class="myrow">
                                            <div class="col-xs-11">
                                                Información de cuenta
                                            </div>
                                            <div class="col-xs-1">
                                                <i class="zmdi zmdi-caret-down f-right"></i>
                                            </div>
                                        </div>
                                    </a>
                                </h4>
                            </div>
                            <div id="personal_info" class="panel-collapse collapse in" role="tabpanel">
                                <div class="panel-body">
                                    <form action="#">
                                        <div>
                                            <div class="p-30">
                                                <div class="row">
                                                    <div class="personal-info"><p class="mb-7">Email: {{ $user->email }}{{-- <a>(Cambiar) --}}</a></p></div>
                                                    <div class="personal-info"><p class="mb-7">Nombre: {{ $user->user_name }} <a id="user_name">(Cambiar)</a></p></div>
                                                    <div class="personal-info"><p class="mb-7">Apellidos: {{ $user->user_surname }} <a id="user_surname">(Cambiar)</a></p></div>
                                                    <div class="personal-info"><p class="mb-7"><a id="password">Cambiar contraseña</a></p></div>
                                                    {{-- <div class="checkbox">
                                                        <label class="mr-10"> 
                                                            <small>
                                                                <input type="checkbox" name="signup">I wish to subscribe to the 69 Fashion newsletter.
                                                            </small>
                                                        </label>
                                                        <br>
                                                        <label> 
                                                            <small>
                                                                <input type="checkbox" name="signup">I have read and agree to the <a href="#">Privacy Policy</a>
                                                            </small>
                                                        </label>
                                                    </div> --}}
                                                </div>
                                                {{-- <div class="row">
                                                    <div class="col-md-6">
                                                        <button class="submit-btn-1 mt-20 btn-hover-1" type="submit" value="register">Save</button>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <button class="submit-btn-1 mt-20 btn-hover-1 f-right" type="reset">Guardar</button>
                                                    </div>
                                                </div> --}}
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- My shipping address -->
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion2" href="#my_shipping">
                                        <div class="myrow">
                                            <div class="col-xs-11">
                                                Mis direcciones de envío
                                            </div>
                                            <div class="col-xs-1">
                                                <i class="zmdi zmdi-caret-down f-right"></i>
                                            </div>
                                        </div>
                                    </a>
                                </h4>
                            </div>
                            <div id="my_shipping" class="panel-collapse collapse {{ ((session()->has('added_address') || session()->has('edited_address') || session()->has('mainaddress')) && session()->get('type') == 'SHIPPING') ? 'in' : '' }}" role="tabpanel" >
                                <div class="panel-body">
                                    <div class="panel-group mt-20">
                                        <?php $shipping_addresses = $user->address->where('type','SHIPPING')->where('active',1)->sortByDesc('is_main_address'); ?>
                                        @foreach($shipping_addresses as $address)
                                            @include('parts/address_bubble')
                                        @endforeach
                                        @if($shipping_addresses->count() == 0)
                                            <p class="mt-20">No has añadido ninguna dirección de envío por ahora.</p>
                                        @endif
                                        <button data-id="SHIPPING" class="submit-btn-1 mt-20 mb-40 btn-hover-1 add_address f-right">Añadir nueva</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- My billing details -->
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion2" href="#billing_address">
                                        <div class="myrow">
                                            <div class="col-xs-11">
                                                Mis direcciones de facturación
                                            </div>
                                            <div class="col-xs-1">
                                                <i class="zmdi zmdi-caret-down f-right"></i>
                                            </div>
                                        </div>
                                    </a>
                                </h4>
                            </div>
                            <div id="billing_address" class="panel-collapse collapse {{ ((session()->has('added_address') || session()->has('edited_address') || session()->has('mainaddress')) && session()->get('type') == 'BILLING') ? 'in' : '' }}" role="tabpanel" >
                                <div class="panel-body">
                                    <div class="panel-group mt-20">
                                        <?php $shipping_addresses = $user->address->where('type','BILLING')->where('active',1)->sortByDesc('is_main_address'); ?>
                                        @foreach($shipping_addresses as $address)
                                            @include('parts/address_bubble')
                                        @endforeach
                                        @if($shipping_addresses->count() == 0)
                                            <p class="mt-20">Si no hay ninguna dirección de facturación se podrá utilizar la misma que la de envío.</p>
                                            <p class="mt-20">No has añadido ninguna dirección de facturación por ahora.</p>
                                        @endif
                                        <button data-id="BILLING" class="submit-btn-1 mt-20 mb-40 btn-hover-1 add_address f-right">Añadir nueva</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- My Order info -->
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion2" href="#My_order_info">
                                        <div class="myrow">
                                            <div class="col-xs-11">
                                                Mis pedidos
                                            </div>
                                            <div class="col-xs-1">
                                                <i class="zmdi zmdi-caret-down f-right"></i>
                                            </div>
                                        </div>
                                    </a>
                                </h4>
                            </div>
                            <div id="My_order_info" class="panel-collapse collapse" role="tabpanel" >
                                <div class="panel-body">
                                    @foreach($user->order->sortByDesc('created_at') as $order)
                                        <?php
                                            $status = \App\Order_history::select('order_statuses.name')->leftjoin('order_statuses','order_status_id','=','order_statuses.id')->where('order_id',$order->id)->orderBy('order_histories.id','desc')->first();
                                            $shipping_address = \App\Order_address::select('order_addresses.*')->find($order->shipment_address_id);
                                            $equal_addresses = $order->shipment_address_id == $order->billing_address_id;
                                            if($equal_addresses) {
                                                $billing_address = $shipping_address;
                                            } else {
                                                $billing_address = \App\Order_address::select('order_addresses.*')->find($order->billing_address_id);
                                            }
                                            $shipper = \App\Shipping_method::find($order->shipping_method_id);
                                            $payment = \App\Payment_method::find($order->payment_method_id);
                                            $products = \App\OrderProduct::select('products.id','products.name','products.slug','order_products.quantity','order_products.unit_price','order_products.tax_amount')->leftjoin('products','order_products.product_id','=','products.id')->where('order_id',$order->id)->get();
                                            $tracking_numbers = App\Order_tracking::where('order_id',$order->id)->get()->implode('tracking_number', ', ');
                                        ?>
                                        <div class="col-xs-12">
                                            <div class="panel panel-primary mt-20">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a data-toggle="collapse" data-parent="#accordion3" href="#order-{{$order->id}}">
                                                            <div class="myrow">
                                                                <div class="col-xs-10">
                                                                    {{ date('d/m/Y',strtotime($order->created_at)) }} | {{ $status['name'] }} | {{ number_format((float)($order->total_price), 2, ',', '.') }}€
                                                                </div>
                                                                <div class="col-xs-2">
                                                                    <i class="zmdi zmdi-caret-down f-right"></i>
                                                                </div>
                                                            </div>
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="order-{{$order->id}}" class="panel-collapse collapse" role="tabpanel" >
                                                    <div class="panel-body">
                                                        <div class="address_tag">
                                                            <div class="row">
                                                                <div class="col-sm-5 col-xs-12">
                                                                    <b><u>Dirección de envío{{ $equal_addresses ? ' y facturación' : '' }}:</u></b><br>
                                                                    {{ $shipping_address->nif }}<br>
                                                                    {{ $shipping_address->user_name }} {{ $shipping_address->user_surname }}<br>
                                                                    {{ $shipping_address->address1 }}<br>
                                                                    {{ $shipping_address->postcode }} {{ $shipping_address->city }}<br>
                                                                    {!! $shipping_address->country_id != 69 ? \App\Country::find($shipping_address->country_id)->name_spa . '<br>' : '' !!}
                                                                </div>
                                                                <div class="col-sm-5 col-xs-12">
                                                                    @if(!$equal_addresses)
                                                                        <b><u>Dirección de facturación:</u></b><br>
                                                                        {{ $billing_address->nif }}<br>
                                                                        {{ $billing_address->user_name }} {{ $billing_address->user_surname }}<br>
                                                                        {{ $billing_address->address1 }}<br>
                                                                        {{ $billing_address->postcode }} {{ $billing_address->city }}<br>
                                                                        {!! $billing_address->country_id != 69 ? \App\Country::find($billing_address->country_id)->name_spa . '<br>' : '' !!}
                                                                    @endif
                                                                </div>
                                                                <div class="col-sm-2 col-xs-12">
                                                                    <a class="btn btn-danger" title="Descargar factura en pdf" href="/pdf_bill/{{ $order->id }}" target="_blank"><i class="zmdi zmdi-collection-pdf"></i> <i class="zmdi zmdi-long-arrow-down"></i></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="shipper">
                                                            <b><u>Forma de envío:</u></b> {{ $shipper->name }}<br>
                                                            @if($tracking_numbers != '')
                                                                <b><u>Seguimiento:</u></b> {{ $tracking_numbers }}<br>
                                                            @endif
                                                        </div>
                                                        <div class="payment">
                                                            <b><u>Forma de pago:</u></b> {{ $payment->name }}<br>
                                                        </div>
                                                        <div class="payment-details p-30">
                                                            <table>
                                                                @foreach($products as $product)
                                                                    <?php $items_price = $product->quantity*$product->unit_price; ?>
                                                                    <tr>
                                                                        <td class="td-title-1">{{ str_limit($product->name, $limit = 61, $end = '...') }}{{ $product->quantity > 1 ? ' (x' . $product->quantity . ')' : '' }}</td>
                                                                        <td class="td-title-2">{{ number_format((float)($items_price), 2, ',', '.') }} €</td>
                                                                    </tr>
                                                                @endforeach
                                                                <tr>
                                                                    <td class="td-title-1">Envío</td>
                                                                    <td class="td-title-2">{{ number_format((float)($order->shipping_price), 2, ',', '.') }} €</td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="td-title-1">Subtotal</td>
                                                                    <td class="td-title-2">{{ number_format((float)( $price_no_tax = $order->total_price / ((100+$order->tax)/100) ), 2, ',', '.') }} €</td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="td-title-1">IVA</td>
                                                                    <td class="td-title-2">{{ number_format((float)($tax = ($order->total_price - $price_no_tax)), 2, ',', '.') }} €</td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="order-total">Total</td>
                                                                    <td class="order-total-price">{{ number_format((float)($order->total_price), 2, ',', '.') }} €</td>
                                                                </tr>
                                                            </table>
                                                        </div> 
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        {{-- <!-- Payment Method -->
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion2" href="#My_payment_method">
                                        <div class="myrow">
                                            <div class="col-xs-6">
                                                Formas de pago
                                            </div>
                                            <div class="col-xs-6">
                                                <i class="zmdi zmdi-caret-down f-right"></i>
                                            </div>
                                        </div>
                                    </a>
                                </h4>
                            </div>
                            <div id="My_payment_method" class="panel-collapse collapse" role="tabpanel" >
                                <div class="panel-body">
                                    <form action="#">
                                        <div class="new-customers p-30">
                                            <select class="custom-select">
                                                <option value="defalt">Card Type</option>
                                                <option value="c-1">Master Card</option>
                                                <option value="c-2">Paypal</option>
                                                <option value="c-3">Paypal</option>
                                                <option value="c-4">Paypal</option>
                                            </select>
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <input type="text"  placeholder="Card Number">
                                                </div>
                                                <div class="col-sm-6">
                                                    <input type="text"  placeholder="Card Security Code">
                                                </div>
                                                <div class="col-sm-6">
                                                    <select class="custom-select">
                                                        <option value="defalt">Month</option>
                                                        <option value="c-1">January</option>
                                                        <option value="c-2">February</option>
                                                        <option value="c-3">March</option>
                                                        <option value="c-4">April</option>
                                                    </select>
                                                </div>
                                                <div class="col-sm-6">
                                                    <select class="custom-select">
                                                        <option value="defalt">Year</option>
                                                        <option value="c-4">2017</option>
                                                        <option value="c-1">2016</option>
                                                        <option value="c-2">2015</option>
                                                        <option value="c-3">2014</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <button class="submit-btn-1 mt-20 btn-hover-1" type="submit" value="register">pay now</button>
                                                </div>
                                                <div class="col-md-4">
                                                    <button class="submit-btn-1 mt-20 btn-hover-1" type="submit" value="register">cancel order</button>
                                                </div>
                                                <div class="col-md-4">
                                                    <button class="submit-btn-1 mt-20 f-right btn-hover-1" type="submit" value="register">continue</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div> --}}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- LOGIN SECTION END -->
</div>
<!-- End page content -->

@endsection