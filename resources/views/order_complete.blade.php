@extends('layouts/layout')

@section('title','Pedido completado')

@section('content')

<section id="page-content" class="page-wrapper">
    <div class="shop-section mb-80">
        <div class="container">
            <h2 class="center">¡Gracias por tu compra!</h2>
            @if(Auth::User()->role_id == 3 && strpos(Auth::User()->email,'@') !== false)
                <div class="set_password center mt-20">
                    <h5>Tan sólo te falta una contraseña para disponer de tu cuenta en emblemasparacoches.com:</h5>
                    @include('common/errors')
                    <form method="POST" action="/set_usr">
                        @csrf
                        <input id="password" type="password" class="form-control" placeholder="Contraseña" name="password" minlength="8" maxlength="50" required>
                        <input id="password-confirm" type="password" class="form-control" placeholder="Confirma tu contraseña" name="password_confirmation" minlength="8" maxlength="50" required>
                        <button class="submit-btn-1 btn-hover-1" type="submit">{{ __('Establecer contraseña') }}</button>
                    </form>
                </div>
            @endif
            <div class="address_tag">
                <div class="row">
                    <?php $equal_addresses = $order->first()->shipment_address_id == $order->first()->billing_address_id; ?>
                    <div class="col-sm-6 col-xs-12">
                        <b><u>Dirección de envío{{ $equal_addresses ? ' y facturación' : '' }}:</u></b><br>
                        {{ $shipping_address->user_name }} {{ $shipping_address->user_surname }}<br>
                        {{ $shipping_address->address1 }}<br>
                        {{ $shipping_address->postcode }} {{ $shipping_address->city }}<br>
                        {!! $shipping_address->country_id != 69 ? \App\Country::find($shipping_address->country_id)->name_spa . '<br>' : '' !!}
                        {{ $shipping_address->nif }}<br>
                    </div>
                    <div class="col-sm-6 col-xs-12">
                        @if(!$equal_addresses)
                            <b><u>Dirección de facturación:</u></b><br>
                            {{ $billing_address->user_name }} {{ $billing_address->user_surname }}<br>
                            {{ $billing_address->address1 }}<br>
                            {{ $billing_address->postcode }} {{ $billing_address->city }}<br>
                            {!! $billing_address->country_id != 69 ? \App\Country::find($billing_address->country_id)->name_spa . '<br>' : '' !!}
                            {{ $billing_address->nif }}<br>
                        @endif
                    </div>
                </div>
            </div>
            <div class="payment-details mt-40 pl-30 mb-50">
                <div class="row">
                    <div class="col-xs-12">
                        <h3>Resumen del pedido:</h3>
                        <table>
                            @foreach($order as $product)
                                <tr>
                                    <td class="td-title-1">{{$product->name}} {{ $product->order_quantity > 1 ? ' ( x' . round($product->order_quantity) . ' )' : '' }}</td>
                                    <td class="td-title-2">{{ number_format((float)($product_total_price = $product->unit_price*$product->order_quantity), 2, ',', '.') }} €</td>
                                </tr>
                            @endforeach
                            <tr>
                                <td class="td-title-1">Total del pedido (Con IVA)</td>
                                <td class="td-title-2">{{ number_format((float)($order->first()->total_price - $order->first()->shipping_price), 2, ',', '.') }} €</td>
                            </tr>
                            <tr>
                                <td class="td-title-1">Envío ({{$shipping_method->name}})</td>
                                <td class="td-title-2">{{ number_format((float)($shipment = $order->first()->shipping_price), 2, ',', '.') }} €</td>
                            </tr>
                            <tr>
                                <td class="order-total">Total</td>
                                <td class="order-total-price">{{ $grand_total = number_format((float)($order->first()->total_price), 2, ',', '.') }} €</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            @if(Auth::User()->role_id != 3)
                <a href="/account" class="button extra-small f-right" title="Ir a Mi cuenta" tabindex="-1">
                    <span class="text-uppercase">Ir a mi cuenta</span>
                </a>
            @endif
        </div>
    </div>
</section>

@endsection