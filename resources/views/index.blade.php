@extends('layouts/layout')

@section('title','emblemasparacoche')

@section('content')

        <section id="page-content" class="page-wrapper">

            @include('parts/slider')
        
            @include('parts/brands')

            @include('parts/featured')

            {{-- @include('parts/upcoming_product') --}}

            @include('parts/product_list')

            {{-- @include('parts/blog') --}}

        </section>
        
@endsection