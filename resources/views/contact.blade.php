@extends('layouts/layout')

@section('title','Contacto')

@section('content')

<?php use Illuminate\Support\Facades\Input; ?>

<section id="page-content" class="page-wrapper">

    <div class="address-section">
        <div class="container">
            <div class="row">
                <div class="contact-address box-shadow">
                    <a href="mailto:contacto@emblemasparacoche.com">
                        <i class="zmdi zmdi-email"></i>
                        <h6>contacto@emblemasparacoche.com</h6>
                    </a>
                    {{-- <h6>info@domainname.com</h6> --}}
                </div>
            </div>
        </div>
    </div>

    <div class="message-box-section mb-80">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="message-box box-shadow white-bg">
                        @include('common/errors')
                        <form id="contact-form" action="/messages" method="POST">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-md-12">
                                    <h4 class="blog-section-title border-left mb-30">Ponte en contacto</h4>
                                </div>
                                <div class="col-md-12">
                                    <input type="text" name="name" placeholder="Nombre *" maxlength="35" value="{{ old('name') }}" required>
                                </div>
                                <div class="col-md-12">
                                    <input type="email" name="email" placeholder="E-mail *" maxlength="100" value="{{ old('email') }}" required>
                                </div>
                                <div class="col-md-12">
                                    <label for="topic">Elige un asunto:</label>
                                    <select id="topic" name="topic_id" class="custom-select">
                                        @foreach($topics as $topic)
                                            <option value="{{ $topic->id }}" {{ (Input::old('topic_id') == $topic->id ? 'selected':'') }}>{{ $topic->name }}</option>
                                        @endforeach
                                    </select>
                                    <div id="hand_topic" class="hidden">
                                        <input type="text" name="subject" placeholder="Asunto *" maxlength="50" value="{{ old('subject') }}">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <textarea class="custom-textarea" name="message" placeholder="Mensaje (Max. 1000 caracteres)" maxlength="1000" value="{{ old('message') }}" ></textarea>
                                </div>
                                <button class="submit-btn-1 mt-30 btn-hover-1 f-right" type="submit">Enviar mensaje</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>

@endsection