<div class="new-customers">
    <?php
        use App\Address;

        $address = isset($address) ? $address : false;

        $type = $address != false ? $address->type : $type;
        $addoredit = $address != false ? 'edit' : 'add';
    ?>
    <form method="POST" action="{{ $address != false ? '/addresses/'.$address->id : '/addresses' }}">
        @csrf
        @if($address != false)
            {{ method_field('PATCH') }}
            <input type="hidden" name="id" value="{{ $address->id }}">
        @endif
        <div class="login-account p-30 box-shadow">
            <input type="hidden" name="type" value="{{ $type }}">
            <input type="hidden" name="addoredit" value="{{ $addoredit }}">
            {{-- <h6 class="widget-title border-left mb-50">{{ __('NUEVA DIRECCIÓN') }}</h6> --}}
            @include('common/errors')
            <div class="row">
                <div class="col-sm-6">
                    <input type="text" name="user_name" placeholder="Nombre *" value="{{ Address::set_field($address,'user_name') }}" maxlength="255" required>
                </div>
                <div class="col-sm-6">
                    <input type="text" name="user_surname" placeholder="Apellidos *" value="{{ Address::set_field($address,'user_surname') }}" maxlength="255" required>
                </div>
            </div>
            <input id="nif" type="text" name='nif' placeholder="N.I.F. / N.I.E. / C.I.F." value="{{ Address::set_field($address,'nif') }}" maxlength="30">
            <input id="address" type="text" name='address1' value="{{ Address::set_field($address,'address1') }}" placeholder="Dirección" required>
            <div class="row">
                <div class="col-sm-3 col-xs-12">
                    <input id="postcode" type="text" name='postcode' value="{{ Address::set_field($address,'postcode') }}" placeholder="Código postal" required>
                </div>
                <div class="col-sm-9 col-xs-12">
                    <input id="city" type="text" name='city' value="{{ Address::set_field($address,'city') }}" placeholder="Localidad" required>
                    {{-- <select class="custom-select">
                        <option value="0">Localidad</option>
                        <option value="c-1">Victoria</option>
                        <option value="c-2">Chittagong</option>
                        <option value="c-3">Boston</option>
                        <option value="c-4">Cambridge</option>
                    </select> --}}
                </div>
            </div>
            <?php $countrycode = $address != false ? $address->country_id : 69 ?>
            <select id="country" name="country_id" class="custom-select">
                @foreach(App\Country::orderBy('name_spa')->get() as $country)
                    <option value="{{ $country->id }}" {{ $country->id == $countrycode ? 'selected' : '' }}>{{ $country->name_spa }}</option>
                @endforeach
            </select>
            <input id="phone" type="text" name='phone' placeholder="Teléfono" value="{{ Address::set_field($address,'phone') }}" maxlength="30">
            <input id="company" type="text" name='company' placeholder="Empresa" value="{{ Address::set_field($address,'company') }}" maxlength="255">
            <input id="comments" type="text" name='comments' placeholder="Comentario para la empresa de transportes" value="{{ Address::set_field($address,'comments') }}" maxlength="255">
            <div class="row">
                <div class="col-md-6">
                </div>
                <div class="col-md-6">
                    <button class="submit-btn-1 mt-20 btn-hover-1 f-right" type="submit" value="register">{{ __('Guardar dirección') }}</button>
                </div>
            </div>
        </div>
    </form>
</div>