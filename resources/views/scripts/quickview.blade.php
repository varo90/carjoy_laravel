<?php 
$sell_price = $product->Product_sell_price->last()->sell_price;
$undiscounted_sell_price = $product->Product_sell_price->last()->undiscounted_sell_price;
?>
<!-- START QUICKVIEW PRODUCT -->
<div class="modal-body">
    <div class="modal-product clearfix">
        <div class="product-images">
            <div class="main-image images">
                <img alt="" src="{{ !$product->Product_image->isEmpty() ? $product->product_image->first()->path : 'img/product/quickview.jpg' }}">
            </div>
        </div><!-- .product-images -->
        
        <div class="product-info">
            <h1>{{$product->name}}</h1>
            <div class="price-box-3">
                <div class="s-price-box">
                    <span class="new-price">{{ number_format(($sell_price), 2, ',', '.') }} €</span>
                    <span class="old-price">{{ ($undiscounted_sell_price != null || $undiscounted_sell_price > $sell_price) ? number_format(($undiscounted_sell_price), 2, ',', '.').'€' : '' }}</span>
                </div>
            </div>
            <div>
                <a href="/products/{{$product->slug}}"><i class="zmdi zmdi-eye"></i> </a><a href="/products/{{$product->slug}}" class="see-all">Ver en detalle</a>
            </div>
            <div class="quick-add-to-cart">
                <div class="cart">
                    <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                    <input type="hidden" name="prdd" id="prdd" value="{{ $product->id }}">
                    <div class="numbers-row">
                        <input name="qty" id="qty" type="number" value="{{ $product->recommended_quantity }}" min="0" max="{{ $product->quantity }}" step="1">
                    </div>
                    <a class="button single_add_to_cart_button" tabindex="-1">
                        <span class="text-uppercase">AÑADIR AL CARRITO</span>
                    </a>
                    <a href="/quickbuy?p={{ $product->id }}&quantity={{ $product->recommended_quantity }}" class="button quick_buy" tabindex="-1">
                        <span class="text-uppercase">COMPRA RÁPIDA</span>
                    </a>
                    <div id="added_to_wishlist">
                        @if($en_wishlist == 0)
                            <a id="wishlist__{{$product->id}}" class="btn_pr wishlist_btn wishlist_btn_single"><i class="zmdi zmdi-favorite"></i> ¡A la lista de deseos!</a>
                        @else
                            <p><i class="zmdi zmdi-favorite"></i> En tu lista de deseos</p>
                        @endif
                    </div>
                </div>
                <div id="added_to_cart"></div>
            </div>
            <div class="quick-desc">
                {{ $product->short_description }}
            </div>
            <div class="social-sharing">
                <div class="widget widget_socialsharing_widget">
                    <h3 class="widget-title-modal">Comparte este producto</h3>
                    <ul class="social-icons clearfix">
                        <li>
                            <a class="facebook" href="#" target="_blank" title="Facebook">
                                <i class="zmdi zmdi-facebook"></i>
                            </a>
                        </li>
                        <li>
                            <a class="google-plus" href="#" target="_blank" title="Google +">
                                <i class="zmdi zmdi-google-plus"></i>
                            </a>
                        </li>
                        <li>
                            <a class="twitter" href="#" target="_blank" title="Twitter">
                                <i class="zmdi zmdi-twitter"></i>
                            </a>
                        </li>
                        <li>
                            <a class="pinterest" href="#" target="_blank" title="Pinterest">
                                <i class="zmdi zmdi-pinterest"></i>
                            </a>
                        </li>
                        <li>
                            <a class="rss" href="#" target="_blank" title="RSS">
                                <i class="zmdi zmdi-rss"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div><!-- .product-info -->
    </div><!-- .modal-product -->
</div><!-- .modal-body -->