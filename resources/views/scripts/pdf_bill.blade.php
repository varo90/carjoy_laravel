<!doctype html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>Factura Emblemasparacoche</title>

<style type="text/css">
    /* @font-face {
        font-family: HeavyEquipment;
        src: url('fonts/HeavyEquipment/HeavyEquipment.ttf');
    } */
    * {
        font-family: 'HeavyEquipment';
    }
    .container {
        width: 90%;
        margin: 0 auto;
    }
    table{
        font-size: 14px;
    }
    tfoot tr td{
        font-weight: bold;
        font-size: 14px;
    }
    .gray {
        background-color: lightgray
    }
    #body {
        margin-top: 120px;
    }
    #products-info {
        margin-top: 40px;
    }
    #summary {
        margin-top: 20px;
    }
    #footer {
        position: absolute;
        right: 0;
        bottom: -35px;
        left: 0;
        /* padding: 1rem; */
        text-align: center;
    }
    #footer table tr td{
        font-size: 10px;
    }
</style>

</head>
<body>
    <div class="container">
        <div id="header">
            <table width="100%">
                <tr>
                    <td align="center">
                        <img src="images/logo/logo.png" alt="" width="150px"/>
                    </td>
                </tr>

            </table>
        </div>

        <div id="body">
            <div id="address">
                <table width="100%">
                    <tr>
                        <td align="left">
                            <strong>Factura:</strong> #{{ $order->order_id }}<br><br>
                            @if($order->company != '')
                                <b>{{ $order->company }}</b><br>
                            @else
                                <b>{{ $order->user_name }} {{ $order->user_surname }}</b><br>
                            @endif
                            {{ $order->address1 }}<br>
                            {{ $order->postcode }} {{ $order->city }}<br>
                            {{ $order->country_id != 69 ? \App\Country::find($order->country_id)->name_spa . '<br>' : '' }}
                            {!! ($order->nif != NULL && $order->nif != '') ? 'NIF / NIE / CIF: ' . $order->nif . '<br>' : '' !!}
                        </td>
                        <td align="right">
                            {{ date('d-m-Y',strtotime($order->created_at)) }}
                        </td>
                    </tr>
                </table>
            </div>

            <br/>
            <div id="products-info">
                <table width="100%">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Descripción</th>
                            <th align="right">Cantidad</th>
                            <th align="right">Precio</th>
                            <th align="right">Importe</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($products as $count => $product)
                            <tr>
                                <th scope="row">{{$count+1}}</th>
                                <td>{{ $product->name }}</td>
                                <td align="right">{{ $product->quantity }}</td>
                                <td align="right">{{ number_format((float)($product->unit_price), 2, ',', '.') }} €</td>
                                <td align="right">{{ number_format((float)($product->unit_price*$product->quantity), 2, ',', '.') }} €</td>
                            </tr>
                        @endforeach
                        <tr>
                            <th scope="row"> </th>
                            <td>Envío</td>
                            <td align="right">1</td>
                            <td align="right">{{ number_format((float)($order->shipping_price), 2, ',', '.') }} €</td>
                            <td align="right">{{ number_format((float)($order->shipping_price), 2, ',', '.') }} €</td>
                        </tr>

                    </tbody>
                </table>
                <br>
                <table align="right" width="34%" style="margin-right:-7px;">
                    <tr>
                        <td align="right">Subtotal:</td>
                        <td align="right">{{ number_format((float)( $price_no_tax = $order->total_price / ((100+$order->tax)/100) ), 2, ',', '.') }} €</td>
                    </tr>
                    <tr>
                        <td align="right">IVA ({{ round($order->tax) }}%):</td>
                        <td align="right">{{ number_format((float)($tax = ($order->total_price - $price_no_tax)), 2, ',', '.') }} €</td>
                    </tr>
                    <tr>
                        <td align="right"><b>Total:</b></td>
                        <td align="right"><b>{{ number_format((float)($order->total_price), 2, ',', '.') }} €</b></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <footer id="footer">
        <table width="100%">
            <tr>
                <td align="center">
                    PASSEIG DE GRÀCIA 93 08008 BARCELONA T+34 932 150 674<br>
                    WWW.emblemasparacoche.ES CONTACTO@EMBLEMASPARACOCHE.ES<br>
                    <p style="font-size: 7px">SANTA EULALIA SA REGISTRO MERCANTIL DE BARCELONA H 18630 F195 T658 L 204 S 2 NIF A-08.031734</p>
                </td>
            </tr>
        </table>
    </footer>
</body>
</html>