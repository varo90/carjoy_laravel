<div class="row mlr-60 center">
    <form method="POST" action="account/change">
        @csrf
        
        <input id="curr_password" type="password" class="form-control" name="curr_password" placeholder="Introduce tu contraseña actual" required>
        <input type="hidden" name="attr" value="{{$attr}}">

        <br>
        
        @if($attr == 'user_name' || $attr == 'user_surname')
            <input id="user_name" type="text" class="form-control" name="user_name" placeholder="Introduce el nombre" maxlength="50" required>
            <input id="user_surname" type="text" class="form-control" name="user_surname" placeholder="Introduce los apellidos" maxlength="100" required>
        @elseif($attr == 'password')
            <input id="password" type="password" class="form-control" name="password" placeholder="Introduce tu nueva contraseña" maxlength="255" required>
            <input id="password_confirmation" type="password" class="form-control" name="password_confirmation" placeholder="Confirma tu nueva contraseña" maxlength="255" required>
        @endif

        <center>
            <button type="submit" class="btn btn-primary">
                {{ __('Actualizar') }}
            </button>
        </center>
    </form>
</div>