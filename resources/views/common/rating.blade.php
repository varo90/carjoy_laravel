<?php 
    $title = $star == 0 ? 'Sin votos aún' : '';
?>
@for($i=0;$i<5;$i++)
    <?php 
        if($star >= 0.8) {
            $draw_star = 'zmdi-star';
        } else if ($star > 0 && $star < 0.8) {
            $draw_star = 'zmdi-star-half';
        } else {
            $draw_star = 'zmdi-star-outline';
        }
        $star--;
    ?>
    <a title="{{ $title }}"><i class="zmdi {{ $draw_star }}"></i></a>
@endfor