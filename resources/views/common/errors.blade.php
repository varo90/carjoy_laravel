<?php
    if($errors->any()) {
        $errors = $errors->all();
    } else if(request()->has('errors')) {
        $errors = request()['errors'];
    } else {
        $errors = [];
    }
?>
@if(count($errors) > 0)
    <div class="notification is-danger">
        <ul>
            @foreach($errors as $error)
                <li> {{ $error }} </li>
            @endforeach
        </ul>
    </div>
    <br>
@endif