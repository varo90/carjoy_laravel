<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    // 'password' => 'Passwords must be at least eight characters and match the confirmation.',
    // 'reset' => 'Your password has been reset!',
    // 'sent' => 'We have e-mailed your password reset link!',
    // 'token' => 'This password reset token is invalid.',
    // 'user' => "We can't find a user with that e-mail address.",
    'password' => 'La contraseña debe tener al menos 8 caracteres y ser igual que la confirmación.',
    'reset' => 'Tu contraseña ha sido reinicializada.',
    'sent' => 'Te hemos enviado un correo con un link para resetear la contraseña.',
    'token' => 'Este Token para la contraseña no es válido.',
    'user' => "No existe ningún usuario con esa cuenta de email.",

];
